<meta charset="utf-8">
<?php

@session_start();
include_once "_common.php";

$_POST = array_map('mysql_escape_string', $_POST);
$_GET = array_map('mysql_escape_string', $_GET);

include ADFRAME_ROOT_PATH."/lib/ora11g_conn.php";


if (!$Confirm) {
    go_back("잘못된 접근입니다.");
    exit;
}

switch($Confirm)
{

    case "":

        go_back("로그인 정보가 잘못되었습니다.");
        break;

    case "login":

        session_unset("MEMBER_GROUP");
        session_unset("MEMBER_UID");
        session_unset("MEMBER_UNAME");
        session_unset("MEMBER_GUBUN");

        $oradb=new ora11g();
        $oradb->con();

        $_POST[login_id] = SQL_Injection($_POST[login_id]);
        $_POST[login_pw] = SQL_Injection($_POST[login_pw]);

        //학생로그인
        if($_POST['divide'] == "student"){

            $loginQue = "SELECT USERNO, KORNM, LOGIN_PASS, DR_ST, GUBUN FROM haksa.home_login_hakseng WHERE USERNO='".$_POST['login_id']."' ";
            $rs=$oradb->query($loginQue);

            $db_id = $rs[USERNO];
            $db_pw = $rs[LOGIN_PASS];

            $db_name = $rs[KORNM];
            $db_divide = $rs[DR_ST];

            $db_gubun = $rs[GUBUN];

            //교직원 로그인
        }else if($_POST['divide'] == "employee"){

            $loginQue = "SELECT USERNO, KORNM, LOGIN_PASS, DR_ST, SS_GS_JW_NM, SS_JW_JW_NM FROM haksa.home_login_jikwon WHERE USERNO='".$_POST['login_id']."' ";
            $rs=$oradb->query($loginQue);

            $db_id = $rs[USERNO];
            $db_pw = $rs[LOGIN_PASS];

            $db_name = $rs[KORNM];
            $db_dtst = $rs[DR_ST]; //재직, 휴직구분

            $db_ss_gsnm = $rs[SS_GS_JW_NM]; //교수직위
            $db_ss_jwnm = $rs[SS_JW_JW_NM]; //직원직위


            //디바이드 체크(직원변수 변환)
            if($db_ss_gsnm && $db_ss_jwnm)	$db_divide = $db_ss_gsnm;
            else if($db_ss_gsnm)			$db_divide = $db_ss_gsnm;
            else if($db_ss_jwnm)			$db_divide = $db_ss_jwnm;
            else							$db_divide = "";

            $_POST['login_pw'] = md5(md5($_POST['login_pw']));

        } else if($_POST['divide']=="company") {
            $hostName = "203.234.1.32";
            $userName = "root";
            $userPassword = "doemqodzm77";
            $dbName = "iuk_job";

            ##### 데이터베이스에 연결한다.
            $conn = mysql_connect($hostName,$userName,$userPassword) or DIE("ACCESS_DENIED_DB_CONNECTION");
            mysql_select_db($dbName) ;
            $loginQue = "SELECT * FROM iuk_job.way_member WHERE mb_id='".$_POST['login_id']."' ";
            $rs = mysql_query($loginQue);
            $row = mysql_fetch_array($rs);
            $db_id = $row[mb_id];
            $db_pw = $row[mb_password];
            $_POST['login_pw'] = sql_password($_POST['login_pw']);
            $db_name = $row[mb_name];
            mysql_close($conn);

            $division="CO";
        }


        //$oradb->discon();

        if( $db_id != $_POST['login_id'] || $db_pw != $_POST['login_pw'] || $_POST['login_id'] == "" || $_POST['login_pw'] == "")
        {

            go_back("로그인 정보가 잘못되었습니다.");
            exit;
        }
        else
        {

//            @session_register("MEMBER_GROUP");
//            @session_register("MEMBER_UID");
//            @session_register("MEMBER_UNAME");
//            @session_register("MEMBER_GUBUN");


            /*
            "교원(교수)"=>"GS",
            "직원"=>"JW",
            "조교"=>"JK",
            "시간강사"=>"SK",

            "재학생"=>"HS",
            "졸업생"=>"JS",
            "휴학생"=>"HK"
            */

            //디바이드 세션 굽기(학생)
            if($_POST['divide'] == "student"){
                if(strpos(",".$db_divide, "졸업") == true){
                    $division = "JS";
                }else if(strpos(",".$db_divide, "휴학") == true){
                    $division = "HK";
                }else{
                    $division = "HS";
                }
            }

            //디바이드 세션 굽기(교직원)
            if($_POST['divide'] == "employee"){
                if(strpos(",".$db_divide, "시간") == true){
                    $division = "SK";
                }else if(strpos(",".$db_divide, "교수") == true){
                    $division = "GS";
                }else if(strpos(",".$db_divide, "조교") == true){
                    $division = "JK";
                }else{
                    $division = "JW";
                }
            }

            $_SESSION['MEMBER_GROUP'] = $division;
            $_SESSION['MEMBER_UID'] = $db_id;
            // String - Change the Charset encoding            
            if ( mb_detect_encoding($db_name) != "UTF-8" ) {
                $db_name = iconv("CP949", "UTF-8", $db_name);
            }
            $_SESSION['MEMBER_UNAME'] = $db_name;
            $_SESSION['MEMBER_GUBUN'] = $db_gubun;

            if(empty($_SESSION['MEMBER_UID'])){
                go_back("로그인이 실패하였습니다.");
            }else{
                //https 적용시
                //script(" location.href = 'http://www.iuk.ac.kr/login/logon_https.php'; ");
                //script("opener.location.reload(); window.close(); ");


                if($_POST['mainform']=="check")	$redirect_file = "logon_https_main.php";
                else							$redirect_file = "logon_https.php";

                //학생정보센터
                if($site == "cis"){
                    script(" location.href = 'http://cis.iuk.ac.kr/login/".$redirect_file."'; ");
                }else if($site == "ipsi"){
                    script(" location.href = 'http://ipsi.iuk.ac.kr/login/".$redirect_file."'; ");
                }else if($site){
                    script(" location.href = 'http://".$site."/global_img/".$redirect_file."'; ");
                }else{

                    //script(" location.href = 'http://www.iuk.ac.kr/login/logon_https.php'; ");
                    //script(" location.href = './logon_https.php'; ");
                    //script(" location.href = '/'; ");
                    //script(" location.href = http://".$MAINSITEURL."'/login/".$redirect_file."'; ");
                    // 학과에서 페이지 새로고침
                    script(" window.parent.location.reload(); ");
                }

            }
            exit;
        }
        break;

    case "logout":

        $qry = "Y";
        if($qry == "Y"){

            //session_unregister("MEMBER_GROUP");
            //session_unregister("MEMBER_UID");
            //session_unregister("MEMBER_UNAME");

            session_unset("MEMBER_GROUP");
            session_unset("MEMBER_UID");
            session_unset("MEMBER_UNAME");
            session_unset("MEMBER_GUBUN");


            if($_SESSION['MEMBER_UID']){
                go_back("로그아웃에 실패하였습니다.");
            }else{
                //https 적용시
                //script(" location.href = 'http://www.iuk.ac.kr/login/logon_https.php'; ");
                //script("opener.location.reload(); window.close(); ");

                if(empty($_GET['BURI']))	goto_url("http://".$MAINSITEURL);

                //학생정보센터
                if($site == "cis"){
                    script(" location.href = 'http://cis.iuk.ac.kr/login/logout.php?Confirm=logout&BURI=".$_GET['BURI']."'; ");
                }else if($site == "ipsi"){
                    script(" location.href = 'http://ipsi.iuk.ac.kr/login/logout.php?Confirm=logout&BURI=".$_GET['BURI']."'; ");
                }else if($site){
                    script(" location.href = 'http://".$site."/global_img/logout.php?Confirm=logout&BURI=".$_GET['BURI']."'; ");
                }else{
                    //goto_url($_GET['BURI']);
                    //redirect($_GET['BURI']);
                    //redirect("/");
                    // 학과에서 페이지 새로고침
                    script(" window.parent.location.reload(); ");
                }
            }
        }
        break;

}

function SQL_Injection($get_Str) {
    //$query_str = mysql_real_escape_string($get_Str);
    $query_str = $get_Str;

    return preg_replace("(undefined| select | or| and| from| where| union| insert| update| delete| drop|\"|\'|#|\/\*|\*\/|\\\|\;)", "", $query_str);
}

?>
