<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />

	<link rel="stylesheet" href="./css/popup.css">
	<script type="text/javascript" src="./js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/common.iuk.js"></script>

	<!--[if lt IE 9]>
	<script src="./js/modernizr-2.6.2.min.js"></script>
	<![endif]-->

	<title>로그인 &lt; iuk 한국국제대학교(INTERNATIONAL UNIVERSITY OF KOREA)</title>
</head>
<body>
<!--<form id="loginForm" name="loginForm" action="./login_proc.php" method="post">-->
	<input type="hidden" name="site" value="<?=$_GET['site']?>">
	<input type="hidden" name="mainform" value="<?=$_GET['mainform']?>">
	<input type="hidden" name="Confirm" value="login">

	<fieldset>
		<legend class="blind">로그인</legend>

		<div class="login-area">
			<div class="login-title">
				<h1>
					한국국제대학교 로그인
				</h1>

				<a href="javascript:window.close();" class="btn-close">
					<img src="images/btn_close.gif" alt="로그인 창닫기" />
				</a>
			</div>

			<div class="login-box">

				<div class="choise-checkbox">
					<input type="radio" id="radio0101" name="divide" value="student" checked="checked" />
					<label for="radio0101">
						학생
					</label>

					<input type="radio" id="radio0102" name="divide" value="employee" />
					<label for="radio0102">
						교직원
					</label>
				</div>

				<input type="text" id="login_id" name="login_id" placeholder="학번(직번)" />

				<input type="password" id="login_pw" name="login_pw" placeholder="비밀번호" />

				<input type="submit" id="btnDoLogin" name="" value="로그인" />

				<ul class="ul-list01">
					<li>재학생 및 교직원은 <strong>별도의 가입없이 해당 학번(직번)을 사용하</strong>시면 됩니다.</li>
					<li>로그인을 하시면 더 많은 정보를 얻을 수 있습니다.</li>
				</ul>
			</div>



		</div>
	</fieldset>
<!--</form>-->
<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
<script>$("[placeholder]").placeholder();</script>
<script>
	$(document).ready(function() {
		$('#btnDoLogin').bind('click', function() {
			// 입력 값 validation
			if ($('#login_id').val() == '') {
				alert("아이디를 입력하세요.");
				return false;
			}

			if ($('#login_pw').val() == '') {
				alert("패스워드를 입력하세요.");
				return false;
			}

			//var _mainForm = $(window.opener).find('#mainLoginForm');
			var _mainForm = $(window.opener.document).find('#mainLoginForm');
			_mainForm.find('#login_id').val($('#login_id').val());
			_mainForm.find('#login_pw').val($('#login_pw').val());
			_mainForm.find('#divide').val($('input[name="divide"]:checked').val());

			_mainForm.submit();
			window.close();
			//$('#loginForm').submit();
		});

	});
</script>

<?
$MAINSITEURL = "www.iuk.ac.kr";
if(empty($_SESSION['MEMBER_UID'])) {
?>
<script language="javascript" src="/js/jquery-1.6.min.js"></script>
<script language="Javascript">
$(window).load(function(){
	$.getJSON("http://<?=$MAINSITEURL?>/login/session_public.php?jsoncallback=?", {SessionHost:"<?=$_SERVER['HTTP_HOST']?>"}, function(data){
		if(data.session_key != "false"){
			$.getJSON("session_public.php", {SessionHost:"<?=$_SERVER['HTTP_HOST']?>",session_key:data.session_key}, function(sresult){
				if(sresult.enddata == "complite"){
					opener.location.reload();
					window.close();
				}else{
					alert('로그인에 실패하였습니다.');
					window.close();
				}
			});
		}else{
			alert('로그인에 실패하였습니다.');
			window.close();
		}
	});
});
</script>
<? }else{ ?>
<script language="Javascript">
	opener.location.reload();
	window.close();	
</script>
<? } ?>
</body>
</html>
