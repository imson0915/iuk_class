<?php
class MailClass {
	//**************************************//
	// Script By ADBANK Solution Team Yunil //
	//**************************************//
	var $server = "127.0.0.1";
	var $port = 25;
	var $timeout = 30;
	var $contentType = "text/plain";
	var $mailFromAddress;
	var $fromName;
	function MailClass($server="") {
		if($server != "") $this->server = $server;
	}

	function setFrom($mailFromAddress, $fromName) {
		$this->mailFromAddress = $mailFromAddress;
		$this->fromName = $fromName;
	}

	function setContentType($type) {
		$this->contentType = $type;
	}

	function checkMsg($msg) {
		if (!ereg("^250", $msg)) {
			echo "메일을 보내지 못했습니다<BR>".$msg; 
			exit;
		} else {
			// Delete Comment When Debugin Mode
			// echo $msg."<BR>";
		}
	}

	function send($mailToAddress, $toName, $subject, $body) {
		$subject = addslashes($subject);
		$body = chunk_split(base64_encode($body));
		$fp = fsockopen($this->server, $this->port, &$errno, &$errstr, $this->timeout);
		if(!$fp) {echo("sendmail ?: $errstr ($errno)\n"); exit;}
		fgets($fp, 128);
		fputs($fp, "HELO ".$this->server."\r\n");
		$this->checkMsg(fgets($fp, 128));
		
		fputs($fp, "MAIL from: ".$this->mailFromAddress."\r\n");
		$this->checkMsg(fgets($fp, 128));
		fputs($fp, "RCPT TO: ".$mailToAddress."\r\n");
		$this->checkMsg(fgets($fp, 128));
		fputs($fp, "DATA\r\n");
		fgets($fp, 128);		
		fputs($fp, "Return-Path: < ".$this->mailFromAddress." >\r\n"); 
		fputs($fp, "From: \"".$this->fromName."\" < ".$this->mailFromAddress." >\r\n"); 
		fputs($fp, "To: \"".$toName."\" <".$mailToAddress.">\r\n"); 
		fputs($fp, "Subject: ".$subject."\r\n");
		fputs($fp, "X-Mailer: ADBANK-MAILER\r\n"); 
		fputs($fp, "MIME-Version: 1.0\r\n");
		fputs($fp, "Content-Type: ".$this->contentType.";\r\n"); 
		fputs($fp, "\tcharset=\"ks_c_5601-1987\"\r\n"); 
		fputs($fp, "Content-Transfer-Encoding: base64\r\n");
		fputs($fp, "\r\n");
		fputs($fp, $body);
		fputs($fp, "\r\n");
		fputs($fp, ".");
		fputs($fp, "\r\n");
		$this->checkMsg(fgets($fp, 128));		
		fclose($fp);
	}
}
?>
