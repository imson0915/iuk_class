<?
class wec4decoder
{
	var $item_dir       = "";
	var $item_url       = "";
	var $msg            = "";
	var $mime_version   = "";
	var $x_generator    = "";
	var $content_type   = "";
	var $boundary       = "";
	var $contents       = ""; 
	var $item           = ""; 
	var $max_item       = 0;  
	var $error          = "";
	var $errno          = 0;
	function wec4decoder( $msg="")
	{
		$this->msg = $msg;
		return;
	}
	function set_basic()
	{
		$tmp = explode( "\r\n", $this->msg);
		for( $i = 0; $tmp[$i]; $i++)
		{
			$tmp_sub = explode( ":", $tmp[$i]);
			$tmp_sub[0] = trim( $tmp_sub[0]);
			$tmp_sub[1] = trim( $tmp_sub[1]);

			if( $tmp_sub[0] == "MIME-Version")
			{
				$this->mime_version = $tmp_sub[1];
			}
			if( $tmp_sub[0] == "X-Generator")
			{
				$this->x_generator = str_replace( ";", "", $tmp_sub[1]);
			}
			if( $tmp_sub[0] == "Content-Type")
			{
				$this->content_type =	$tmp_sub[1];
			}
		}
		$this->boundary = "";
		if( $i = strpos( $this->msg, "boundary="))
		{
			
			$i = $i + strlen( "boundary=\\\"");
			
			
			while( $this->msg[$i] != "\\" && $i <= strlen( $this->msg))
			{
				$this->boundary .= $this->msg[$i];
				$i++;
			}
			
		}
		
		
	}
	function split()
	{

		

		$item = explode( "--".$this->boundary, $this->msg);
		$this->max_item = 0;

		

		for( $i = 0; $i < sizeof( $item); $i++)
		{
			
			
			$item_sub    = explode( "\r\n\r\n", $item[$i]);

			

			$item_header = trim( $item_sub[0]); 
			$item_body   = trim( $item_sub[1]); 

			
			
			$tmp['type'] = "";
			$j = strpos( $item_header, "Content-Type:");
			$j = $j + strlen( "Content-Type:");
			while( $item_header[$j] != "\n" && $item_header[$j] != ";" && $j < strlen( $item_header))
			{
				$tmp[type] .= $item_header[$j];
				$j++;
			}
			$tmp['type'] = trim( $tmp['type']);
			//---> 확장자 알아내기
			$tmpext = explode("/",$tmp['type']);
			$i_ext = strtolower(trim($tmpext[1]));
			$tmp[encoding] = "";
			$j = strpos( $item_header, "Content-Transfer-Encoding:");
			$j = $j + strlen( "Content-Transfer-Encoding:");
			while( $item_header[$j] != "\n" && $item_header[$j] != ";" && $j < strlen( $item_header))
			{
				$tmp['encoding'] .= $item_header[$j];
				$j++;
			}
			$tmp['encoding'] = trim( $tmp['encoding']);
			$tmp[id] = "";
			$j = strpos( $item_header, "Content-ID: <");
			$j = $j + strlen( "Content-ID: <");
			while( $item_header[$j] != ">" && $j < strlen( $item_header))
			{
				$tmp['id'] .= $item_header[$j];
				$j++;
			}
			$tmp['id'] = trim( $tmp[id]);
			$tmp['name'] = "";
			$j = strpos( $item_header, "name=");
			$j = $j + strlen( "name=\\\"");
			while( $item_header[$j] != "\\" && $j < strlen( $item_header))
			{
				$tmp['name'] .= $item_header[$j];
				$j++;
			}
			$tmp[name] = trim( $tmp[name]);
			//---> 한글이름의 경우 원래 인코딩된 이름으로 유지하도록 함
			$ck = strstr($tmp[name],"?");
			$tmp['name'] = str_replace("?","",$tmp['name']);
			$tmp['name'] = str_replace("=","",$tmp['name']);
			$tmp['name'] = str_replace("+","",$tmp['name']);
			$tmp['name'] = str_replace("/","",$tmp['name']);
			if(($i_ext=="gif" || $i_ext=="jpeg" || $i_ext=="bmp") && $ck) $tmp['name'] .= ".".$i_ext;

			$tmp['body'] = base64_decode( $item_body);
			
			if( $tmp['type'])
			{
				if( $tmp['type'] == "multipart/related")
				{
				}else if( $tmp['type'] == "text/page")
				{
					$this->contents = $tmp;
				}
				else
				{
					$this->item[$this->max_item] = $tmp;
					$this->max_item++;
				}
			}
		}
		return;
	}
	function set_body()
	{
		for( $i = 0; $i < $this->max_item; $i++)
		{
			$this->contents['body'] = str_replace(
				"cid:".$this->item[$i][id],
				$this->item_url.$this->item[$i][name],
				$this->contents['body']
			);
		}
		return;
	}

         function set_item()
	{
	 	for( $i = 0; $i < $this->max_item; $i++){
    		  for( $j = 0; $j < $this->max_item; $j++){
  	  	  $this->item[$i][body] = str_replace(
  	  	   "cid:".$this->item[$j][id],
  	  	   $this->item_url.$this->item[$j][name],
  	  	   $this->item[$i][body]
  	  	  );
		  }
		}
		return;
	}

	function store_item()
	{
		for( $i = 0; $i < $this->max_item; $i++)
		{
			$fp = fopen( $this->item_dir.$this->item[$i][name], 'wb')
				or $this->set_error( $this->item_dir."을 열수 없습니다!");
			fwrite( $fp, $this->item[$i][body])
				or $this->set_error( $this->item_dir."에 쓸수 없습니다!");
			fclose( $fp);
		}
		return;
	}
	function set_error( $msg)
	{
		$this->error = $msg;
		
		return;
	}
	function run()
	{
		$this->set_basic();   
		$this->split();       
		$this->set_body();  
		$this->set_item();    
		$this->store_item();  
		
		return;
	}
}
?>
