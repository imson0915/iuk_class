<?php
// GET 파라미터 변수 암호 인코딩 
function Encoding64( $param ) {
    return base64_encode($param);
}

// GET 파라미터 변수 암호 디코딩
function Decoding64( $param ) {
    $exp_prm = explode( "&", str_replace("||", "", base64_decode($param)) );
    foreach ( $exp_prm as $key => $value ) {
        $element = explode("=", $value);
        $ret[$element[0]] = $element[1];
    }
    return $ret;
}

// move page to url
// @param string $url - web url
function redirect($url) {
    echo "<meta http-equiv='refresh' content='0; URL=".$url."'>";
    exit;
}

function go_back($msg="") {
    echo "<script>";
    if ( $msg ) echo "alert('".$msg."');";
    echo "history.go(-1); </script>";
    exit;
}

function alert($msg) {
    str_replace('\\', '\\\\', $msg);
    echo "<script> alert('".$msg."'); </script>";
}

function alert_href($url, $msg="") {
    echo"<script>";
    if ( $msg ) echo "alert('$msg');";
    echo "location.href = '$url'; </script> ";
    exit;
}

function alert_replace($url, $msg="") {
    echo "<script>";
    if ( $msg ) echo "alert('$msg');";
    echo " location.replace('$url'); </script>";
    exit;
}

function alert_close($msg="") {
    echo"<script>";
    if ( $Msg ) echo "alert('$msg');";
    echo " window.close(); </script> ";
    exit;
}

function MsgView($go, $Msg="") {
    echo"<script>";
    if ( $Msg ) echo " alert('$Msg'); ";
    echo "history.go($go); </script> ";
    exit;
}

// (관리자페이지) 허용 IP 거르기
function allow_ip_setting ( $ip ) {
    foreach ( explode(".", $ip) as $k => $v ) {
        if ( $k == 3 ) break;
        else {
            if ( $user_IP != "" ) $user_IP .= ".";
            $user_IP .= $v;
        }
    }
    // 허용 IP
    $admission_IP = array(
        "112.217.216"			// adbank IP
    ,"127.0.0"				// localhost
    );
    while (list ($key, $val) = each ($admission_IP)) {
        if ($val==$user_IP){
            $adminssion = "Y";
            break;
        }
    }
    return $adminssion;
}

//로그 입력 : 구분(등록, 수정, 삭제) / 테이블명 / 글번호 / 작성자
function log_history($gubun, $table, $board_id, $writer = ''){
    global $adb;

    $sql = " select max(BOARD_NO), min(PARENT) from ".TABLE_LOG_HISTORY;
    $row = $adb->getRow($sql, array(), DB_FETCHMODE_ASSOC);
    // 구분번호
    $insert_id = $row[BOARD_NO]+1;
    $parent = $row[PARENT]-1;

    if($_SESSION['__MEMBER_ID__'] == '') $logWriter = $writer;
    else $logWriter = $_SESSION['__MEMBER_ID__'];

    if($gubun == '수정' || $gubun == '삭제') $board_id .= '번';
    else if($gubun == '등록') $board_id .= '신규';

    $historyTitle = $logWriter."님이 '".$table."' 테이블에서 ".$board_id." 게시물을 ".$gubun."하였습니다.";
    $historyContents = "작성자 : $logWriter <br/>
							IP : ".$_SERVER['REMOTE_ADDR']."<br/>
							테이블 : ".$table."<br/>							
							행위 : ".$gubun;
    $fields_and_values = array(
        'BOARD_NO'			=> $insert_id,
        'PARENT'            => $parent,
        'CATEGORY'			=> 1,
        'WRITER'			=> 'log',
        'REMOTE_ADDR'		=> $_SERVER['REMOTE_ADDR'],
        'TITLE'				=> $historyTitle,
        'CONTENTS'			=> $historyContents,
        'RTIME'				=> time()
    );

    $error = $adb->autoExecute(TABLE_LOG_HISTORY, $fields_and_values);
}

//로그인로그 입력 : 구분(등록, 수정, 삭제) / 테이블명 / 글번호 / 작성자
function log_admin(){
    global $adb;

    $sql = " select max(BOARD_NO), min(PARENT) from ".TABLE_LOG_ADMIN;
    $row = $adb->getRow($sql, array(), DB_FETCHMODE_ASSOC);

    $insert_id = $row[0]+1;
    $parent = $row[1]-1;

    $logWriter = $_SESSION['__MEMBER_ID__'];

    $historyTitle = "[".date('Y.m.d H:i:s', time())."] ".$logWriter."님이 로그인하였습니다.";
    $historyContents = "계정명 : $logWriter <br/>
								IP : ".$_SERVER['REMOTE_ADDR']."<br/>";
    $fields_and_values = array(
        'BOARD_NO'			=> $insert_id,
        'PARENT'            => $parent,
        'CATEGORY'			=> 1,
        'WRITER'			=> 'log',
        'REMOTE_ADDR'		=> $_SERVER['REMOTE_ADDR'],
        'TITLE'				=> $historyTitle,
        'CONTENTS'			=> $historyContents,
        'RTIME'				=> time()
    );

    $error = $adb->autoExecute('xboard_board_log_admin', $fields_and_values);
}

function desc_columns($tbl_name) {
    global $adb;

    $sql = " SHOW COLUMNS FROM ".$tbl_name;
    $rs = $adb->query($sql);
    for ( $i = 0 ; $row = $rs->fetchRow(DB_FETCHMODE_ASSOC) ; $i++ ) {
        $arr[] = $row[Field];
    }
    return $arr;
}

// 변수 또는 배열의 이름과 값을 얻어냄. print_r() 함수의 변형
// 아래가 출력이 보기 편하다. 
function print_r2($var)
{
    ob_start();
    print_r($var);
    $str = ob_get_contents();
    ob_end_clean();
    $str = preg_replace("/ /", "&nbsp;", $str);
    echo nl2br("<span style='font-family:Tahoma, 굴림; font-size:9pt;'>$str</span>");
}

// 디렉토리정보 로드 
function get_dir_list($dir)
{
    $result_array = array();

    $dirname = $dir;
    $handle = opendir($dirname);
    while ($file = readdir($handle))
    {
        if($file == "."||$file == "..") continue;

        if (is_dir($dirname.$file)) $result_array[] = $file;

    }
    closedir($handle);
    sort($result_array);

    return $result_array;
}

function get_file_list($path, $arr=array()){
    $dir = opendir($path);
    while($file = readdir($dir)){
        if($file == '.' || $file == '..'){
            continue;
        }else if(is_dir($path.'/'.$file)){
            $arr = get_file_list($path.'/'.$file, $arr);
        }else{
            $arr[] = $path.'/'.$file;
        }

    }
    closedir($dir);
    return $arr;
}

// 게시판 생성 함수. class_bbs.php 를 참조한다. 
function create_bbs($board_key, $category_no="0", $SecAdmin="0", $bbs_userqry="", $bbs_subqry="", $bbs_subcolumnqry="", $design_category=""){
    global $bbs;
    
    $Obj=new Sub_BBSStart();
    
    $Obj->makebbs($bbs, $board_key, $category_no, $SecAdmin, $bbs_userqry, $bbs_subqry, $bbs_subcolumnqry, $design_category);
}

// Get방식으로 넘어온 변수를 Decode하는 함수
function Decode64($sending_data){

    //global $EncoderKey;
    
    $vars=explode("&",base64_decode(str_replace("||","",$sending_data)));
    //$vars=explode("&",base64_decode(str_replace($EncoderKey,"",$sending_data)));

    $vars_num=count($vars);
    for($i=0;$i<$vars_num;$i++){
        $elements=explode("=",$vars[$i]);
        $var[$elements[0]]=$elements[1];
        //echo " $elements[0] = $elements[1] <br> ";
    }
    return $var;
}

// Get 방식 변수 암호화 함수
function Encode64($data)
{
    //global $EncoderKey;
    //$data = rand_str(5, 0).$data.rand_str(3, 0)

    //return base64_encode($data.$EncoderKey);
    return base64_encode($data)."||";
}

//현재시간과 특정일 사이의 기간
function BetweenPeriod($datetime,$periodDay)
{//2003-02-19 11:32:15
    $now = time();
    $timeArr= explode(":",substr($datetime,11,8));
    $dayArr	= explode("-",substr($datetime,0,10));

    $mktime = mktime($timeArr[0],$timeArr[1],$timeArr[2],$dayArr[1],$dayArr[2],$dayArr[0]);
    $period	= $periodDay*24*60*60;		//기간계산

    if($now >$mktime && $now < ($mktime+$period))
        return 1;
    else if( ($mktime-$period) <$now && $now <$mktime)
        return -1;
    else
        return 0;
}

/*
 * 한글 깨짐 방지를 위해 mb_str..메서드를 사용. php 버전 4이상. 
 * param 1 : 문자열 원본
 * param 2 : 자르기 시작 인덱스 (디폴트 0)
 * param 3 : 자르기 종료 인덱스
 * param 4 : 문자열 인코딩 셋. utf-8이 기본이다. 다른 인코딩 사용하지 말것.
 * param 5 : 덧붙일 문자열
 */
function StringCut($string,$start=0,$length,$charset,$addString="...") {

    if($charset==NULL) {
        $charset='UTF-8';
    }
    $str_len=mb_strlen($string,$charset);
    
    if($str_len>$length) {
        /* mb_substr  PHP 4.0 이상, iconv_substr PHP 5.0 이상 */
        $string=mb_substr($string,$start,$length,$charset);
        $string.=$addString;
        
    }
    return $string;
}


//Post, Get방식으로 넘어온 값 쿼리로 변환
function setQuery ($arr, $str) {
    $r_str = '';

    // $arr : $_POST 혹은 $_GET, 배열 반복
    foreach ($arr as $key=>$val) {

        // input name 이 지정한 문자열로 시작되면 적용 하고 그 문자열은 삭제 
        //if (@ereg ("^$str", $key)) { // php 5.3이후 디프레케이트 6.0부터삭제
        if(preg_match("/^$str/", $key)) {

            //$key = @ereg_replace ("^$str", "", $key);  // php 5.3이후 디프레케이트 6.0부터삭제
            $key = preg_replace ("/^$str/", "", $key);

            // 문자열을 제거한 키값이 아직 존재하면 리턴해줄 문자열에 연결합니다.
            // 처음은 xxx = 'xxx' 로 두번째 부터는 ,xxx = 'xxx'로 이어줍니다.
            if (!empty($key)) {
                if (!empty($r_str)) {
                    $r_str .= ', ';
                }

                $r_str .= "${key} = '${val}'";
            }
        }
    }

    return $r_str;
}


//게시판 최근게시물 값 배열에 담기
function BBS_GetList($board_table , $board_code, $board_type=0, $limit_num=5, $cut_content=0, $debugmod=0){
//BBS_GetList("게시판 테이블명(fullname)", "게시판코드", 보드타입, 배열에 담을 최근게시물 수, 내용글 수:html형식이라 300이상으로 잡아야...);
    /*
        $board_type=0 : 일반
        $board_type=1 : 갤러리
        $board_type=2 : UCC
    
        [사용방법]
        $newlist1 = BBS_GetList("bbs_com1", "3010", 0, 5, 300);
        
        for($i=0; $i < count($newlist1); $i++){
            
            //echo $newlist1[$i][title]."<br>"; //제목
            
            //echo $newlist1[$i][content]."<br>"; //내용
            
            //echo $newlist1[$i][linkdata]."<br>"; //링크데이타  <a href='파일명?bbs=see&data=$newlist1[$i][linkdata]'>
            //echo $newlist1[$i][file_src]."<br>"; //파일경로 <img src='$newlist1[$i][file_src]'> width height는 지정
            
            //echo $newlist1[$i][datetime]."<br>"; //등록일자
            //echo $newlist1[$i][newimg]."<br>"; //새글 아이콘
        
        }
    
    */

    if($board_type == 1)
        //$bbs_subcolumnqry = ", (select idx from ".$board_table."_file where file_type > 0 and file_type < 4 and up_file_idx = A.up_file_idx limit 0,1) as file_idx";
        $bbs_subcolumnqry = ", (select idx from ".$board_table."_file where file_type > 0 and file_type < 4 and up_file_idx = A.up_file_idx limit 0,1) as file_idx";
    else if($board_type == 2)
        //$bbs_subcolumnqry = ", (select up_filename from ".$board_table."_file where file_type = 10 and up_file_idx = A.up_file_idx limit 0,1) as up_filename";
        $bbs_subcolumnqry = ", (select up_filename from ".$board_table."_file where up_file_idx = A.up_file_idx limit 0,1) as up_filename";
    else
        $bbs_subcolumnqry = "";

    $bbs_qry = "SELECT * ";
    $bbs_qry .= $bbs_subcolumnqry;
    $bbs_qry .= " FROM ".$board_table." A WHERE idx > 0 and code = '$board_code' ";
    $bbs_qry .= " ORDER BY notice ASC, ref DESC, re_step ASC limit $limit_num ";

    if($debugmod == 1)	echo $bbs_qry;

    $result = DBquery($bbs_qry);

    for($i=0; $row=@mysql_fetch_array($result); $i++){

        $bbslimit[$i][idx] = $row[idx];

        //게시판 제목
        $bbslimit[$i][title] = $row[title];

        //게시판 등록일자
        $bbslimit[$i][datetime] = substr($row[writeday],0,10);

        //게시판 새글
        if(BetweenPeriod($row[writeday],"1") > 0) $bbslimit[$i][newimg] = "<img src='../make_img/board/icon_new.gif' border='0' align='absmiddle'>";
        else $bbslimit[$i][newimg] = "";

        //링크 데이타
        $encode_str = "pagecnt=0&idx=".$row[idx]."&letter_no=&offset=";
        $encode_str.= "&search=".$search."&searchstring=".$searchstring;
        $encode_str.= "&Boardkey=".$row[code]."&Sub_No=".$row[sub_no]."&DBTable=".$board_table;
        $list_data=Encode64($encode_str);

        $bbslimit[$i][linkdata] = $list_data;

        //타입별 이미지경로
        if($board_type == 1){/*
            $bbslimit[$i][file_idx] = $row[file_idx];

            $fileencode_str = "Boardkey=".$board_code."&DBTable=".$board_table."&idx=".$row[file_idx]."&download=ok";
            $file_data=Encode64($fileencode_str);
            $bbslimit[$i][file_src] = "/bbs/imageview.php?image=thumnail&data=".$file_data;
            //$bbslimit[$i][file_src] = "/bbs/imageview.php?data=".$file_data;
            */
            $data = "image=thumnail&board_table=".$board_table."&idx=".$row[up_file_idx];
            $bbslimit[$i][file_src] = "/adframe/bbs/imageview_renew.php?".$data;
        }else if($board_type == 2){
            $bbslimit[$i][file_src] = "/bbs/".$row[up_filename];
        }else{
            //$bbslimit[$i][file_src] = "/bbs/imageview.php?image=noimg";
            $bbslimit[$i][file_src] = "/bbs/skin/iuk_gallery/images/noimg.gif";
        }
        //내용 출력시
        if($cut_content > 0){
            $bbs_content = str_replace("&nbsp;", "", $row[content]);
            $bbs_content = StringCut(trim(strip_tags($bbs_content)), $cut_content);
            $bbslimit[$i][content] = $bbs_content;

        }

    }

    return $bbslimit;
}
    
// 하단 바로가기 링크
function class_select_link($category, $site_no='1') {
    global $adb;
    $tbl="af_link_site";
    if ( $category == '1' ) $title = "학부/학과 홈페이지 바로가기";
    else if ( $category == '2' ) $title = "부속부설기관 바로가기";
    echo "<option value=''> ".$title." </option>";
    $sql = " SELECT * FROM ".$tbl." WHERE lt_category = '".$category."' AND site_no = '".$site_no."' ORDER BY lt_rank DESC ";
    $rs = $adb->query($sql);
    for ( $i = 0 ; $row = $rs->fetchRow(DB_FETCHMODE_ASSOC) ; $i++ ) {
        if ( $row[lt_url] ) $url = $row[lt_url];
        else $url = "none";
        echo "<option value='".$url."'> ".$row[lt_name]."</option>";
    }
}

function class_databaseTableName ($tree_no) {
    switch($tree_no) {
        case '28': $tbl = "bbs_hak01"; break;
        case '30': $tbl = "bbs_hak02"; break;
        case '29': $tbl = "bbs_hak03"; break;
        case '31': $tbl = "bbs_hak04"; break;
        case '38': $tbl = "bbs_hak05"; break;
        case '37': $tbl = "bbs_hak06"; break;
        case '34': $tbl = "bbs_hak07"; break;
        case '33': $tbl = "bbs_hak08"; break;
        case '32': $tbl = "bbs_hak09"; break;
        case '35': $tbl = "bbs_hak10"; break;
        case '43': $tbl = "bbs_hak11"; break;
        case '41': $tbl = "bbs_hak12"; break;
        case '42': $tbl = "bbs_hak13"; break;
        case '40': $tbl = "bbs_hak14"; break;
        case '44': $tbl = "bbs_hak15"; break;
        case '47': $tbl = "bbs_hak16"; break;
        case '46': $tbl = "bbs_hak17"; break;
        case '45': $tbl = "bbs_hak18"; break;
        case '48': $tbl = "bbs_hak20"; break;
        case '49': $tbl = "bbs_hak21"; break;
        case '50': $tbl = "bbs_hak22"; break;
        case '52': $tbl = "bbs_hak23"; break;
        case '51': $tbl = "bbs_hak24"; break;
        case '56': $tbl = "bbs_hak26"; break;
        case '54': $tbl = "bbs_hak27"; break;
        case '55': $tbl = "bbs_hak28"; break;
        case '57': $tbl = "bbs_hak30"; break;
        case '36': $tbl = "bbs_hak32"; break;
        case '39': $tbl = "bbs_iuk30"; break;
        case '53': $tbl = "bbs_iuk32"; break;
        default: $tbl = "none"; break;
    }
    return $tbl;
} 


?>
