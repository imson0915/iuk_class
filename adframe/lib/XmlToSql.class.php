<?

require_once dirname(__FILE__) . "/XPath.class.php";

class XmlToSql {

	// @param string $db_type - database type

	var $xpath;

	var $db_type;



	function XmlToSql($db_type, $xml_path = '') {

		$this->xpath = new XPath($xml_path);

		if(!is_object( $this->xpath )) {

			echo('XPATH Loading Fail');

			exit;

		}

		$this->db_type = $db_type;

	}

	

	// function : Loading from xml string

	// @param string $xml - xml format string

	function xpathFromString($xml) {

		$this->xpath->reset();

		$this->xpath->importFromString($xml);

	}



	function xpathFromFile($path) {

		$this->xpath->reset();

		$this->xpath->importFromFile($path);

		print_r($this->xpath->getLastError());

	}

	



	// @param string $fields_type - to convert field type

	function getFieldType($field_type) {

		$type = parse_ini_file(dirname( __FILE__ ).'/../_Aetc/ini/' . strtolower($this->db_type) . ".ini");

		return $type[$field_type];

	}

	

	function getNodeData($path) {

		return $this->xpath->getData($path);

	}

	

	// @param string $table_name - to create table name ('mysql', 'oci8', 'mssql' ...)

	function parseSchemaXml($table_name = '') {

		if($this->xpath->match('/schema/table') ) {			

			

			$table = ($table_name == '') ? $this->xpath->getAttributes('/schema/table', 'name') : $table_name;

			$sql = 'CREATE TABLE ' . $table . ' (' . "\n";



			// Field loading			

			for($i=1; $this->xpath->match('/schema/table/field-list/field[' . $i .']'); $i++) {

				$current_path = '/schema/table/field-list/field[' . $i . ']';



				$field = $this->xpath->getAttributes($current_path);

				$sql .= "\t" . $this->xpath->getData($current_path);

				$sql .= " " . strtoupper($this->getFieldType($field['type']));

				if( $field['size'] ) {

					$sql .= '(' . $field['size']  .') ';

				}

				if($field['not_null'] == '1') {

					$sql .= ' NOT NULL ';

				}

				

				if(!is_null($field['default'])) {

					if(is_int($field['default'])) {

						$sql .= ' DEFAULT ' . $field['default'] . ' ';

					} else {

						$sql .= " DEFAULT '" . $field['default'] . "' ";

					}

				}

				if($field['extra']) {

					$sql .=  " " . $field['extra']."";

				}

				$sql = trim($sql) . ",\n";

			}



			// Primary-Key loading

			if($this->xpath->match("/schema/table/primary-key")) {

				$current_path = "/schema/table/primary-key";

				$sql .= "\tPRIMARY KEY (" . $this->xpath->getData($current_path) . "),\n";

			}



			// Index loading

			for($i=1; $this->xpath->match('/schema/table/index[' . $i . ']'); $i++) {

				$current_path = '/schema/table/index[' . $i . ']';

				$attributes = $this->xpath->getAttributes($current_path);

				$sql .= "\tINDEX " . $table . '_' . $attributes['name'] . "(" . $this->xpath->getData($current_path) . "),\n";

			}

/*

			// Constraint loading

			for($i=1; $this->xpath->match('/schema/table/constraint[' . $i . ']'); $i++) {

				$current_path = '/schema/table/constraint[' . $i . ']';

				$constraint = $this->xpath->getAttributes($current_path);



				$sql .= "\tCONSTRAINT " . $this->xpath->getData($current_path) . "_" . $table_name . " FOREIGN KEY (" . $constraint['foreign-key'] . ") REFERENCES ";



				if($constraint[ 'reference-table' ] == "board") 

					$sql .= $board_id . "_";



				$sql .= $constraint['reference-table'] . "(" . $constraint['reference-field'] . "),\n";

			}


*/
			$sql = substr( $sql, 0, -2 );



			$sql .= "\n);";

		}



		$result = array(

			'table'	=> $table,

			'query'	=> $sql

		);

		return $result;

	}

}

?>
