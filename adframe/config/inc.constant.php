<?

// Adframe 관련 상수 정보
define("ADFRAME_VERSION", "0.0.1");
define("ADFRAME_PREFIX",  "__AF__");
define("ADFRAME_PUBLISH",  $_SERVER['DOCUMENT_ROOT']."/page");  // 사이트 도메인 정보

define("ADFRAME_WEB_ROOT", "/adframe");
define("ADFRAME_ROOT_PATH", $_SERVER['DOCUMENT_ROOT'].ADFRAME_WEB_ROOT);

// 사이트 관련 전역 변수 
define("SERVICE_DOMAIN",  "iuk.ac.kr");  // 사이트 도메인 정보

// static resource(js, css, img) 루트 경로
define("ADFRAME_ASSETS_ROOT", $_SERVER['DOCUMENT_ROOT']."/assets");
// 퍼블리싱(html, php, jsp) 루트 경로
define("ADFRAME_PAGE_ROOT", $_SERVER['DOCUMENT_ROOT']."/page");

// 파일 업로드 루트 경로 
define("ADFRAME_UPLOAD_ROOT", $_SERVER['DOCUMENT_ROOT']."/data");
// 파일 다운로드 루트 
define("ADFRAME_DOWNLOAD_ROOT", "/data");


/*
// GET 파라미터들
$arr_getArg = array("cms", "board_id", "sel_type", "s_t", "s_w", "s_c", "key", "keyword", "p", "category");
$get_arg = "";
foreach ( $arr_getArg as $k => $v ) {
	if ( $get_arg != "" ) $get_arg .= "&";
	if ( $v == "s_t" || $v == "s_w" || $v == "s_c" || $v == "keyword" ) $v = urlencode($_GET[$v]);
	$get_arg .= $v."=".$_GET[$v];
}
$get_arg_view = "&no=".$_GET[no];

// 글쓰기, 리스트, 수정, 삭제 링크
$tplAssign = array();
$tplAssign['delete_url'] = "confirm.php?" . $get_arg . '&mode=del&no=' . $_GET['no'];
$tplAssign['modify_url'] = "write.php?mode=modify&" . $get_arg . '&no=' . $_GET['no'];
$tplAssign['reply_url'] = "write.php?mode=reply&" . $get_arg . '&no=' . $_GET['no'];
$tplAssign['write_url'] = 'write.php?'.$get_arg;
$tplAssign['list_url'] = 'aboard.php?'.$get_arg;
$tplAssign['list_url2'] = 'waybbs.php?'.$get_arg;
// 휴지통 관련 링크
$tplAssign['list_trash_url'] = 'aboard_trash.php?'.$get_arg;
*/

$aboard['protocol']		= 'http'.(isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on' ? 's' : '');
$aboard['base_url']		= $aboard['protocol'] . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
$aboard['login_url']		= $aboard['base_url'] . '/login.php';
$aboard['error_url']		= $aboard['base_url'] . '/error.php';
$aboard['default_category_name']	= '일반';


// 기본 상수 설정
define("_TAG_TITLE", "IUK 한국국제대학교(INTERNATIONAL UNIVERSITY OF KOREA)");

// 기본 경로 설정 
define("__UPLOAD_DIR__", dirname(__FILE__)."/../var/DATA");

// Table SETTING
define("TABLE_PREF", str_replace("_", "", ADFRAME_PREFIX)."_");																	// TABLE 접두사
define("TABLE_POSITION", TABLE_PREF."position_table");						// 회원 그룹 정보
define("TABLE_MEMBER", "admin");															// 회원 정보

// 기본 TABLE 정보
define('TABLE_MANAGER',	 strtolower(TABLE_PREF."MANAGE_TABLE"));
define('TABLE_TREE', strtolower(TABLE_PREF."TREE"));
define('TABLE_TREE_MANAGER', TABLE_PREF."tree_manager");            // 메뉴 - TREE 관리

define('TABLE_CMS_CONTENTS', 'cms_class_menu_info');            // 학과 홈페이지 기본 정보 테이블




// 사용자페이지 메뉴 설정

/*
define('TABLE_BOARD', TABLE_PREF.TABLE_AFF_BOARD.$_GET['board_id']);
define('TABLE_BOARD_NAMO',	TABLE_PREF.TABLE_AFF_BOARD.$_GET['board_id']);
define('TABLE_BOARD_SEQ', TABLE_PREF.TABLE_AFF_BOARD.$_GET['board_id'].'_SEQ');
//define('TABLE_COMMENT',		'aboard_comment_' . $_GET['board_id']);
define('TABLE_COMMENT', TABLE_PREF.'COMMENT');
define('TABLE_FILE',	 TABLE_PREF.'FILE_TABLE');
define('TABLE_TRACKBACK',	TABLE_PREF.'trackback_table');
define('TABLE_CATEGORY', TABLE_PREF.'category_table');

// 김대리 추가////////////////////////////////////
define('TABLE_POLL', TABLE_PREF.'poll');
define('TABLE_POLL_ITEM', TABLE_PREF.'poll_item');
define('TABLE_FOOD_MENU', TABLE_PREF.'food_menu');
define('TABLE_HAKSA_SCHEDULE', TABLE_PREF.'haksa_schedule');
define('TABLE_IPSI_SCHEDULE', TABLE_PREF.'ipsi_schedule');
define('TABLE_DEMAND', TABLE_PREF.'demand');
/////////////////////////////////////////////////




$system_ini['upload_max_filesize'] = ini_get('upload_max_filesize');
$system_ini['file_uploads'] = ini_get('file_uploads');

// Check File Upload
$shorthand = substr($system_ini['upload_max_filesize'], -1);
switch ($shorthand) {
	case 'G' :
		$system_upload_limit = substr($system_ini['upload_max_filesize'], 0, strlen($system_ini['upload_max_filesize']) - 1);
		$system_upload_limit = intval($system_upload_limit) * 1024 * 1024 * 1000;
	break;

	case 'M' :
		$system_upload_limit = substr($system_ini['upload_max_filesize'], 0, strlen($system_ini['upload_max_filesize']) - 1);
		$system_upload_limit = intval($system_upload_limit) * 1024 * 1024;
	break;

	case 'K' :
		$system_upload_limit = substr($system_ini['upload_max_filesize'], 0, strlen($system_ini['upload_max_filesize']) - 1);
		$system_upload_limit = intval($system_upload_limit) * 1024;
	break;

	default :
		$system_upload_limit = intval($system_upload_limit);
}

*/

?>