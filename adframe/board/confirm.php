
<!doctype html>
<html lang="ko">
<head>
    <?
    //
    // +----------------------------------------------------------------------+
    // | XBoard - PHP WebBoard                                                |
    // +----------------------------------------------------------------------+
    // | Copyright (c) 2005 Il Yun <r007m4n@naver.com>                        |
    // +----------------------------------------------------------------------+
    // | idea#1 : fboard0.5 <http://www.fguy.com>                             |
    // | idea#2 : zboard4 <http://www.nzeo.com>                               |
    // +----------------------------------------------------------------------+
    // | Authors: Il Yun <r007m4n@naver.com>                                  |
    // +----------------------------------------------------------------------+
    //
    // $Id: $
    //


    include_once("../include/meta.php");

    ?>
    <title>BIST 부산과학기술대학교(BUSAN INSTITUTE OF SCIENCE AND TECHNOLOGY) 입학안내</title>

    <?
    $get_arg = 'board_id=' . $board_id . '&menu=' . $menu . '&key=' . $key . '&keyword=' . urlencode($keyword) . '&p=' . $p . '&category=' . $category;

    $xdb = DB::connect($dsn);
    if (DB::isError($xdb)) die($xdb->getMessage().'('.__FILE__.':'.__LINE__.')');
    switch ($cmd) {
        case 'modify' :
            redirect('write.php?mode=modify&' . $get_arg . '&no=' . $no);
            break;

        case 'delete' :
            // $sql  = 'DELETE  FROM ' . TABLE_BOARD;
            // $sql .= ' Where BOARD_NO=' . $no;

            $sql  = 'UPDATE ' . TABLE_BOARD;
            $sql .= ' SET DEL_YN=\'Y\' ';
            $sql .= ' Where BOARD_NO=' . $no;

            DBquery($sql);

            echo "<script>alert('삭제되었습니다.');</script>";
            redirect('list.php?' . $get_arg );

        case 'del_comment' :
            //$sql  = "DELETE  From " . TABLE_COMMENT;
            //$sql .= " WHERE BOARD_ID='". $board_id. "' AND COMMENT_NO=". $comment_no;


            $sql  = 'UPDATE ' . TABLE_COMMENT;
            $sql .= ' SET DEL_YN=\'Y\' ';
            $sql .= " WHERE BOARD_ID='". $board_id. "' AND COMMENT_NO=". $comment_no;

            DBquery($sql);


            // 덧글수 Update
            $sql  = 'Select count(*) ';
            $sql .= ' From ' . TABLE_COMMENT;
            $sql .= ' Where PARENT=' . $_GET['no'];
            $sql .= " AND BOARD_ID='" . $board_id."' AND DEL_YN='N' ";

            $comment_num = $xdb->getOne($sql);
            if(is_null($comment_num)) $comment_num = 0;

            $sql  = 'Update ' . TABLE_BOARD;
            $sql .= ' Set ';
            $sql .= ' COMMENT=' . $comment_num;
            $sql .= " Where BOARD_NO='" . $_GET['no'] . "'";
            DBquery($sql);

            DBquery($sql);

            redirect('view.php?' . $get_arg . '&no=' . $_GET['no']);
    }

    echo $cmd;

    $xdb->disconnect();

    ?>
</head>
<body>
</body>
</html>