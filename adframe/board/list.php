<?php
include_once ("_common.php");
?>
<!doctype html>
<html lang="ko">
<head>
    <? include_once("./include/meta.php");?>
    <title>
        <?=$current_menu_name?> &lt; <?=$parent_menu_name?> &lt; <?=$site_title?>
    </title>
</head>
<body>
<!-- total -->
<div id="wrapper">
    <p class="skip-navigation">
        <a href="#container">본문으로 바로가기</a>
    </p>
    <!-- header -->
    <? include_once("../include/header.php");?>
    <!-- //header -->


    <!-- container -->
    <section>
        <div class="container bg0<?=$gubun1?>" id="container">
            <!-- subvisual -->
            <? include_once("../include/subvisual.php");?>
            <!-- //subvisual -->
            <div class="container-area">
                <!-- lnb -->

                <?
                include_once("../include/lnb.php");
                ?>
                <!-- //lnb -->

                <article>
                    <!-- contents -->
                    <div class="contents">

                        <!-- contents navigation -->
                        <div class="contents-title-area">
                            <nav>
                                <p class="contents-navigation">
                                    <span class="icon-home">HOME</span>
                                    <span class="icon-gt">&gt;</span>
                                    <?=$parent_menu_name?>
                                    <span class="icon-gt">&gt;</span>
                                    <strong><?=$current_menu_name?></strong>
                                </p>
                            </nav>

                            <h2>
                                <?=$current_menu_name?>
                            </h2>
                        </div>
                        <!-- //contents navigation -->

                        <!-- contents area -->
                        <div class="contents-area">
                            <div class="board-area">
                            <?
                            if($_GET['cms'] != "admin"){
                                session_start();
                            }

                            //비디오 일때 카테고리 1이 default 로 선택되게 - otep
                            if($board_id=='video')  if($category=='') $category=1;


                            // TODO : 비밀글 제목 감추기 옵션추가
                            $XBOARD_SCRIPT = 'list';

                            $TABLE_BOARD = "xboard_board_".$board_id;

                            require_once($DOCUMENT_ROOT.'/xlib/Pear/HTTP/Upload.php');
                            require_once($DOCUMENT_ROOT.'/xlib/Template_/Template_xboard.php');
                            require_once($DOCUMENT_ROOT.'/xboard/env.php');

                            $etc1 = "";

                            $get_arg = 'board_id=' . $_GET['board_id'] . '&menu='.$_GET['menu'].'&key=' . $_GET['key'] . '&keyword=' . urlencode($_GET['keyword']) . '&p=' . $_GET['p'] . '&category=' . $_GET['category'];



                            //목록보기 권한
                            if($PERM['list'] == 'deny'){
                                alert("목록보기 권한이 없습니다.");
                                if($_SESSION[__MEMBER_LICENSE__]) {
                                   // redirect("/");
                                    exit;
                                } else {
                                   // redirect("login.php?return_url=xboard.php?board_id=".$board_id);
                                    exit;
                                }
                            }

                            $p = $p;
                            if($p == '') $p = 1;
                            $s = ($p - 1) * $conf['ROW_NUM'];


                            $where[] = "B.DEL_YN ='N' ";
                            if($category)	$where[] = "(B.CATEGORY='" . $category . "' or B.NOTICE=1)";

                            if($_GET['sel_type']=="1") {
                                $where[] = "B.TITLE like '%" . $_GET['keyword'] . "%'";
                            } else if($_GET['sel_type']=="2") {
                                $where[] = "B.CONTENTS like '%" . $_GET['keyword'] . "%'";
                            } else if($_GET['sel_type']=="3") {
                                $where[] = "B.WRITER like '%" . $_GET['keyword'] . "%'";
                            }

                            if($_GET['cms'] != 'admin'){
                                //$where[] = "B.FLAG3 = 'Y'";		//공개비공개
                                //$where[] = "B.FLAG4 = 'N'";		//삭제여부
                            }

                            //print_r($where);
                            $sql = 'Select count(*) From ' . $TABLE_BOARD . ' AS B ';
                            if(is_array($where)) $sql .= ' Where ' . array_to_str($where, ' AND ');

                            $total_row = $xdb->getOne($sql);



                            $page_info = array(
                                'url'		=> 'list.php',
                                'total_row'	=> $total_row,
                                'p'			=> $p,
                                'row_num'	=> $conf['ROW_NUM'],
                                'page'		=> $conf['PAGE_INDEX'],
                                'param'		=> $get_arg
                            );


                            $paging = pageLink($page_info);
                            //print_r($paging);
                            $tplAssign['page'] = $paging;
                            //print_r($paging);




                            $list_no = $total_row - ($conf['ROW_NUM'] * ($p-1));

                            if($conf['USE_CATEGORY']) {
                                if($conf['USE_TREE']) {
                                    $sql = "Select * From " . $conf['TREE_ID'] . "_XTREE Where PARENT IN (" . $conf['TREE_PARENT']. ")";
                                    $res = $xdb->query($sql);
                                    $category_list = "<option value=''>카테고리</option>";
                                    while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                                        //echo $category;
                                        $opt = ($category == $row['TREE_NO']) ? 'selected=\"selected\"' : '';
                                        $category_list .= '<option '. $opt . " value='" . $row['TREE_NO'] . "'>" . $row['NAME']."</option>";
                                    }
                                    $tplAssign['category_list'] = $category_list;
                                    $tplAssign['jcategory'] = array('parent' => $cat1, 'child' => $category);
                                }else{
                                    $sql = 'Select * From ' . TABLE_CATEGORY . " Where BOARD_ID='" . $board_id. "'";
                                    $res = $xdb->query($sql);
                                    if($board_id!="photo")	$category_list = "<option value=''>카테고리</option>";
                                    while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {

                                        //echo $category;
                                        $opt = ($category == $row['CATEGORY_NO']) ? 'selected=\"selected\"' : '';
                                        $category_list .= '<option '. $opt . " value='" . $row['CATEGORY_NO'] . "'>" . $row['CATEGORY_NAME']."</option>";
                                    }
                                    $tplAssign['category_list'] = $category_list;

                                }
                            }
                            if($conf['USE_TREE']) {
                                $sql  = 'Select B.BOARD_NO, B.ImageFile, B.CHILD, B.EMAIL, B.DEPTH, B.WRITER, B.CATEGORY, B.TITLE, B.CONTENTS, B.HIT, B.COMMENT, B.USE_SECRET, B.RTIME, B.NOTICE, B.RANK, C.NAME as CATEGORY_NAME, B.FLAG1, B.FLAG2, B.FLAG3, B.FLAG4, B.etc1, B.etc2, B.LINK1 ';
                                $sql .= ' FROM ' . $TABLE_BOARD . ' AS B LEFT JOIN ' . $conf['TREE_ID'] . '_XTREE AS C ';
                                $sql .= ' ON(B.CATEGORY = C.TREE_NO) ';
                            } else {
                                $sql  = 'Select B.BOARD_NO, B.ImageFile, B.CHILD, B.EMAIL, B.DEPTH, B.WRITER, B.CATEGORY, B.TITLE, B.CONTENTS, B.HIT, B.COMMENT, B.USE_SECRET, B.RTIME, B.NOTICE, B.RANK, C.CATEGORY_NAME, B.FLAG1, B.FLAG2, B.FLAG3, B.FLAG4, B.etc1, B.etc2, B.LINK1 ';
                                $sql .= ' FROM ' . $TABLE_BOARD . ' AS B LEFT JOIN ' . TABLE_CATEGORY . ' AS C ';
                                $sql .= ' ON(B.CATEGORY = C.CATEGORY_NO) ';
                                $where[] = "C.BOARD_ID='" . $board_id . "'";
                            }
                            //$sql = " select * from ".$TABLE_BOARD." AS B ";

                            if(is_array($where)) $sql .= ' WHERE ' . array_to_str($where, ' AND ');
                            //$sql .= ' Order By NOTICE DESC, B.RANK DESC, B.PARENT ASC, CHILD ASC ';
                            $sql .= ' ORDER BY NOTICE DESC,  PARENT ASC, DEPTH ASC, CHILD ASC ';

                            $res = $xdb->limitQuery($sql, $s, $conf['ROW_NUM']);

                            $z=0;

                            $skin_type = ($_GET['cms'] == 'admin') ? $conf['ADMIN_SKIN'] : $conf['SKIN'];
                            //print_R($row = $res->fetchRow(DB_FETCHMODE_ASSOC));exit;
                            while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                                //print_r($row);
                                if($z==0) $start = $row['BOARD_NO'];
                                $z++;

                                //print_r($row);

                                unset($depth, $files);

                                $title = $row['TITLE'];

                                if($row['DEPTH'] > 0)
                                    $title = "<span class='icon-reply'>Reply</span>".$title;
                                if($conf['CUT_SIZE'] > 0) $title = cut_str($title, $conf['CUT_SIZE']);
                                if($row['COMMENT'] != 0) $title .= ' <span class=comment-hit>(' . $row['COMMENT'] . ')</span>';
                                if(intval($conf['USE_CATEGORY'])) {
                                    $category = $row['CATEGORY_NAME'];
                                    if($board_id!='video' && $board_id!='brochure'){ //video 게시판이 아닌 게시물 앞에 [카테고리] 붙여준다.
                                        $title = "[".$category."]".$title;
                                    }
                                }

                                /*
                                 * 비밀글. 최고관리자가 아닌경우 비밀변호 확인
                                 */
                                $view_url="view.php?" . $get_arg ;
                                if($row['USE_SECRET']=="1") {
                                    $title = "<a href=#none no=" . $row['BOARD_NO'] . ">" . $title . '</a>';
                                } else {
                                    $title = "<a href='./view.php?" . $get_arg ."&no=" . $row['BOARD_NO'] . "'>" . $title . '</a>';
                                }
                                //$contents = ($row['USE_HTML'] || $conf['USE_EDITOR']) ? $row['CONTENTS'] : nl2br($row['CONTENTS']);
                               $plink = "<a href='./view.php?" . $get_arg ."&menu=".$menu. "&no=" . $row['BOARD_NO'] . "'>";
                                $plink2 = "<a href='./list.php?" . $get_arg . "&no=" . $row['BOARD_NO'] . "'>";	//	사이버홍보관용
                                for($i=0; $i<$row['DEPTH']; $i++) {
                                    $depth .= '&nbsp;&nbsp;';
                                }

                                $siteid = $row['etc2'];

                                $newicon = ($row['RTIME'] > time() - (3600*24)) ? '1' : '0';
                                $u_images = "";
                                if($row['ImageFile'] != "")
                                    $u_images = "<img src='/xvar/ucc/ThumbnailImg/".$row['ImageFile']."' width='96' height='64' border=0>";


                                $sql = 'Select * From ' . TABLE_FILE . " Where BOARD_ID='" . $board_id . "' AND PARENT='" . $row['BOARD_NO'] . "'";
                                $fres = $xdb->query($sql);

                                $s_images = "";
                                $p_images = "";
                                $w_images = "";
                                $file_down = "";

                                while($frow = $fres->fetchRow(DB_FETCHMODE_ASSOC)) {

                                    $img = (is_file(dirname(__FILE__)."/icon/".strtolower(get_ext($frow['ORIGINAL_NAME']).".gif"))) ? $xboard['base_url']."/icon/".strtolower(get_ext($frow['ORIGINAL_NAME'])).".gif" : $xboard['base_url']."/icon/unknown.gif";

                                    $ext_img = "<a href='download.php?file_no=" . $frow['FILE_NO'] . "&board_id=".$board_id."'><img src='$img' style='cursor:hand' alt='" .$frow['ORIGINAL_NAME'] . "' border=0 align=absmiddle></a>";

                                    //$file_down = "<a href='download.php?file_no=" . $frow['FILE_NO'] . "'>" . $row['TITLE'] . "</a>";

                                    if($skin_type=="data") {
                                        if(is_file("../xvar/DATA/".$board_id.'/'.$frow['FILE_NAME'])) {
                                            $file_down = "<a href='download.php?file_no=" . $frow['FILE_NO'] . "'><img src=\"/conf/make_img/board/icon_file.gif\" alt=\"첨부파일\" /></a>";
                                        }
                                    }

                                    $files[] = array(
                                        'img'		=> $ext_img
                                    );

                                    //$images = '/xvar/DATA/' . $board_id . '/' . $frow['FILE_NAME'];

                                    if(eregi('image', $frow['FILE_TYPE']) && $frow['REMARKS']=="T") {
                                        $imgC = new ImgClass(155,101);
                                        $size = @getimagesize($upload_dir . '/' . $board_id . '/' . $frow['FILE_NAME']);
                                        $imgC->imageResize($size[0], $size[1]);
                                        //echo $board_id;
                                        //if(ereg("^bodo$|^gallery$|^youngsang$", $board_id)) $upload_dir_web = "http://ipsi.mnu.ac.kr".$upload_dir_web;

                                        $s_images = "<img src='/xvar/DATA/" . $board_id . '/' . $frow['FILE_NAME'] . "' width=$size[0] height=$size[1] border=0 alt='$row[TITLE]'>";	//입시갤러리용
                                        if($skin_type=="brochure") {
                                            if(is_file("../xvar/DATA/".$siteid.'/'.$frow['ORIGINAL_NAME'])) {
                                                $p_images = "<img src='/xvar/DATA/".$siteid."/".$frow['ORIGINAL_NAME']."' alt='$row[TITLE]' class='image-brochure' />";
                                            } else {
                                                $p_images = "<img src='/xvar/DATA/".$board_id."/".$frow['FILE_NAME']."'  alt='$row[TITLE]' class='image-brochure'/>";
                                            }
                                        }  else {
                                            if(is_file("../xvar/DATA/".$siteid.'/'.$frow['ORIGINAL_NAME'])) {
                                                $p_images = "<img src='/xvar/DATA/".$siteid."/".$frow['ORIGINAL_NAME']."' border='0' width='155' height='101' alt='$row[TITLE]'/>";
                                            } else {
                                                $p_images = "<img src='/xvar/DATA/".$board_id."/".$frow['FILE_NAME']."' border='0' width='155' height='101' alt='$row[TITLE]'/>";
                                            }
                                        }

                                        $imgC = new ImgClass(155,101);
                                        $size = @getimagesize($upload_dir . '/' . $board_id . '/' . $frow['FILE_NAME']);
                                        $imgC->imageResize($size[0], $size[1]);
                                        //echo $board_id;
                                        //if(ereg("^bodo$|^gallery$|^youngsang$", $board_id)) $upload_dir_web = "http://ipsi.mnu.ac.kr".$upload_dir_web;

                                        if($size[0] > 0)
                                            $w_images = "<img src='/xvar/DATA/" . $board_id . '/' . $frow['FILE_NAME'] . "' width=$size[0] height=$size[1] border=0 alt='$row[TITLE]'>";	//대학웹진형

                                        $imgC = new ImgClass(155,101);
                                        $size = @getimagesize($upload_dir . '/' . $board_id . '/' . $frow['FILE_NAME']);
                                        $imgC->imageResize($size[0], $size[1]);
                                        //echo $board_id;
                                        //if(ereg("^bodo$|^gallery$|^youngsang$", $board_id)) $upload_dir_web = "http://ipsi.mnu.ac.kr".$upload_dir_web;

                                        if($size[0] > 0)
                                            $images_6745 = "<img src='/xvar/DATA/" . $board_id . '/' . $frow['FILE_NAME'] . "' width=$size[0] height=$size[1] border=0 alt='$row[TITLE]'>";	//사이버홍보관
                                    }

                                    if($skin_type=="gallery") {
                                        if(is_file("../xvar/DATA/".$siteid.'/'.$frow['ORIGINAL_NAME'])) {
                                            $p_images = "<img src='/xvar/DATA/".$siteid."/".$frow['ORIGINAL_NAME']."' border='0' width='155' height='101' alt='$row[TITLE]'>";
                                        } else {
                                            if($board_id == "depart_ot_photo" || $board_id == "depart_tc_photo" || $board_id = "depart_hc_photo"){
                                                $p_images = "<img src='/xvar/DATA/".$board_id."/".$frow['FILE_NAME']."' border='0' width='150' height='101' alt='$row[TITLE]'>";
                                            }else{
                                                $p_images = "<img src='/xvar/DATA/".$board_id."/".$frow['FILE_NAME']."' border='0' width='155' height='101' alt='$row[TITLE]'>";
                                            }
                                        }
                                    }

                                }

                                if($p_images == "") $p_images = $u_images;
                                $category = cut_str(str_replace(' ', '', $category),18, 'no');
                                $writer = cut_str(str_replace(' ', '', $row['WRITER']),12, 'no');
                                //$contents = cut_str(htmlspecialchars(strip_tags(stripcslashes(str_replace('&nbsp;', '', $row['CONTENTS'])))),160,'no');

                                $flag1 = $row['FLAG1'];	//메인표시여부
                                if($flag1=="Y")
                                    $flag1 = "<a href='action.php?mode=flag1&no=".$row['BOARD_NO']."&yn=N'><B>O</B></a>";
                                else
                                    $flag1 = "<a href='action.php?mode=flag1&no=".$row['BOARD_NO']."&yn=Y'><B>X</B></a>";

                                $flag2 = $row['FLAG2'];		//메인 썸네일 표시여부
                                if($flag2=="Y")
                                    $flag2 = "<a href='action.php?mode=flag2&no=".$row['BOARD_NO']."&yn=N'><B>O</B></a>";
                                else
                                    $flag2 = "<a href='action.php?mode=flag2&no=".$row['BOARD_NO']."&yn=Y'><B>X</B></a>";

                                $flag3 = $row['FLAG3'];	 //공개, 비공개
                                if($flag3=="Y")
                                    $flag3 = "<a href='action.php?mode=flag3&no=".$row['BOARD_NO']."&yn=N'><B>O</B></a>";
                                else
                                    $flag3 = "<a href='action.php?mode=flag3&no=".$row['BOARD_NO']."&yn=Y'><B>X</B></a>";

                                // TODO : new icon 여부추가
                                //print_r($row);

                                //echo $place;

                                $list[] = array(
                                    'list_no'	=> $list_no,
                                    'no'		=> $row['BOARD_NO'],
                                    'depth'		=> $depth,
                                    'writer'	=> $writer,
                                    'category'	=> $category,
                                    'title'		=> stripslashes($title),
                                    'title2'    => $row['TITLE'],
                                    'plink'		=> $plink,
                                    'plink2'	=> $plink2,
                                    'email'		=> $row['EMAIL'],
                                    'title_file'	=> $file_down,
                                    //'contents'	=> stripslashes($contents),
                                    'hit'		=> $row['HIT'],
                                    'notice'	=> $row['NOTICE'],
                                    'newicon'	=> $newicon,
                                    'files'		=> $files,
                                    'images'	=> $images,
                                    's_images'	=> $s_images,
                                    'p_images'	=> $p_images,
                                    'u_images'	=> $u_images,
                                    'w_images'	=> $w_images,
                                    'images_6745'	=> $images_6745,
                                    'secret'	=> intval($row['USE_SECRET']),
                                    'date'		=> date('Y.m.d', $row['RTIME']),
                                    'sdate'		=> date('y.m.d', $row['RTIME']),
                                    'RANK'		=> $row['RANK'],
                                    'flag1'		=> $flag1,
                                    'flag2'		=> $flag2,
                                    'flag3'		=> $flag3,
                                    'etc1'     => $etc1,
                                    'etc2'    => $row['ETC2'],
                                    'conhost' => $row['conhost'],
                                    'conterm' => $row['conterm']
                                );

                                $list_no--;
                                $end = $row['BOARD_NO'];


                            }


                            $skin = ($_GET['cms'] == 'admin') ? $conf['ADMIN_SKIN'] : $conf['SKIN'];
                            $skin_path = "/xboard/skin/".$skin."/board.php";
                            include_once($DOCUMENT_ROOT.$skin_path);
                            ?>

                                <!-- manager information -->
                                <? if($damdang_on=='Y') include('../include/page_information.php'); ?>
                                <!-- //manager information -->
                            </div>
                        </div>
                        <!-- //contents area -->
                    </div>
                    <!-- //contents -->
                </article>
            </div>

        </div>
    </section>
    <!-- //container -->

    <!-- footer -->
    <? include_once("../include/footer.php");?>
    <!-- //footer -->
</div>
<!-- //total -->

<script type="text/javascript">
    initTopMenu(<?=$gubun1?>);
    top2menuView(<?=$gubun1?>);
    setMenuOn(<?=$gubun2?>);
</script>

</body>
</html>
