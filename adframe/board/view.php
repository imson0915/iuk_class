<?php
/**
 * User: otep
 * Date: 2015-07-13 오후 7:13
 * Description : 콘텐츠 타입 메뉴의 내용을 보여준다.
 */

?>

<!doctype html>
<html lang="ko">
<head>
    <? include_once("../include/meta.php");?>
    <title>
        <?=$current_menu_name?> &lt; <?=$parent_menu_name?> &lt; <?=$site_title?>
    </title>
</head>
<body>
<!-- total -->
<div id="wrapper">
    <p class="skip-navigation">
        <a href="#container">본문으로 바로가기</a>
    </p>
    <!-- header -->
    <? include_once("../include/header.php");?>
    <!-- //header -->


    <!-- container -->
    <section>
        <div class="container bg0<?=$gubun1?>" id="container">
            <!-- subvisual -->
            <? include_once("../include/subvisual.php");?>
            <!-- //subvisual -->
            <div class="container-area">
                <!-- lnb -->

                <?
                include_once("../include/lnb.php");
                ?>
                <!-- //lnb -->

                <article>
                    <!-- contents -->
                    <div class="contents">

                        <!-- contents navigation -->
                        <div class="contents-title-area">
                            <nav>
                                <p class="contents-navigation">
                                    <span class="icon-home">HOME</span>
                                    <span class="icon-gt">&gt;</span>
                                    <?=$parent_menu_name?>
                                    <span class="icon-gt">&gt;</span>
                                    <strong><?=$current_menu_name?></strong>
                                </p>
                            </nav>

                            <h2>
                                <?=$current_menu_name?>
                            </h2>
                        </div>
                        <!-- //contents navigation -->

                        <!-- contents area -->
                        <div class="contents-area">
                            <div class="board-area">
                            <?php
                            //error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT ^ E_DEPRECATED);
                            ini_set('display_errors', 'On');


                            // TODO : 비밀글 제목 감추기 옵션추가
                            $XBOARD_SCRIPT = 'view';

                            require_once($DOCUMENT_ROOT.'/xlib/Template_/Template_xboard.php');
                            require_once($DOCUMENT_ROOT.'/xlib/wec4decoder.php');
                            require_once($DOCUMENT_ROOT.'/xboard/env.php');

                            if($_GET['board_id'] == '') redirect($xboard['error_url']);

                            //내용보기 권한
                            if($PERM['view'] == 'deny'){
                                alert("내용보기 권한이 없습니다.");
                                if($_SESSION[__MEMBER_LICENSE__]) {
                                    redirect("list.php?board_id=".$board_id."&menu=".$menu);
                                    exit;
                                } else {
                                    //redirect("login.php?return_url=xboard.php?board_id=".$board_id);
                                    exit;
                                }
                            }

                            $sql  = 'Select * ';
                            $sql .= ' From ' . TABLE_BOARD . ' AS B LEFT JOIN ' . TABLE_CATEGORY . ' AS C ';
                            $sql .= ' ON(B.CATEGORY = C.CATEGORY_NO) ';
                            $sql .= " Where board_no='" . $_GET['no'] . "'";
                            $row = DBarray($sql);

                            // TODO : 비밀글이면 읽기 권한이 있는지 확인

                            //if(intval($row['USE_SECRET']) && $_SESSION['__BOARD_SECRET_' . $_GET['no'] . '__'] != $row['PASSWD'] ) {
                            //	redirect("confirm.php?" . $get_arg . '&mode=secret_view&no=' . $_GET['no']);
                            //}

                            //print_r($row);
                            //echo $row['USE_HTML'];exit;
                            $contents = (intval($row['USE_HTML'])) ? $row['CONTENTS'] : nl2br($row['CONTENTS']);
                            $ETC1 = (intval($row['USE_HTML'])) ? nl2br($row['ETC1']) : nl2br($row['ETC1']);
                            if($row['LINK1']) $link1 = "<a target='_blank' href='" . $row['LINK1'] . "'>" . $row['LINK1'] . "</a>";
                            if($row['LINK2']) $link2 = "<a target='_blank' href='" . $row['LINK2'] . "'>" . $row['LINK2'] . "</a>";

                            // 조회수 증가
                            if($_SESSION['__view_' . $_GET['no'] . '__'] != 'checked') {
                                $sql = 'Update ' . TABLE_BOARD . ' SET HIT=HIT+1 Where BOARD_NO=' . $_GET['no'];
                                DBquery($sql);
                                set_session("__view_".$_GET['no']."__", "checked");
                                //session_register('__view_' . $_GET['no'] . '__');
                                $_SESSION['__view_' . $_GET['no'] . '__'] = 'checked';
                            }

                            // 수정삭제 권한
                            // otep 수정 -1 빼버림..
                            if($row['MEMBER_LICENSE'] == $_SESSION['__MEMBER_LICENSE__'] ||  BOARD_ADMIN  || $_SESSION['__MEMBER_POSITION__']==1) {
                                $PERM['modify'] = 'allow';
                                $PERM['delete'] = 'allow';
                            } else {
                                $PERM['modify'] = 'deny';
                                $PERM['delete'] = 'deny';
                            }
                            //print_r($PERM);
                            $tplAssign['PERM'] = $PERM;
                            //print_r($PERM);

                            $category_name = $row['CATEGORY_NAME'];
                            if($conf['USE_TREE']) {
                                $sql = 'Select NAME FROM ' . $conf['TREE_ID'] . "_XTREE Where TREE_NO='" . $row['CATEGORY'] . "'";
                                $category_name = sql_query($sql);
                                $category_name = $category_name['NAME'];
                            }

                            $siteid = $row[etc2];

                            $contents = str_replace("<BR>","<br/>",$contents);
                            $contents = str_replace("<BR >","<br/>",$contents);
                            $contents = str_replace("<br>","<br/>",$contents);
                            $contents = str_replace("<br >","<br/>",$contents);
                            $contents = @preg_replace("/(\<br )([^\>]*)(\>)/i", "\\1 \\2 /\\3", $contents);
                            $contents = @preg_replace("/(\<img )([^\>]*)(\>)/i", "\\1 \\2 /\\3", $contents);
                            $contents = str_replace("//>","/>",$contents);
                            //$contents = preg_replace("(\<(/?[^\>]+)\>)", "", $contents);
                            $contents = str_replace("https","http",$contents);
                            $view = array(
                                'category_name'		=> $category_name,
                                'writer'			=> stripslashes($row['WRITER']),
                                'email'				=> $row['EMAIL'],
                                'homepage'			=> $row['HOMEPAGE'],
                                'title'				=> stripslashes($row['TITLE']),
                                'board_num'			=> $row['BOARD_NO'],
                                'VideoFile'			=> $row['VideoFile'],
                                'ImageFile'			=> $row['ImageFile'],
                                'FileSize'			=> $row['FileSize'],
                                'PlayTime'			=> $row['PlayTime'],
                                'TABLE_BOARD_name'	=> TABLE_BOARD,
                                'contents'			=> stripslashes($contents),
                                'RANK'				=> $row['RANK'],
                                'link1'				=> $link1,
                                'link2'				=> $link2,
                                'etc1'				=> stripslashes($ETC1),
                                'etc2'				=> $etc2,
                                'hit'				=> $row['HIT'],
                                'ip'				=> $row['REMOTE_ADDR'],
                                'date'				=> date('Y.m.d H:i:s', $row['RTIME']),
                                'conhost' => $row['conhost'],
                                'conterm' => $row['conterm'],
                                'condate' => $row['condate'],
                                'contel' => $row['contel'],
                                'conoutline' => $row['conoutline'],
                                'confee' => $row['confee']
                            );

                            $sql  = 'Select * ';
                            $sql .= ' From ' . TABLE_FILE;
                            $sql .= " Where BOARD_ID='" . $_GET['board_id'] . "' AND PARENT= '" . $_GET['no'] . "'";
                            $res = DBquery($sql);
                            $imgUrl = '/xvar/DATA/';
                            //echo $sql;
                            if($siteid == "")
                                $siteid = $_GET['board_id'];
                            $images = "<table width='100%'>";
                            while($row = mysql_fetch_array($res)) {
                                if($siteid) {
                                    $file_name = "<a href='download.php?file_no=" . $row['FILE_NO'] . "&board_id=" . $_GET['board_id'] . "&siteid=".$siteid."' class='add-file'>" . $row['ORIGINAL_NAME'] ." [". formatSizeUnits($row['FILE_SIZE']). "]</a>";
                                } else {
                                    $file_name = "<a href='download2.php?filename=".$row['ORIGINAL_NAME']."&filename2=".$row['FILE_NAME']."&board_id=" . $_GET['board_id']."' class='add-file'>" . $row['ORIGINAL_NAME'] ." [". formatSizeUnits($row['FILE_SIZE']). "]</a>";
                                }
                                if($row['REMARKS'] != "T"){
                                    $download[] = array(
                                        'no'			=> $row['FILE_NO'],
                                        'name'			=> $file_name,
                                        'size'			=> convert_size($row['FILE_SIZE'])
                                    );

                                    if(eregi('gif', $row['FILE_TYPE']) || eregi('jpg', $row['FILE_TYPE'])) {
                                        $imgC = new ImgClass(460,1000);
                                        if(is_file($upload_dir . '/' . $siteid . '/' . $row['ORIGINAL_NAME'])) {
                                            $size = @getimagesize($upload_dir . '/' . $siteid . '/' . $row['ORIGINAL_NAME']);
                                            $imgC->imageResize($size[0], $size[1]);

                                            $images .= "<tr><td width='100%' style='padding:5px'><div align=center><img src='".$imgUrl. $siteid . '/' . $row['ORIGINAL_NAME'] . "' style='cursor:hand' onclick='real_size(this.src)' width=$size[0] height=$size[1] alt='$row[FILE_TEXT]'/></div></td></tr>";
                                        } else {
                                            $size = @getimagesize($upload_dir . '/' . $_GET['board_id'] . '/' . $row['FILE_NAME']);
                                            $imgC->imageResize($size[0], $size[1]);

                                            $images .= "<tr><td width='100%' style='padding:5px'><div align=center><img src='".$imgUrl. $_GET['board_id'] . '/' . $row['FILE_NAME'] . "' style='cursor:hand' onclick='real_size(this.src)' width=$size[0] height=$size[1] alt='$row[FILE_TEXT]'/></div></td></tr>";

                                        }

                                        $imgC = new ImgClass(130,85);
                                        if(is_file($upload_dir . '/' . $siteid . '/' . $row['ORIGINAL_NAME'])) {
                                            $size = @getimagesize($upload_dir . '/' . $siteid . '/' . $row['ORIGINAL_NAME']);
                                            $imgC->imageResize($size[0], $size[1]);

                                            $s_images = "<img src='".$imgUrl. $siteid . '/' . $row['ORIGINAL_NAME'] . "' style='cursor:hand' width=$size[0] height=$size[1] alt='$row[FILE_TEXT]' />";
                                        } else {
                                            $size = @getimagesize($upload_dir . '/' . $_GET['board_id'] . '/' . $row['FILE_NAME']);
                                            $imgC->imageResize($size[0], $size[1]);

                                            $s_images = "<img src='".$imgUrl. $_GET['board_id'] . '/' . $row['FILE_NAME'] . "' style='cursor:hand' width=$size[0] height=$size[1] alt='$row[FILE_TEXT]' />";
                                        }
                                    }
                                }
                            }
                            $images .= "</table>";

                            $sql  = 'Select * ';
                            $sql .= ' From ' . TABLE_FILE;
                            $sql .= " Where BOARD_ID='" . $_GET['board_id'] . "' AND PARENT= '" . $_GET['no'] . "' order by FILE_NO asc ";
                            //	echo $sql;
                            $res = DBquery($sql);
                            $k=0;
                            while($row = mysql_fetch_array($res)) {

                                if(!$row[REMARKS] && (eregi('gif', $row['FILE_TYPE']) || eregi('jpg', $row['FILE_TYPE']) || eregi('image', $row['FILE_TYPE']))) {
                                    if(is_file("../xvar/DATA/".$siteid.'/'.$row['ORIGINAL_NAME'])) {
                                        $board_img_src .= "<center><a href='/xvar/DATA/".$siteid."/".$row['ORIGINAL_NAME']."' target='_blank'><img src='".$imgUrl.$siteid."/".$row['ORIGINAL_NAME']."'  onload='javascript:if(this.width>600){this.width=600;}' alt='$row[FILE_TEXT]' /></a></center><br/>";
                                    } else {
                                        $board_img_src .= "<center><a href='/xvar/DATA/".$_GET['board_id']."/".$row['FILE_NAME']."' target='_blank'><img src='".$imgUrl.$_GET['board_id']."/".$row['FILE_NAME']."'  onload='javascript:if(this.width>600){this.width=600;}' alt='$row[FILE_TEXT]'/></a></center><br/>";
                                    }
                                }
                                $k++;
                            }


                            $tplAssign['file'] = $download;

                            $view['images'] = $images;
                            $view['s_images'] = $s_images;

                            if($board_id == "board1")	{
                                $view['board_img_src'] = "";
                            }else{
                                $view['board_img_src'] = $board_img_src;
                            }


                            $view['board_img2_src'] = $board_img2_src;
                            $tplAssign['view'] = $view;

                            // comment
                            $sql  = 'Select * ';
                            $sql .= ' From ' . TABLE_COMMENT;
                            $sql .= " Where BOARD_ID='" . $_GET['board_id']."'";
                            $sql .= '   AND     PARENT=' . $_GET['no'];
                            $sql .= ' AND DEL_YN=\'N\' ';
                            $sql .= ' order by COMMENT_NO ASC';
                            $res = DBquery($sql);

                            //otep 추가
                            $get_arg = 'board_id=' . $_GET['board_id'] . '&menu=' . $_GET['menu'] . '&key=' . $_GET['key'] . '&keyword=' . urlencode($_GET['keyword']) . '&p=' . $_GET['p'] . '&category=' . $_GET['category'];


                            while($row = @mysql_fetch_array($res)) {
                                $comment[] = array(
                                    'no'			=> $row['COMMENT_NO'],
                                    'delete'		=> 'confirm.php?' . $get_arg . '&no=' . $_GET['no'] . '&mode=cmt_del&comment_no=' . $row['COMMENT_NO'],
                                    'writer'		=> $row['WRITER'],
                                    'ip'			=> $row['REMOTE_ADDR'],
                                    'memo'			=> nl2br($row['MEMO']),
                                    'date'			=> date('y.m.d H:i:s', $row['RTIME'])
                                );
                            }
                            $tplAssign['comment'] = $comment;


                            // TODO : 조회수 증가


                            //이전글 가져오기
                            $PrevSql = "SELECT BOARD_NO,TITLE FROM xboard_board_$board_id WHERE DEL_YN='N' AND BOARD_NO < '$no' ORDER BY BOARD_NO DESC LIMIT 0, 1";
                            $Prev = DBarray($PrevSql);

                            //다음글 가져오기
                            $NextSql = "SELECT BOARD_NO,TITLE FROM xboard_board_$board_id WHERE DEL_YN='N' AND BOARD_NO > '$no' ORDER BY BOARD_NO ASC LIMIT 0, 1";
                            //print_R($NextSql);
                            $Next = DBarray($NextSql);

                            $skin = ($_GET['cms'] == 'admin') ? $conf['ADMIN_SKIN'] : $conf['SKIN'];

                             $plink = "list.php?" . $get_arg ."&menu=".$menu. "&no=" . $row['BOARD_NO'] ;


                            $skin_path = "/xboard/skin/".$skin."/view.php";
                            include_once($DOCUMENT_ROOT.$skin_path);



                            ?>
                            </div>

                        </div>
                        <!-- //contents area -->
                    </div>
                    <!-- //contents -->
                </article>
            </div>

        </div>
    </section>
    </div>
    <!-- //container -->

    <!-- footer -->
    <? include_once("../include/footer.php");?>
    <!-- //footer -->
</div>
<!-- //total -->

<script type="text/javascript">
    initTopMenu(<?=$gubun1?>);
    top2menuView(<?=$gubun1?>);
    setMenuOn(<?=$gubun2?>);
</script>

</body>
</html>