<?
//
// +----------------------------------------------------------------------+
// | XBoard - PHP WebBoard                                                |
// +----------------------------------------------------------------------+
// | Copyright (c) 2005 Il Yun <r007m4n@naver.com>                        |
// +----------------------------------------------------------------------+
// | idea#1 : fboard0.5 <http://www.fguy.com>                             |
// | idea#2 : zboard4 <http://www.nzeo.com>                               |
// +----------------------------------------------------------------------+
// | Authors: Il Yun <r007m4n@naver.com>                                  |
// +----------------------------------------------------------------------+
//
// $Id: $
//

// TODO : 비밀글 제목 감추기 옵션추가
$XBOARD_SCRIPT = 'comment';

include_once("../include/meta.php");
require_once($DOCUMENT_ROOT.'/xlib/Template_/Template_xboard.php');
require_once($DOCUMENT_ROOT.'/xlib/wec4decoder.php');
require_once($DOCUMENT_ROOT.'/xboard/env.php');


$writer = ($LICENSE_NO) ? $user_info['NAME'] : del_tag(trim($_POST['writer']));
$passwd = ($LICENSE_NO) ? $user_info['PASSWD'] : md5(md5($_POST['passwd']));
$memo = del_tag(trim($_POST['memo']));
$member_license = ($LICENSE_NO) ? $LICENSE_NO : -1;

$writer1 = $writer;
$passwd1 = $passwd;
$memo1 = $memo;
$LICENSE_NO1 = 1;

$time =time();
$sql ="INSERT INTO ".TABLE_COMMENT." (PARENT, BOARD_ID, WRITER, PASSWD, REMOTE_ADDR, MEMBER_LICENSE, MEMO, RTIME) VALUES";
$sql .= "( $_GET[no], '$board_id' , '$writer','$passwd','$_SERVER[REMOTE_ADDR]', '$LICENSE_NO', '$memo','$time'  ) ";

DBquery($sql);
//print_r($error);

// 덧글수 Update
$sql  = 'Select count(*) ';
$sql .= ' From ' . TABLE_COMMENT;
$sql .= ' Where PARENT=' . $_GET['no'];
$sql .= " AND BOARD_ID='" . $board_id."' AND DEL_YN='N' ";
$comment_num = $xdb->getOne($sql);
if(is_null($comment_num)) $comment_num = 0;

$sql  = 'Update ' . TABLE_BOARD;
$sql .= ' Set ';
$sql .= ' COMMENT=' . $comment_num;
$sql .= " Where BOARD_NO='" . $_GET['no'] . "'";
DBquery($sql);

redirect($_SERVER['HTTP_REFERER']);
?>
