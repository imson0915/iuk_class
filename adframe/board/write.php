
<!doctype html>
<html lang="ko">
<head>
    <? include_once("../include/meta.php");?>
    <title>
        <?=$current_menu_name?> &lt; <?=$parent_menu_name?> &lt; <?=$site_title?>
    </title>
</head>
<body>
<!-- total -->
<div id="wrapper">
    <p class="skip-navigation">
        <a href="#container">본문으로 바로가기</a>
    </p>
    <!-- header -->
    <? include_once("../include/header.php");?>
    <!-- //header -->


    <!-- container -->
    <section>
        <div class="container bg0<?=$gubun1?>" id="container">
            <!-- subvisual -->
            <? include_once("../include/subvisual.php");?>
            <!-- //subvisual -->
            <div class="container-area">
                <!-- lnb -->

                <?
                include_once("../include/lnb.php");
                ?>
                <!-- //lnb -->

                <article>
                    <!-- contents -->
                    <div class="contents">

                        <!-- contents navigation -->
                        <div class="contents-title-area">
                            <nav>
                                <p class="contents-navigation">
                                    <span class="icon-home">HOME</span>
                                    <span class="icon-gt">&gt;</span>
                                    <?=$parent_menu_name?>
                                    <span class="icon-gt">&gt;</span>
                                    <strong><?=$current_menu_name?></strong>
                                </p>
                            </nav>

                            <h2>
                                <?=$current_menu_name?>
                            </h2>
                        </div>
                        <!-- //contents navigation -->

                        <!-- contents area -->
                        <div class="contents-area">
                            <div class="board-area">
                                <?php
                                //session_start();
                                $XBOARD_SCRIPT = 'write';

                                require_once($DOCUMENT_ROOT.'/xetc/config.php');
                                require_once($DOCUMENT_ROOT.'/xlib/Template_/Template_xboard.php');
                                require_once($DOCUMENT_ROOT.'/xlib/wec4decoder.php');
                                require_once($DOCUMENT_ROOT.'/xboard/env.php');

                                // Check BOARD_ID
                                if($_GET['board_id'] == '') redirect($xboard['error_url']);

                                //글쓰기 권한
                                if($PERM['write'] == 'deny'){
                                    alert("글쓰기 권한이 없습니다.");
                                    redirect("board_list.php?board_id=".$board_id."&menu=".$menu);
                                    exit;
                                }

                                ////답급쓰기 권한
                                if($PERM['reply'] == 'deny' && $PERM['write'] == 'deny'){
                                	alert("답글쓰기 권한이 없습니다.");
                                    redirect("board_list.php?board_id=".$board_id."&menu=".$menu);
                                	exit;
                                }

                                //echo $board_id;
                                //depart_idd_gallery

                                $get_arg = 'board_id=' . $_GET['board_id'] . '&menu='.$_GET['menu'].'&key=' . $_GET['key'] . '&keyword=' . urlencode($_GET['keyword']) . '&p=' . $_GET['p'] . '&category=' . $_GET['category'];


                                $tplAssign['USE_CATEGORY'] = $conf['USE_CATEGORY'];
                                $tplAssign['USE_LINK'] = $conf['USE_LINK'];
                                $tplAssign['USE_HTML'] = $conf['USE_HTML'];
                                $tplAssign['USE_SECRET'] = $conf['USE_SECRET'];
                                $tplAssign['USE_TREE'] = $conf['USE_TREE'];
                                $tplAssign['USE_TRACKBACK'] = $conf['USE_TRACKBACK'] && function_exists('fsockopen');
                                $tplAssign['USE_PDS'] = $conf['USE_PDS'] && ini_get('file_uploads');
                                $tplAssign['UPLOAD_MAX_FILESIZE'] = $conf['UPLOAD_MAX_FILESIZE'];
                                $tplAssign['USE_UCC'] = $conf['USE_UCC'];
                                $tplAssign['USE_OPEN'] = $conf['USE_OPEN'];
                                $tplAssign['USE_STATUS'] = $conf['USE_STATUS'];
                                $tplAssign['USE_NOTICE'] = $conf['USE_NOTICE'];

                                switch ($_GET['mode']) {
                                    case 'modify':
                                    case 'reply':

                                        $sql  = 'Select * From ' . TABLE_BOARD;
                                        $sql .= " Where BOARD_NO=" . $_GET['no'];
                                        $row = $xdb->getRow($sql, DB_FETCHMODE_ASSOC);
                                        $category = $row['CATEGORY'];
                                        if($conf['USE_TREE']) {
                                            $sql = 'Select PARENT FROM ' . $conf['TREE_ID'] . "_XTREE Where TREE_NO='" . $row['CATEGORY'] . "'";
                                            $tree_parent = $xdb->getOne($sql);
                                            //$sql = 'Select TREE_NO FROM ' . $conf['TREE_ID'] . "_XTREE Where PARENT='" . $tree_parent . "'";
                                            //$parent_no = $xdb->getOne($sql);
                                            $tplAssign['jcategory'] = array('parent' => $tree_parent, 'child' => $row['CATEGORY']);
                                        }



                                        if($_GET['mode'] == 'modify') {
                                            // 비밀글 수정
                                            if(intval($row['USE_SECRET']) && $_SESSION['__BOARD_SECRET_' . $_GET['no'] . '__'] != $row['PASSWD'] && $_GET['cms'] != 'admin') {
                                                if($_SESSION['__ADMIN_LICENSE__'] == ""){
                                                    //otep 주석처리.. ajax로 확인
                                                    //redirect("confirm.php?" . $get_arg . '&mode=secret_modify&no=' . $_GET['no']);
                                                }
                                            }


                                            $board_num = $row['BOARD_NO'];
                                            $writer = str_replace('"', '&quot', $row['WRITER']);
                                            $email = $row['EMAIL'];
                                            $homepage = $row['HOMEPAGE'];
                                            $link1 = $row['LINK1'];
                                            $link2 = $row['LINK2'];
                                            $title = str_replace('"', '&quot', $row['TITLE']);
                                            //$contents = htmlspecialchars($row['CONTENTS']);
                                            $contents = $row['CONTENTS'];
                                            $contents = str_replace("\\","",$contents);
                                            $use_secret = (intval($row['USE_SECRET'])) ? 'checked' : '';
                                            $use_html = (intval($row['USE_HTML'])) ? 'checked' : '';
                                            $notice = (intval($row['NOTICE'])) ? 'checked' : '';
                                            $VideoFile = $row['VideoFile'];
                                            $ImageFile = $row['ImageFile'];
                                            $FileSize = $row['FileSize'];
                                            $PlayTime = $row['PlayTime'];
                                            $RANK = $row['RANK'];
                                            $date_in = date('Y-m-d H:m:s', $row['RTIME']);
                                            $hit_in = $row['HIT'];
                                            $etc1 = $row['ETC1'];
                                            $conhost = $row['conhost'];
                                            $conterm = $row['conterm'];
                                            $condate = $row['condate'];
                                            $contel = $row['contel'];
                                            $conoutline= $row['conoutline'];
                                            $confee = $row['confee'];

                                            // Download
                                            $sql  = 'Select * ';
                                            $sql .= ' From ' . TABLE_FILE;
                                            $sql .= " Where BOARD_ID='" . $_GET['board_id'] . "' AND PARENT=" . $_GET['no'];
                                            //echo $sql;
                                            $res = $xdb->query($sql);
                                            while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                                                $file_name = "<a href='download.php?file_no=" . $row['FILE_NO'] . "&board_id=" . $_GET['board_id'] . "'  alt='".$row['FILE_TEXT']."'  class=add-file>" . $row['ORIGINAL_NAME'] . "</a>";
                                                $download[] = array(
                                                    'no'			=> $row['FILE_NO'],
                                                    'name'			=> $file_name,
                                                    'remarks'			=> $row['REMARKS'],
                                                    'size'			=> convert_size($row['FILE_SIZE'])
                                                );
                                                if(eregi('image', $row['FILE_TYPE'])) {
                                                    $imgC = new ImgClass(460,1000);
                                                    $size = @getimagesize($upload_dir . '/' . $_GET['board_id'] . '/' . $row['FILE_NAME']);
                                                    $imgC->imageResize($size[0], $size[1]);
                                                    //echo $board_id;
                                                    //if(ereg("^bodo$|^gallery$|^youngsang$", $board_id)) $upload_dir_web = "http://ipsi.mnu.ac.kr".$upload_dir_web;

                                                    $images = "<table width='100%'><tr><td width='100%'><div align=center><img src='/xvar/DATA/" . $_GET['board_id'] . '/' . $row['FILE_NAME'] . "' style='cursor:hand' onclick='real_size(this.src)' alt=$row[FILE_TEXT] width=$size[0] height=$size[1]></div></td></tr></table>";
                                                    //$images = '<img src="/xvar/DATA/' . $_GET['board_id'] . '/' . $row['FILE_NAME'] . '">';

                                                    $imgC = new ImgClass(130,85);
                                                    $size = @getimagesize($upload_dir . '/' . $_GET['board_id'] . '/' . $row['FILE_NAME']);
                                                    $imgC->imageResize($size[0], $size[1]);
                                                    //echo $board_id;
                                                    //if(ereg("^bodo$|^gallery$|^youngsang$", $board_id)) $upload_dir_web = "http://ipsi.mnu.ac.kr".$upload_dir_web;

                                                    $s_images = "<img src='/xvar/DATA/" . $_GET['board_id'] . '/' . $row['FILE_NAME'] . "' style='cursor:hand' width=$size[0] height=$size[1]>";
                                                }
                                            }
                                            $tplAssign['file'] = $download;

                                            if($row['VideoFile']) {
                                                $del_ucc = "<INPUT TYPE='checkbox' NAME='del_ucc' value='1'>동영상 삭제";
                                            }
                                        } else {
                                            $category = $row['CATEGORY'];
                                            $title = '[RE] ' . str_replace('"', '&quot', $row['TITLE']);
                                            //$contents = htmlspecialchars($row['CONTENTS']);
                                            $contents = $row['CONTENTS'];
                                            //$contents_re = nl2br(strip_tags($row['CONTENTS']));
                                            $contents_re = nl2br(strip_tags($row['CONTENTS']));

                                            if($_GET['mode'] == 'reply') {
                                                $conf["USE_reply"] = 1;
                                                $tplAssign['USE_reply'] = 1;
                                            }
//
//			if($conf['USE_EDITOR']) {
//				$contents = $contents;
//			} else {
//				$contents = str_replace(chr(13), chr(13) . '>', $contents);
//				$contents = "\n\n>" . $contents . "\n";
//			}
                                            $contents = "";
                                        }



                                        $frm = array(
                                            'category'		=> $row['CATEGORY'],

                                            'board_num'		=> $board_num,
                                            'writer'		=> stripslashes($writer),
                                            'email'			=> $email,
                                            'homepage'		=> $homepage,
                                            'title'			=> stripslashes($title),
                                            'use_html'		=> $use_html,
                                            'use_secret'	=> $use_secret,
                                            'notice'		=> $notice,
                                            'link1'			=> $link1,
                                            'VideoFile'		=> $VideoFile,
                                            'ImageFile'		=> $ImageFile,
                                            'FileSize'		=> $FileSize,
                                            'PlayTime'		=> $PlayTime,
                                            'RANK'			=> $RANK,
                                            'date_in'		=> $date_in,
                                            'hit_in'		=> $hit_in,
                                            'contents_re'		=> $contents_re,
                                            'member_name'		=> $member_name,
                                            'etc1'			=> $etc1,
                                            'link2'			=> $link2,
                                            'conhost' => $conhost,
                                            'conterm' => $conterm,
                                            'condate' => $condate,
                                            'contel' => $contel,
                                            'conoutline' => $conoutline,
                                            'confee' => $confee
                                        );
                                        //print_r($frm);
                                        //$tplAssign['contents'] = stripslashes($contents);
                                        $tplAssign['contents'] = $contents;
                                        break;

                                    default:

                                }



                                $tplAssign['frm'] = $frm;

                                //분류
                                if($conf['USE_CATEGORY']) {
                                    if($conf['USE_TREE']) {
                                        $sql = "Select * From " . $conf['TREE_ID'] . "_XTREE Where PARENT IN (" . $conf['TREE_PARENT']. ")";
                                        $res = $xdb->query($sql);
                                        $category_list = "<option value=''>선택하세요";
                                        while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                                            //echo $category;
                                            $opt = ($category == $row['TREE_NO']) ? 'selected' : '';
                                            $category_list .= '<option '. $opt . " value='" . $row['TREE_NO'] . "'>" . $row['NAME'];
                                        }
                                        $tplAssign['category_list'] = $category_list;
                                    }else{
                                        $sql = 'Select * From ' . TABLE_CATEGORY . " Where BOARD_ID='" . $_GET['board_id']. "'";
                                        $res = $xdb->query($sql);
                                        $category_list = "<option value=''>선택하세요";
                                        while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                                            //echo $category;
                                            $opt = ($category == $row['CATEGORY_NO']) ? 'selected' : '';
                                            $category_list .= '<option '. $opt . " value='" . $row['CATEGORY_NO'] . "'>" . $row['CATEGORY_NAME'];
                                        }
                                        $tplAssign['category_list'] = $category_list;

                                    }
                                }

                                $tplAssign['form_action'] = 'write_post.php?board_id=' . $_GET['board_id']."&menu=" . $_GET['menu'];
                                //$tplAssign['form_action'] = 'write_post.php';


                                $skin = ($_GET['cms'] == 'admin') ? $conf['ADMIN_SKIN'] : $conf['SKIN'];
                                if($board_id == "org_02_display" || $board_id == "org_02_showsmall" || $board_id == "org_02_showbig") {$skin=$conf['SKIN'];}
                                $skin_path = "/xboard/skin/".$skin."/write.php";
                                include_once($DOCUMENT_ROOT.$skin_path);

                                /*
                                $tpl = new Template('xboard/' . $skin);
                                $tpl->assign($tplAssign);
                                $tpl->define('template', 'write.html');
                                //if(intval($conf['USE_EDITOR'])) $tpl->define('editor',  '../../xeditor/editor.htm');
                                $tpl->print_('template');
                                */
                                ?>

                            </div>
                        </div>
                        <!-- //contents area -->
                    </div>
                    <!-- //contents -->
                </article>
            </div>

        </div>
    </section>
    <!-- //container -->

    <!-- footer -->
    <? include_once("../include/footer.php");?>
    <!-- //footer -->
</div>
<!-- //total -->

<script type="text/javascript">
    initTopMenu(<?=$gubun1?>);
    top2menuView(<?=$gubun1?>);
    setMenuOn(<?=$gubun2?>);
</script>

</body>
</html>
