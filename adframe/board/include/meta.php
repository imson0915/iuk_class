<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0" />
<meta name="format-detection" content="telephone=no" />

<link rel="stylesheet" href="../css/common.css" />
<link rel="stylesheet" href="../css/sub.css" />
<link rel="stylesheet" href="../css/board.css" />

<script type="text/javascript" src="../js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="../js/basic.js"></script>
<script type="text/javascript" src="../js/modernizr.custom.js"></script>
<script type="text/javascript" src="../js/classie.js"></script>
<script type="text/javascript" src="../js/board.js"></script>
<!--[if lt IE 9]>
<script src="../js/html5.js"></script>
<script src="../js/modernizr-1.7.min.js"></script>
<script src="../js/respond.min.js"></script>
<script src="../js/IE7.js"></script>
<![endif]-->
<?php
require_once($_SERVER["DOCUMENT_ROOT"].'/xetc/config.php');
require_once('DB.php');
require_once($_SERVER["DOCUMENT_ROOT"].'/xlib/lib.function.php');
require_once('Constants.php');
$xdb = DB::connect($dsn);
if(DB::isError($xdb)) die($xdb->getMessage() . '(' . __FILE__ . ':' . __LINE__ . ')');
//메뉴 정보
require_once('menu_info.php');

$SERVER_NAME_ex = $site_name;

//로그
include_once(dirname(__FILE__)."/../../js/logger.php");

?>