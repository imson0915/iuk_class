<?php

include $_SERVER["DOCUMENT_ROOT"]."/config/config.php";


$web_dir = "/bbs/edit_data/".date("Y-m")."/".date("Y-m-d");

//업로드 이미지 저장
if($_FILES['upimage']['name'] && $_FILES['upimage']['size']>0)
{
	
	$UPDIR1 = $_SERVER['DOCUMENT_ROOT']."/bbs/edit_data/".date("Y-m");
	$UPDIR2 = $_SERVER['DOCUMENT_ROOT']."/bbs/edit_data/".date("Y-m")."/".date("Y-m-d");
	
	if(!file_exists($UPDIR1)) {
		mkdir($UPDIR1, 0707);
    	$result = chmod($UPDIR1, 0707);
    	if(!$result)	go_back("저장 디렉토리 생성에 실패하였습니다.");
	}
	if(!file_exists($UPDIR2)) {
		mkdir($UPDIR2, 0707); 
    	$result = chmod($UPDIR2, 0707);
    	if(!$result)	go_back("저장 디렉토리 생성에 실패하였습니다.");
	}
	
	// 파일업로드 취약점 조치 ~
	//preg_match('/(\.gif|\.jpg|\.jpeg|\.png)$/i', $_FILES['upimage']['name'], $matches) 
	if(preg_match('/(\.gif|\.jpg|\.jpeg|\.png)$/i', $_FILES['upimage']['name'], $matches)<1){
		echo "<script>alert(\"업로드 할 수 없는 파일 입니다.\");self.close();</script>";
		exit;
	}
	// ~ 파일업로드 취약점 조치
    $m = substr(microtime(),2,4);
    $filename = date("YmdHis").$m.eregi_replace("(.+)(\.[gif|jpg|png])","\\2",$_FILES['upimage']['name']);
    $alt = $_FILES['upimage']['name'];
    $b = $_POST['border'];
    $width = $_POST['width'];
    $height = $_POST['height'];
    if($width) $w = "width='{$width}'";
    if($height) $h = "height='{$height}'";
    $align = $_POST['align'];
    if($align) $a = "align='{$align}'";

    $u = "{$web_dir}/{$filename}";
    $result=move_uploaded_file($_FILES['upimage']['tmp_name'], "{$UPDIR2}/{$filename}");

    if($result)
    {
		
        echo "
        <script>
        var str = \"<img src='{$u}' border='{$b}' {$w} {$h} {$a} alt='{$alt}' onLoad='sizeModify(this);'>\";
        opener.pureUtil._editor.innerHTML(str);
        self.close();
        </script>
        ";
		

    }
    else
    {
        echo "<script>alert('이미지 첨부 에러입니다!'); self.close(); </script>";
    }

    exit;
} //end if
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>이미지 삽입</title>
<style>
body {
    background: threedface;
    color: windowtext;
    margin: 10px;
    border-style: none;
    font:9pt 돋움;
    text-align:center;
}
body, button, div, input, select, td, legend { font:9pt 돋움; }
input,select {color:highlight}
button {width:80px;}
fieldset { margin-bottom:5px;text-align:left;padding:5px }

</style>
<script type="text/javascript">
<!--
function insertImage()
{
    var str="";
    var f=document.tform;
    var src = f.upimage.value;    
    if(!src.match(/\.(gif|jpg|png)$/i)) {
		alert("이미지파일을 첨부 해주세요!"); 
		return false; 
		}
    else {
		f.submit();
	}
}


function fileUploadPreview(thisObj, preViewer) {
  if(!/(\.gif|\.jpg|\.jpeg|\.png)$/i.test(thisObj.value)) {
		//alert(thisObj.value);
		//alert(thisObj.value);
        alert("이미지 형식의 파일을 선택하십시오");
        return false;
    }
	
    preViewer = (typeof(preViewer) == "object") ? preViewer : document.getElementById(preViewer);
    var ua = window.navigator.userAgent;
    if (ua.indexOf("MSIE") > -1) {
        var img_path = "";
        if (thisObj.value.indexOf("\\fakepath\\") < 0) {
            img_path = thisObj.value;
        } else {
            thisObj.select();
            var selectionRange = document.selection.createRange();
            img_path = selectionRange.text.toString();
            thisObj.blur();
        }
        preViewer.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='fi" + "le://" + img_path + "', sizingMethod='scale')";
    } else {
        preViewer.innerHTML = "";
        var W = preViewer.offsetWidth;
        var H = preViewer.offsetHeight;
        var tmpImage = document.createElement("img");
        preViewer.appendChild(tmpImage);

        tmpImage.onerror = function () {
            return preViewer.innerHTML = "";
        }

        tmpImage.onload = function () {
            if (this.width > W) {
                this.height = this.height / (this.width / W);
                this.width = W;
            }
            if (this.height > H) {
                this.width = this.width / (this.height / H);
                this.height = H;
            }
        }
        if (ua.indexOf("Firefox/3") > -1) {
            var picData = thisObj.files.item(0).getAsDataURL();
            tmpImage.src = picData;
        } else {
            tmpImage.src = "file://" + thisObj.value;
        }
    }
}
//-->
</script>
</head>

<body scroll="no">
<form name="tform" method="post" enctype="multipart/form-data">
	<!--
    <fieldset>
    <legend>미리보기</legend>
    <table border=0 cellspacing=0 cellpadding=0 width="100%">
    <tr>
      <td align="center" style="height:150px" >
        <div id="preView" class="preView" title="이미지미리보기"></div>
      </td>
    </tr>
    </table>
    </fieldset>
    -->
    
    <fieldset>
    <legend>이미지 선택</legend>
    <input type="file" name="upimage" style="width:100%" onchange="fileUploadPreview(this, 'preView')" />
    </fieldset>

    <fieldset>
    <legend>옵션</legend>
    <table border=0 cellspacing=6 cellpadding=0>
    <tr>
    <td>정렬</td>
    <td>
    <select name="align">
    <option value="" selected>없음
    <option value="baseline">기준선</option>
    <option value="top">위쪽</option>
    <option value="middle">가운데</option>
    <option value="bottom">아래쪽</option>
    <option value="texttop">문자열 위쪽</option>
    <option value="absmiddle">선택 영역의 가운데</option>
    <option value="absbottom">선택 영역의 아래쪽</option>
    <option value="left">왼쪽</option>
    <option value="right">오른쪽</option>
    </select>
    </td>
    </tr>
    <tr>
    <td>가로*세로</td>
    <td>
    <input type="text" name="width" value="" size="3" maxlength=1> * 
    <input type="text" name="height" value="" size="3" maxlength=1>px
    </td>
    </tr>
    <tr>
    <td>두께</td>
    <td>
    <input type="text" name="border" value="0" size="2" maxlength=1>px
    </td>
    </tr>
    </table>
    </fieldset>

    <button onclick="insertImage()">확인</button>

</form>
</body>
</html> 