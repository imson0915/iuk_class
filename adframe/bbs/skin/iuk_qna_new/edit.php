<link rel="stylesheet" href="/page/css/board.css"/>

<script>
    var cat2 = new Array;


    <?
    $sql = "select cms_id,cms_name from cms_category where cms_id like '10%' and length(cms_id) > 4 order by cms_id asc";
    $result = sql_query($sql);

    $arr1 = 0;
    $arr2 = 0;
    for($i=0; $row=sql_fetch_array($result); $i++){

    if(substr($row[cms_id],0,4) != $cms_id){

        $arr1 = $arr1+1;;
        $arr2 = 0;

        echo "cat2[".$arr1."] = new Array\n";

    }else{
        $arr2 = $arr2+1;
    }
    ?>
    cat2[<?=$arr1?>][<?=$arr2?>] = {code:'<?=$row[cms_id]?>',name:'<?=$row[cms_name]?>'}
    <?
    $cms_id = substr($row[cms_id],0,4);
    }

    ?>



    function view_cat2(s) {
        selected = s;
        obj = document.all['fm_cat2'];

        obj.options[0] = new Option( '::소분류선택::', -1, true, true);
        if(selected == 0) {
            obj.options.length = 1;
            return;
        }
        //alert(selected);
        obj.options.length = cat2[selected].length+1;
        for(j=0; j<cat2[selected].length; j++) {
            obj.options[j+1] = new Option(cat2[selected][j].name, cat2[selected][j].code);
        }
        default_value = obj[0].value;
        default_value = obj[0].selected = true;

    }
</script>

<div class="board-area">
    <form name="writeform" id="writeform" method="post" action="/adframe/bbs/module_edt.php" enctype="multipart/form-data">
        <fieldset>
            <legend class="blind">글쓰기</legend>

            <div class="board-write">

                <dl>
                    <dt>
                        <label for="writer">
                            작성자
                        </label>
                    </dt>
                    <dd>
                        <input type="text" id="fm_name" name="fm_name" class="w30" value="<?=$bbs_row[name]?>" />
                    </dd>
                </dl>

                <dl>
                    <dt>
                        <label for="type">
                            학과선택
                        </label>
                    </dt>
                    <dd>
                        <select id="fm_cat1" name="fm_cat1" onchange="view_cat2(this.selectedIndex);">
                            <option value="">선택하세요</option>
                            <?
                            $sql = "select cms_id,cms_name from cms_category where cms_id != '1080' and cms_id like '10%' and length(cms_id) = 4 order by cms_id asc";
                            $result = sql_query($sql);

                            for($i=0; $row=sql_fetch_array($result); $i++){

                            if(substr($row[cms_id],0,4) == substr($bbs_row[cat2],0,4))	$sel = " selected";
                            else	$sel = "";
                            ?>
                            <option value="<?=$row[cms_id]?>"<?=$sel?>><?=$row[cms_name]?></option>
                            <? } ?>
                        </select>

                        <select name="fm_cat2">
                            <option value="" selected>::소분류선택::</option>
                            <?
                            $sql = "select cms_id,cms_name from cms_category where cms_id like '".$bbs_row[cat1]."%' and length(cms_id) > 4 order by cms_id asc";
                            $result = sql_query($sql);

                            for($i=0; $row=sql_fetch_array($result); $i++){

                                if($row[cms_id] == $bbs_row[cat2])	$sel = " selected";
                                else	$sel = "";
                                ?>
                                <option value="<?=$row[cms_id]?>"<?=$sel?>><?=$row[cms_name]?></option>
                            <? } ?>
                        </select>
                    </dd>
                </dl>

                <dl>
                    <dt>
                        <label for="category">
                            분류
                        </label>
                    </dt>
                    <dd>
                        <input type="radio" name="fm_cat3" value="신입학" <? if($bbs_row[cat3] == "신입학"){ echo " checked "; } ?> >신입학
                        <input type="radio" name="fm_cat3" value="편입학" <? if($bbs_row[cat3] == "편입학"){ echo " checked "; } ?> >편입학
                    </dd>
                </dl>

                <dl>
                    <dt>
                        <label for="title">
                            제목
                        </label>
                    </dt>
                    <dd>
                        <input type="text" id="title" name="fm_title" value='<?=$bbs_row[title]?>'  />
                    </dd>
                </dl>
                <? if($auto_bbs_input != "true" && $auto_bbs_userpwd){ ?>
                    <input type="hidden" name="fm_pwd" value="<?=$auto_bbs_userpwd?>" />
                <? } else { ?>
                    <dl>
                        <dt>
                            <label for="password">
                                비밀번호
                            </label>
                        </dt>
                        <dd>
                            <input type="password" id="fm_pwd" name="fm_pwd" value="<?=$bbs_row[pwd]?>" />
                        </dd>
                    </dl>
                <? } ?>

                <div class="editer-area">
                    <? include $_SERVER["DOCUMENT_ROOT"]."/adframe/bbs/module/editor/".$configBBS[module_editor];
                    ?>
                </div>

            </div>

            <div class="btn-right">
                <input type="submit" id="btnReg" name="" value="수정" class="btns02 btns-color03 btns-2nd btns-mr" />
                <a href="#" id="btnList" class="btns02 btns-color02 w45 btns-2nd btns-ml">
                    목록
                </a>
            </div>

        </fieldset>

        <input type="hidden" name="data" value="<?=$data?>">
        <input type="hidden" name="BURL" value="<?=$PHP_SELF?>">
        <input type="hidden" name="Confirm" value="define">
    </form>
</div>


<script>
    function submit2(){
        //var content = <?=$bbs_row[content]?>;
        var fr = document.getElementById("writeform");
        //alert(content);
        bbsSendit();
    }
    $(document).ready(function() {
        $('#btnReg').bind('click', function() {
            submit2();
        });

        $('#btnList').bind('click', function() {
            location.replace("<?=$PHP_SELF?>?bbs=list&data=<?=$data?>");
        })
    })
</script>
