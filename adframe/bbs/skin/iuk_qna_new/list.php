<link rel="stylesheet" href="/page/css/board.css"/>




            <div class="board-area">
                <form action="">
                    <fieldset>
                        <legend class="blind">검색</legend>
                        <p class="total">
                            총 게시물 : <?=$numrows?>건
                        </p>
                        <form name="searchForm" action="<?=$PHP_SELF?>" method="post" onSubmit="return searchSendit();">
                            <input type="hidden" name="data" value="<?=$data?>">
                            <div class="search-wrapper">
                                <div class="search-area fl">
    <!--                                <select id="" title="검색 선택창">-->
    <!--                                    <option value="" selected="selected">분류</option>-->
    <!--                                </select>-->

                                    <select id="search" name="search" title="검색 선택창">
                                        <option value="title" <? if($search=="title") echo "selected"; ?>>제목</option>
                                        <option value="name" <? if($search=="name") echo "selected"; ?>>내용</option>
                                        <option value="content" <? if($search=="content") echo "selected"; ?>>제목 + 내용</option>
                                    </select>
                                    <div class="search-box">
                                        <input type="text" id="" name="searchstring" value="<?=$searchstring?>" title="검색어 입력" />
                                        <input type="submit" id="" name="" value="검색" class="btn-search" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </fieldset>
                </form>

                <div class="board-list01">
                    <table style="width: 100%" summary="이 표는 DIT동의과학대학교 DIT광장의 입시상담을 번호, 제목, 작성자, 첨부파일, 등록일, 조회에 관한 정보를 제공하는 정보표입니다.">
                        <caption>공지사항표</caption>
                        <thead>
                        <tr>
                            <th scope="col">
                                번호
                            </th>
                            <th scope="col">
                                구분
                            </th>
                            <th scope="col">
                                제목
                            </th>
                            <th scope="col">
                                작성자
                            </th>
                            <th scope="col">
                                등록일
                            </th>
                            <th scope="col">
                                조회
                            </th>
                        </tr>
                        </thead>
                        <tbody>


                        <?php
                        $bbs_result = DBquery($bbs_qry);
                        if ($numrows<1) {
                        ?>
                            <tr>
                                <td colspan="6">
                                    등록된 정보가 없습니다.
                                </td>
                            </tr>
                        <?php
                        } else {
                            // 게시판 출력 로직
                            $s_letter=$letter_no; //페이지별 시작 글번호

                            while($bbs_row=mysql_fetch_array($bbs_result))
                            {

                                $encode_str = "pagecnt=".$pagecnt."&idx=".$bbs_row[idx]."&letter_no=".$s_letter."&offset=".$offset;
                                $encode_str.= "&search=".$search."&searchstring=".$searchstring;
                                $encode_str.= "&Boardkey=".$bbs_row[code]."&Sub_No=".$bbs_row[sub_no]."&DBTable=".$configBBS[board_id];

                                $list_data=Encode64($encode_str); //각 레코드 정보

                                //새글이미지
                                //if(BetweenPeriod($bbs_row[writeday],"1") > 0) $newImg = "<img src='/adframe/bbs/skin/$configBBS[board_skin]/images/icon_new.gif' border='0'>";
                                if(BetweenPeriod($bbs_row[writeday],"1") > 0) $newImg = "<span class=\"icon-new\">new</span>";
                                else $newImg = "";

                                $writeday = explode("-",substr($bbs_row[writeday],0,11));
                                $writeday = $writeday[0]."-".$writeday[1]."-".$writeday[2];

                                // 첨부파일 존재 여부
                                $file_exists = $bbs_row[up_file_idx];

                                if($bbs_row[re_level]>0){	//답변
                                    $level_img = "<span class=\"icon-reply\">Reply</span>";
                                    $wid=5*$bbs_row[re_level]; //레벨 이미지 길이
                                    //$level_img = "<img src='/bbs/skin/$configBBS[board_skin]/images/null.gif' width=".$wid." height=1 border='0'><img src='/bbs/skin/$configBBS[board_skin]/images/re_icon.gif' width='11' height='12' border='0'>";
                                }else{
                                    $level_img = "";
                                }

                                $patternkey = "/$searchstring/i";
                                $BBS_Title  = StringCut($bbs_row[title],0,$configBBS[board_titlecut],'UTF-8','...');
                                if(!empty($searchstring) && $search == "title") {
                                    $BBS_Title = preg_replace($patternkey, "<font color=000000 style=background-color:FFF000;>$searchstring</font>", $BBS_Title);
                                }

                                //$bbs_name = OnlyCut($bbs_row[name],20);
                                $bbs_name = StringCut($bbs_row[name],0,20,'UTF-8','');

                                //if($bbs_row[notice] == "Y") $list_no = "<img src='/bbs/skin/$configBBS[board_skin]/images/notice.gif' alt='공지사항' width='51' height='25'>";
                                if($bbs_row[notice] == "Y") $list_no = '<span class="icon-new">new</span>';
                                else if ($dataArr[idx] == $bbs_row[idx] && $bbs == "see") $list_no = "<img src='/bbs/skin/$configBBS[board_skin]/images/list_arrow_icon.gif'>";
                                else						$list_no = $letter_no;


                                //리스트 & 내용보기 권한이 없을때 링크삭제
                                if($SecAdmin != 1 && $configBBS[auth_list] && @strpos(",".$configBBS[auth_list], $bbs_authgroup) == false){
                                    $BBS_TitleLink = $BBS_Title." ".$newImg;

                                }if($SecAdmin != 1 && $configBBS[auth_read] && @strpos(",".$configBBS[auth_read], $bbs_authgroup) == false){
                                $BBS_TitleLink = $BBS_Title." ".$newImg;

                            }else{
                                $BBS_TitleLink = $level_img."&nbsp;<a href='".$PHP_SELF."?bbs=see&data=".$list_data."'>".$BBS_Title." ".$newImg."</a>";
                            }

                        ?>

                                <tr>
                                    <td class="number">
                                        <?=$list_no?>
                                    </td>
                                    <td class="file">
                                        <?php
                                            if($bbs_row[cat3] == "신입학"){
                                        ?>
                                            <span class="point-color01">
                                            신입학
                                            </span>
                                        <?php
                                            } else {
                                        ?>
                                            <span class="point-color02">
                                            편입학
                                            </span>
                                        <?php
                                            }
                                        ?>
                                    </td>

                                    <td class="title left">
<!--														<span class="icon-secret">-->
<!--															비밀글-->
<!--														</span>-->
                                        <a href="#">
                                            <?=$BBS_TitleLink?>
<!--                                            <span class="comment-hit">(0)</span>-->
                                        </a>
                                        <div class="mobile-info">
                                            <span class="mobile-date"><?=$writeday?></span>
                                        </div>
                                    </td>
                                    <td class="writer">
<!--                                        <img src="../make_img/board/icon_admin.gif" alt="DIT 관리자" />-->
                                            <?=$bbs_name?>
                                    </td>
<!--                                    <td class="file">-->
<!--                                        --><?php
//                                            if ($file_exists) {
//                                        ?>
<!--                                            <img src="../make_img/board/icon_file.png" alt="첨부파일" />-->
<!--                                        --><?php
//                                            }
//                                        ?>
<!--                                    </td>-->
                                    <td class="date">
                                        <?=$writeday?>
                                    </td>
                                    <td class="hit">
                                        <?=$bbs_row[readnum]?>
                                    </td>
                                </tr>






                                <?
                                $letter_no--;
                            }//while



                            ?>

                        <?php
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <?php
                $Obj=new CList($PHP_SELF,$pagecnt,$offset,$numrows,$PAGEBLOCK,$LIMIT,$search,$searchstring,"");
                ?>

                <div class="btn-right">
<!--                    --><?// BBSButtonLink($_BBS_Written, "<a href=\"#\" class=\"btns02 btns-color03\"> 글작성</a>"); ?>
                    <? BBSButtonLink($_BBS_Written, "글작성", null, "btns02 btns-color03"); ?>
                </div>

                <p class="paging-navigation">
<!--                    //--><?//=$Obj->putList(true,"<a href=\"#none\" class=\"btn-preview\">이전 페이지로 이동</a>","<a href=\"#none\" class=\"btn-next\">다음 페이지로 이동</a>");?>
                    <?=$Obj->putList(false);?>
                </p>


                <!-- 비밀글 비밀번호 입력 -->
                <div class="check-password-area">
                    <div class="check-password-box">
                        <form action="" method="" id="" name="">
                            <fieldset>
                                <legend class="blind">비밀글보기 입력 폼</legend>
                                <h2>
                                    비밀글보기
                                </h2>

                                <!-- 비밀번호 입력 폼 -->
                                <div class="input-password">
                                    <p>
                                        이글은 비밀글입니다.<strong class="point-color01">비밀번호를 입력하여 주세요.</strong>
                                        <span class="span-br"></span>
                                        관리자는 확인버튼만 누르시면 됩니다.
                                    </p>
                                    <dl>
                                        <dt>
                                            <label for="password">
                                                비밀번호
                                            </label>
                                        </dt>
                                        <dd>
                                            <input type="text" id="password" name="" value="" />
                                            <input type="submit" id="" name="" value="확인" title="확인" />
                                        </dd>
                                    </dl>
                                </div>
                                <!-- 비밀번호 입력 폼 -->

                                <!-- 잘못된 비밀번호 입력 시 -->
                                <div class="discord-password" style="display: none">
                                    <p>
                                        잘못된 비밀번호입니다. <span class="span-br"></span>
                                        비밀번호를 다시 입력해주세요.
                                    </p>
                                    <a href="#ok" class="btn-ok">
                                        확인
                                    </a>
                                </div>
                                <!-- //잘못된 비밀번호 입력 시 -->

                            </fieldset>
                        </form>
                        <a href="#none" class="btn-close">
                            <img src="../make_img/board/btn_close@2x.gif" alt="비밀글 비밀번호 입력 창 닫기" />
                        </a>
                    </div>
                </div>
                <!-- //비밀글 비밀번호 입력 -->
            </div>


