<link rel="stylesheet" href="/page/css/board.css"/>

<script language="JavaScript">
function bluringIMG(){ 
if(event.srcElement.tagName=="A"||event.srcElement.tagName=="IMG") document.body.focus(); 
} 
document.onfocusin=bluringIMG; 

function sizeModify(img){
   if (img.width > <?=$configBBS[board_viewimgwidth]?>){
       img.width = <?=$configBBS[board_viewimgwidth]?> ; 
   }
}
function PopupIMG(FURL,WIDTH,HEIGHT,VALUED){
	window.open(FURL+'?data='+VALUED,'popup','width='+WIDTH+',height='+HEIGHT+',scrollbars=no,stauts=no');
}

function DisplayDetail(divname,state) {
		if(state == 1) {			
			document.getElementById(divname).style.display = "";
		} 
		else {
			document.getElementById(divname).style.display = "none";
		}
}
</script>


        <div class="board-area">

            <div class="board-view">
                <dl>
                    <dt>
                        <strong>제목</strong>
                    </dt>
                    <dd>
                        <strong><?=$view_row[title]?></strong>
                    </dd>
                </dl>
                <dl class="half-box01">
                    <dt>
                        등록일
                    </dt>
                    <dd>
                        <?=$writeday[0]?>-<?=$writeday[1]?>-<?=$writeday[2]?>
                    </dd>
                </dl>

                <dl class="half-box02">
                    <dt>
                        작성자
                    </dt>
                    <dd>
                        <?=$bbs_name?>
                    </dd>
                </dl>

                <dl class="half-box01">
                    <dt>
                        첨부파일
                    </dt>
                    <dd>
                        <?php
                        for($i=0; $i < $filev; $i++) {
                            ?>
                            <?=$upfile_link[$i]?>
                            <?php
                        }
                        ?>
                    </dd>
                </dl>
                <dl class="half-box02 none">
                    <dt>
                        조회
                    </dt>
                    <dd>
                        <?=$readnum?>
                    </dd>
                </dl>
            </div>

            <div class="board-contents clear">
                <?
                $content = preg_replace("/(\<img )([^\>]*)(\>)/i", "\\1 name='target_resize_image[]'  \\2 \\3", $content);
                ?>
                <?=$content?>
            </div>

            <!--    <div class="preview-next">-->
            <!--        <a href="#" class="line">-->
            <!--            <dl>-->
            <!--                <dt class="preview">-->
            <!--                    이전글-->
            <!--                </dt>-->
            <!--                <dd>-->
            <!--                    2013학년도 유치원정교사(2급) 자격증 취득예정자 명단 공-->
            <!--                </dd>-->
            <!--            </dl>-->
            <!--        </a>-->
            <!---->
            <!--        <a href="#">-->
            <!--            <dl>-->
            <!--                <dt class="next">-->
            <!--                    다음글-->
            <!--                </dt>-->
            <!--                <dd>-->
            <!--                    2013학년도 유치원정교사(2급) 자격증 취득예정자 명단 공-->
            <!--                </dd>-->
            <!--            </dl>-->
            <!--        </a>-->
            <!--    </div>-->




            <div class="btn-right">
                <input type="submit" id="btnReply" name="" value="답변" class="btns02 btns-color03 w25" />
                <input type="submit" id="btnMod" name="" value="수정" class="btns02 btns-color03 w25" />
                <input type="submit" id="btnDelete" name="" value="삭제" class="btns02 btns-color03 w25" />

                <a href="<?=$PHP_SELF?>?bbs=list&data=<?=$data?>" class="btns02 btns-color02 w25 mr-none">
                    목록
                </a>
            </div>
        </div>


        <!-- 비밀글 비밀번호 입력 -->
        <div class="check-password-area">
            <div class="check-password-box">
                <form action="" method="post" id="pwdForm" name="pwdForm">
                    <fieldset>
                        <legend class="blind">비밀글보기 입력 폼</legend>
                        <h2>
                            비밀번호 입력
                        </h2>

                        <!-- 비밀번호 입력 폼 -->
                        <div class="input-password">
                            <p>
                                <strong class="point-color01">비밀번호를 입력하여 주세요.</strong>
                                <span class="span-br"></span>

                            </p>
                            <dl>
                                <dt>
                                    <label for="password">
                                        비밀번호
                                    </label>
                                </dt>
                                <dd>
                                    <input type="password" id="pwd" name="pwd" value="" />
                                    <input type="submit" id="btnConfirm" name="btnCofirm" value="확인" title="확인" />
                                </dd>
                            </dl>
                        </div>
                        <!-- 비밀번호 입력 폼 -->

                        <!-- 잘못된 비밀번호 입력 시 -->
                        <div class="discord-password" style="display: none">
                            <p>
                                잘못된 비밀번호입니다. <span class="span-br"></span>
                                비밀번호를 다시 입력해주세요.
                            </p>
                            <a href="#ok" class="btn-ok">
                                확인
                            </a>
                        </div>
                        <!-- //잘못된 비밀번호 입력 시 -->

                    </fieldset>
                </form>
                <a href="#none" class="btn-close">
                    <img src="../make_img/board/btn_close@2x.gif" alt="비밀글 비밀번호 입력 창 닫기" />
                </a>
            </div>
        </div>
<!-- //비밀글 비밀번호 입력 -->

<script>
function fImgSize(MaxWidth){ 
	var NewImage = new Image(); 
	
	var Target=document.getElementsByName("target_resize_image[]");
	
	for(i=0; i<Target.length; i++){
		NewImage.src = Target[i].src; 
		OldWidth = NewImage.width; 
		OldHeight = NewImage.height; 

		if(OldWidth >= MaxWidth){
			NewHeight = parseFloat(OldWidth / OldHeight);
		    Target[i].width = MaxWidth;
		    Target[i].height = parseInt(MaxWidth / NewHeight);
		}
	}
}

window.onload=function() {		
	fImgSize(600);
}
</script>



<script>
    $(document).ready(function() {
        $('#btnMod, #btnDelete').click(function() {
            $('#pwd').val('');

            // 모달 팝업이 뜰 때 수정, 삭제 인지 플래그 값 세팅
            if ($(this).attr('id') == 'btnMod') {
                $('#btnConfirm').attr('flag', 'mod');
            } else {
                $('#btnConfirm').attr('flag', 'del');
            }
            $('#wrapper').addClass('fixed');
            $('.check-password-area').fadeIn('fast', function() {
                $('.check-password-box').show();
            });
        });

        /* 비밀글 비밀번호 입력 취소*/
        $('.check-password-box .btn-close').click(function(){
            $('#pwd').val('');
            $('#btnConfirm').removeAttr('flag');

            $('#wrapper').removeClass('fixed');
            $('.check-password-area').hide();
        });
        
        
        // 답글 쓰기 페이지 이동 
        $('#btnReply').bind('click', function() {
            location.replace("<?=$_BBS_Replied?>");
        });
    });


    $('#btnConfirm').bind("click", function() {
        if ($('#pwd').val() == '') {
            alert("패스워드를 입력하여 주십시오.");
            return false;
        }
        
        // 클래스에 정의된 함수 호출
        if ($('#btnConfirm').attr('flag') == 'mod') {
            bbsEdit();
        } else if ($('#btnConfirm').attr('flag') == 'del') {
            bbsDel();
        }

    })
</script>