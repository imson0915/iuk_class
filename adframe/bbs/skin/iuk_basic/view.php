<script language="JavaScript">
    function bluringIMG(){
        if(event.srcElement.tagName=="A"||event.srcElement.tagName=="IMG") document.body.focus();
    }
    document.onfocusin=bluringIMG;

    function sizeModify(img){
        if (img.width > <?=$configBBS[board_viewimgwidth]?>){
            img.width = <?=$configBBS[board_viewimgwidth]?> ;
        }
    }
    function PopupIMG(FURL,WIDTH,HEIGHT,VALUED){
        window.open(FURL+'?data='+VALUED,'popup','width='+WIDTH+',height='+HEIGHT+',scrollbars=no,stauts=no');
    }

    function DisplayDetail(divname,state) {
        if(state == 1) {
            document.getElementById(divname).style.display = "";
        }
        else {
            document.getElementById(divname).style.display = "none";
        }
    }
</script>

<div class="board-area">
    <div class="board-view">
        <div class="title-area">
            <h2>
                <?=$view_row[title]?>
            </h2>
        </div>
        <div class="board-view-info">
            <dl class="writeer" style="width: 35%;">
                <dt>작성자 : </dt>
                <dd><?=$bbs_name?></dd>
            </dl>
            <dl class="hit">
                <dt>조회수 : </dt>
                <dd><?=$readnum?></dd>
            </dl>
            <dl class="date" style="float: right;">
                <dt>작성일 : </dt>
                <dd><?=$writeday[0]?>-<?=$writeday[1]?>-<?=$writeday[2]?></dd>
            </dl>
            <? if ( count($filev) > 0 ) { ?>
            <dl class="add-fime" style="width: 100%;">
                <dt>
                    첨부파일
                </dt>
                <dd>
                    <?
                    for($i=0; $i < $filev; $i++) {
                        echo "<p class='word-addfile'>".$upfile_link[$i]."</p>";
                    }
                    ?>
                </dd>
            </dl>
            <? } ?>
        </div>
        
        <div class="board-contents" style="word-break: break-all;">
            
            <?
            //$content = str_replace("bbs/", "adframe/bbs/", $content);
            $content = str_replace("http://cis.iuk.ac.kr", "", $content);
            $content = preg_replace("/(\<img )([^\>]*)(\>)/i", "\\1 name='target_resize_image[]'  \\2 \\3", $content);
            echo stripslashes(htmlspecialchars_decode($content));
            ?>

        </div>
    </div>

    <div class="btns-area">
        <div class="btns-left">
            <form method="POST" name="pwdForm">
            <?
            echo $_BBS_Password;
            BBSButtonLink($_BBS_Modified, "수정", "", "btns-type01 fl");
            BBSButtonLink($_BBS_Deleted, "삭제", "", "btns-type01 fl");
            ?>
            </form>
        </div>

        <div class="btns-right">
            <? BBSButtonLink($_BBS_Written, "글쓰기", "", "btns-type02 fl");?>
            <a href="<?=$PHP_SELF?>?TREE_NO=<?=$TREE_NO?>&PARENT=<?=$PARENT?>&TREE_ID=<?=$TREE_ID?>&ROOT_NO=<?=$ROOT_NO?>&bbs=list&data=<?=$data?>" class="btns-type02">
                목록
            </a>
        </div>
    </div>
</div>

<script>
    function fImgSize(MaxWidth){
        var NewImage = new Image();

        var Target=document.getElementsByName("target_resize_image[]");

        for(i=0; i<Target.length; i++){
            NewImage.src = Target[i].src;
            OldWidth = NewImage.width;
            OldHeight = NewImage.height;

            if(OldWidth >= MaxWidth){
                NewHeight = parseFloat(OldWidth / OldHeight);
                Target[i].width = MaxWidth;
                Target[i].height = parseInt(MaxWidth / NewHeight);
            }
        }
    }

    window.onload=function() {
        fImgSize(600);
    }
</script>

