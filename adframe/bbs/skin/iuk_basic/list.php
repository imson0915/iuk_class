<div class="search-n-reg-area">
    <p class="total left">
        총 <?=$numrows?>건의 자료가 있습니다. (<?=$NowPage?>/<?=$TotalPage?>페이지)
    </p>
    <div class="right">
        <div class="search-area">
            <form name="searchForm" action="<?=$PHP_SELF?>?TREE_NO=<?=$TREE_NO?>&ROOT_NO=<?=$ROOT_NO?>&PARENT=<?=$PARENT?>&TREE_ID=<?=$TREE_ID?>" method="post" onSubmit="return searchSendit();">
                <input type="hidden" name="data" value="<?=$data?>">
                <fieldset>
                    <legend class="blind">
                        게시판 검색
                    </legend>
                    <select id="search" name="search" title="검색 선택창">
                        <?
                            $arr_tmp_searchVal = array("title"=>"제목", "name"=>"내용", "content"=>"제목 + 내용");
                            foreach ( $arr_tmp_searchVal as $k => $v ) { 
                        ?> 
                        <option value="<?=$k?>" <? if($search==$k) echo "selected"; ?>><?=$v?></option>
                        <? } ?>
                    </select>
                    <div class="search-box">
                        <input type="text" id="" name="searchstring" value="<?=$searchstring?>" title="검색어 입력" />
                        <input type="image" src="../make_img/board/btn_search.gif" alt="검색" />
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>

<div class="board-area">
    <div class="board-list">
        <table style="width: 100%" summary="이 표는 게시판에 관한 목록정보를 제공하는 표입니다.">
            <caption>게시판 목록표</caption>
            <colgroup>
                <col style="width: 10%" />
                <col style="width: 40%" />
                <col style="width: 10%" />
                <col style="width: 15%" />
                <col style="width: 15%" />
                <col style="width: 10%" />
            </colgroup>
            <thead>
                <tr>
                    <th scope="col">
                        번호
                    </th>
                    <th scope="col">
                        내용
                    </th>
                    <th scope="col">
                        파일
                    </th>
                    <th scope="col">
                        작성자
                    </th>
                    <th scope="col">
                        작성일
                    </th>
                    <th scope="col" class="none">
                        조회수
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $bbs_result = DBquery($bbs_qry);
                if ($numrows<1) {
                ?>
                <tr>
                    <td colspan="6">
                        등록된 정보가 없습니다.
                    </td>
                </tr>
                <?php
                }else{
                    
                    $s_letter=$letter_no; //페이지별 시작 글번호
                    while($bbs_row=mysql_fetch_array($bbs_result))
                    {
                  
                        $encode_str = "pagecnt=".$pagecnt."&idx=".$bbs_row[idx]."&letter_no=".$s_letter."&offset=".$offset;
                        $encode_str.= "&search=".$search."&searchstring=".$searchstring;
                        $encode_str.= "&Boardkey=".$bbs_row[code]."&Sub_No=".$bbs_row[sub_no]."&DBTable=".$configBBS[board_id];
                        
                        $list_data=Encode64($encode_str); //각 레코드 정보
                  
                        //새글이미지
                        if(BetweenPeriod($bbs_row[writeday],"1") > 0) $newImg = "<img src='/adframe/bbs/skin/$configBBS[board_skin]/images/icon_new.gif' border='0'>";
                        else $newImg = "";
                  
                        $writeday = explode("-",substr($bbs_row[writeday],0,11));
                        $writeday = $writeday[0]."-".$writeday[1]."-".$writeday[2];
                        
                        // 첨부파일 존재 여부
                        $file_exists = $bbs_row[up_file_idx];
                        
                        
                        if($bbs_row[re_level]>0){	//답변
                            $wid=5*$bbs_row[re_level]; //레벨 이미지 길이
                            $level_img = "<img src='/adframe/bbs/skin/$configBBS[board_skin]/images/null.gif' width=".$wid." height=1 border='0'>";
                            $level_img .= "<img src='/adframe/bbs/skin/$configBBS[board_skin]/images/re_icon.gif' width='11' height='12' border='0'>";
                        }else{
                            $level_img = "";
                        }
                        
                        
                        $patternkey = "/$searchstring/i";
                        $BBS_Title  = StringCut($bbs_row[title],0,24,'UTF-8','...');
                        if(!empty($searchstring) && $search == "title") {
                          $BBS_Title = preg_replace($patternkey, "<font color=000000 style=background-color:FFF000;>$searchstring</font>", $BBS_Title);
                        }
                      
                        $bbs_name = StringCut($bbs_row[name],0,20,'UTF-8','');
                        
                        if($bbs_row[notice] == "Y") $list_no = "<img src='/adframe/bbs/skin/$configBBS[board_skin]/images/icon_notice.gif' alit='공지사항'>";
                        else if ($dataArr[idx] == $bbs_row[idx] && $bbs == "see") $list_no = "<img src='/adframe/bbs/skin/$configBBS[board_skin]/images/list_arrow_icon.gif'>";
                        else						$list_no = $letter_no;
                        
                        
                        //카테고리 표시
                        $category_name = "";
                        if($board_category && $bbs_row[sub_no] > 0){
                          for($i=0; $i < count($board_category); $i++) {
                              if($bbs_row[sub_no] == $i+1)	$category_name = "<strong>[".$board_category[$i]."]</strong>";
                          }
                        }
                          
                        //리스트 & 내용보기 권한이 없을때 링크삭제
                        if($SecAdmin != 1 && $configBBS[auth_list] && @strpos(",".$configBBS[auth_list], $bbs_authgroup) == false){
                          $BBS_TitleLink = $BBS_Title." ".$newImg;  	
                        
                        }
                                                
                        if($SecAdmin != 1 && $configBBS[auth_read] && @strpos(",".$configBBS[auth_read], $bbs_authgroup) == false){
                          $BBS_TitleLink = $BBS_Title." ".$newImg;
                        }else{
                            $BBS_TitleLink = " <a href='".$PHP_SELF."?TREE_ID=".$TREE_ID."&ROOT_NO=".$ROOT_NO."&TREE_NO=".$TREE_NO."&PARENT=".$PARENT."&bbs=see&data=".$list_data."'>".$level_img." ".$BBS_Title." ".$newImg."</a>";
                        }
                ?>
                <tr>
                    <td>
                        <?=$list_no?>
                    </td>
                    <td class="left">
                        <?=$category_name?><?=$BBS_TitleLink?>
                    </td>
                    <td>
                        <?php
                            if ($file_exists) {
                        ?>
                        <img src="../make_img/board/icon_addfile.gif" alt="첨부파일" />
                        <? } ?>
                    </td>
                    <td>
                        <?=$bbs_name?>
                    </td>
                    <td>
                        <?=$writeday?>
                    </td>
                    <td>
                        <?=$bbs_row[readnum]?>
                    </td>
                </tr>
                <?
                        $letter_no--;
                    }//while
                }
                $Obj=new CList($PHP_SELF,$pagecnt,$offset,$numrows,$PAGEBLOCK,$LIMIT,$search,$searchstring,"");
                ?>
            </tbody>
        </table>
    </div>

    <div class="btns-area">
        <div class="btns-right">
            <? BBSButtonLink($_BBS_Written, "글쓰기", "", "btns-type01 fl"); ?>
        </div>
    </div>
</div>

<p class="paging-navigation">
    <?=$Obj->putList(true,"<img src=\"../make_img/board/btn_previous.gif\" alt=\"\" />","<img src=\"../make_img/board/btn_next.gif\" alt=\"\" />")?>
</p>

