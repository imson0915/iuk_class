<script language="JavaScript">
    function bluringIMG(){
        if(event.srcElement.tagName=="A"||event.srcElement.tagName=="IMG") document.body.focus();
    }
    document.onfocusin=bluringIMG;

    function sizeModify(img){
        if (img.width > <?=$configBBS[board_viewimgwidth]?>){
            img.width = <?=$configBBS[board_viewimgwidth]?> ;
        }
    }
    function PopupIMG(FURL,WIDTH,HEIGHT,VALUED){
        window.open(FURL+'?data='+VALUED,'popup','width='+WIDTH+',height='+HEIGHT+',scrollbars=no,stauts=no');
    }

    function DisplayDetail(divname,state) {
        if(state == 1) {
            document.getElementById(divname).style.display = "";
        }
        else {
            document.getElementById(divname).style.display = "none";
        }
    }
</script>

<div class="board-area">

    <div class="board-view01">
        <div class="board-title-area">
            <h2>
                <?=$view_row[title]?>
            </h2>

            <div class="board-title-box">
                <p class="user-view-information">
                    <span class="user">
                        <?=$bbs_name?>
                    </span>
                    <span class="date">
                        <?=$writeday[0]?>-<?=$writeday[1]?>-<?=$writeday[2]?>
                    </span>
                </p>

                <p class="hit">
                    조회 : <?=$readnum?>
                </p>
            </div>

            <dl class="add-fime">
                <dt>
                    첨부파일
                </dt>
                <dd>
                    <? if ( $filev > 0 ) { for($i=0; $i < $filev; $i++){ ?>
                        <p class="word-addfile">
                            <?=$upfile_link[$i]?>
                        </p>
                    <? } } else "첨부파일이 없습니다."; ?>
                </dd>
            </dl>
        </div>

        <div class="board-view-contents">
            <?=$content?>
        </div>

    </div>
</div>

<div class="btns-area">
    <div class="btns-left">
        <form method="POST" name="pwdForm">
        <?
        echo $_BBS_Password;
        BBSButtonLink($_BBS_Modified, "수정", "", "btns-type01 fl");
        BBSButtonLink($_BBS_Deleted, "삭제", "", "btns-type01 fl");
        ?>
        </form>
    </div>

    <div class="btns-right">
        <? global $TREE_NO, $DEPTH; ?>
        <a href="<?=$PHP_SELF?>?TREE_NO=<?=$TREE_NO?>&DEPTH=<?=$DEPTH?>&bbs=list&data=<?=$data?>" class="btns-type02">
            목록
        </a>
    </div>
</div>

<? //include("../board/check_password.php");?>
