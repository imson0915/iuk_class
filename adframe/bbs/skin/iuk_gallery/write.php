					<div class="board-area">
						<form name="writeform" method="POST" action="/bbs/module_wte.php" enctype="multipart/form-data">
							
							<input type="hidden" name="ref" value="<?=$bbs_row[ref]?>">
							<input type="hidden" name="re_step" value="<?=$bbs_row[re_step]?>">
							<input type="hidden" name="re_level" value="<?=$bbs_row[re_level]?>">
							<input type="hidden" name="data" value="<?=$data?>">
							<input type="hidden" name="BURL" value="<?=$PHP_SELF?>">
							<input type="hidden" name="Confirm" value="define">
							<input type="hidden" name="BoardKey" value="<?=$BoardKey?>">
							<fieldset>
								<legend class="blind">글쓰기</legend>

								<div class="board-write01">
																				
									<? if($SecAdmin == 1 ) { ?>											
									<dl>
										<dt>
											<label for="use_notice">
												공지사항
											</label>
										</dt>
										<dd>
											<input type="checkbox" id="use_notice" name="fm_notice" value="Y" />
											<label for="use_notice">
												<strong>* 체크시 공지글로 등록됩니다. </strong>
											</label>
										</dd>
									</dl>
									<? } ?>
									
									<dl>
										<dt>
											<label for="writer">
												작성자
											</label>
										</dt>
										<dd>
											<input type="text" id="writer" name="fm_name" value="" class="w30" />
										</dd>
									</dl>
								
									<dl>
										<dt>
											<label for="password">
												비밀번호
											</label>
										</dt>
										<dd>
											<input type="password" id="password" name="fm_pwd" value="" class="w30" />
										</dd>
									</dl>

									<dl>
										<dt>
											<label for="title">
												제목
											</label>
										</dt>
										<dd>
											<input type="text" id="title" name="fm_title" value="" style="width: 700px" />
										</dd>
									</dl>
									
									<dl>
										<dt>
											<label for="board-write-contents">
												내용
											</label>
										</dt>
										<dd>
											<? include ADFRAME_ROOT_PATH."/bbs/module/editor/".$configBBS[module_editor]; ?>
											<!--<textarea id="" name="" cols="50" rows="5"></textarea>-->
										</dd>
									</dl>
									
									<? include ADFRAME_ROOT_PATH."/bbs/module/uploader/".$configBBS[module_uploader]; ?>
									
								</div>
								
								<div class="btns-area">
									<div class="btns-right">
										<input type="submit" value="확인" class="btns-type02" />
										<a href="<?=$PHP_SELF?>?TREE_NO=<?=$TREE_NO?>&DEPTH=<?=$DEPTH?>&bbs=list&data=<?=$data?>" class="btns-type01">
											목록
										</a>
									</div>
								</div>

							</fieldset>
						</form>				
					</div>