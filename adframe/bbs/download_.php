<?
	$_POST = array_map('mysql_escape_string', $_POST);
	$_GET = array_map('mysql_escape_string', $_GET);
	
	if(strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) == false)  exit;
	
	include $_SERVER["DOCUMENT_ROOT"]."/config/config.php";
	include $_SERVER["DOCUMENT_ROOT"]."/config/dbconn.php";
	include $_SERVER["DOCUMENT_ROOT"]."/config/function.php";

    
	$dataArr=Decode64($_GET['data']);
	$configBBS = DBarray("SELECT * FROM abbs_manager WHERE board_key='".$dataArr[Boardkey]."'"); //게시판 설정로드
	
	if(empty($configBBS[idx]))	go_back("존재하지 않는 게시판입니다.");
	if($dataArr[download] != "ok")	go_back("잘못된 접근입니다.");
	
	//권한매핑 설정
	include $_SERVER["DOCUMENT_ROOT"]."/bbs/auth_config.php";
	if($configBBS[auth_download] && @strpos(",".$configBBS[auth_download], $bbs_authgroup) == false)	go_back("다운로드 권한이 없습니다.");
	
	
	$DRS = DBarray("SELECT * FROM ".$configBBS[board_id]."_file WHERE idx='".$dataArr[idx]."'"); //파일정보
	if(!file_exists($DRS[up_filepath]))	go_back("파일이 존재하지 않습니다.");
	
	//다운로드 카운트 증가
	DBquery("update ".$configBBS[board_id]."_file set down_count=down_count+1 where idx='".$dataArr[idx]."'");
	
	
	Header("Content-Type: application/octet-stream");
    Header("Content-Disposition: attachment; filename = ".$DRS[up_filename]);
	Header("Content-Transfer-Encoding: binary");
	Header("Content-Length: ".$DRS[up_filesize]); 
    Header("Cache-Control: cache, must-revalidate");
    Header("Pragma: no-cache");
    Header("Expires: 0");


    if ($_fopen = fopen($DRS[up_filepath], "r+b")) {
        print fread($_fopen, $DRS[up_filesize]);
    }
    fclose($_fopen);
    exit;
?>