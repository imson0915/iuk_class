<?
	/*
	 *	HEADER() 설정 차이로 인해 기본 라이브러리를 별도로 설정 
	*/
	include($_SERVER['DOCUMENT_ROOT'] . "/adframe/config/inc.constant.php");
	// Pear 라이브러리 디렉토리 설정
	ini_set("include_path", ADFRAME_ROOT_PATH."/lib/Pear");
	require_once(ADFRAME_ROOT_PATH . "/config/dsn.ini.php");
	require_once("DB.php");
	$adb = DB::connect($dsn);
	/*
	 *	HEADER() 설정 차이로 인해 기본 라이브러리를 별도로 설정 
	*/
	foreach ( $_GET as $k => $v ) ${$k} = $v;

    $sql_thumb = " SELECT * FROM ".$board_table."_file WHERE up_file_idx = '".$idx."' ";
    $rs_thumb = $adb->getRow($sql_thumb, DB_FETCHMODE_ASSOC);

    switch ( $rs_thumb[file_type] ) {
        case '1': $ext = "gif"; break;
        case '2': $ext = "jpg"; break;
        case '3': $ext = "png"; break;
        default: $ext = "jpg"; break;
    }

	Header("Content-type: image/".$ext);
	Header("Pragma: no-cache");
	Header("Expires: 0");

	// 썸네일 이미지 위치 변경
	$thumnail_path = str_replace("upfile_data/", "upfile_data_thumnail/", $rs_thumb[up_filepath]);
	$thumnail_path = str_replace("upfile_old/", "upfile_old_thumnail/", $thumnail_path);

	// 썸네일 이미지 절대 경로
	if ( $thumnail_path != "" ) $upload_path = "/data/".$thumnail_path;

	if ( $image = "thumnail" ) {

		if ( !file_exists($_SERVER['DOCUMENT_ROOT'].$upload_path) || $rs_thumb[up_filepath] == "" || !$rs_thumb[up_filepath] ) {
			$thumnail_file = $_SERVER['DOCUMENT_ROOT']."/page/make_img/board/img_noimage.jpg";
			$thumnail_size = filesize($thumnail_file);
		} else {
			$thumnail_file = $_SERVER['DOCUMENT_ROOT'].$upload_path;
			$thumnail_size = filesize($thumnail_file);
		}
		
		$fp = fopen( $thumnail_file, "r" );
		$img_data = fread($fp, $thumnail_size);
		fclose($fp);
		
		echo $img_data;
		exit;
	}
?>