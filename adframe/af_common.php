<?php

/*
 * 이 파일은 adframe 전역 변수(상수)와 전역 설정은 지정하는 파일
 */

//#########################################################
// 상수 설정
//#########################################################
include(dirname(__FILE__) . "/config/inc.constant.php");

//#########################################################
// 기본설정 
//#########################################################

// TODO. adframe 개선사항(php 버전 5.4이하에서 보안에 대한 개선 사항이 필요함)
// register_globals 설정
@extract($_GET);
@extract($_POST);
@extract($_SERVER);

// Pear 라이브러리 디렉토리 설정
ini_set("include_path", ADFRAME_ROOT_PATH."/lib/Pear");

// 쿠키 경로 설정 
ini_set("session.cookie_path", "/");

@umask(0000);

// 에러 메시지 확인 
if(version_compare(PHP_VERSION, '5.4.0', '<')) {
	@error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);
} else {
	@error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING ^ E_STRICT);
}

// 보안설정 및 프레임과 상관없이 쿠키 허용
@header('P3P: CP="ALL CURa ADMa DEVa TAIa OUR BUS IND PHY ONL UNI PUR FIN COM NAV INT DEM CNT STA POL HEA PRE LOC OTC"');

//#########################################################
// 세션 설정
//#########################################################
ini_set('session.auto_start','1'); 
ini_set("session.cache_expire", 30);				// 세션 유지 시간 ( 단위 : 분 )
ini_set("session.gc_maxlifetime",1800);			// 세션 가비지 컬렉션 ( 로그인 지속 시간 / 단위 : 초 )
ini_set("session.use_trans_sid", 0);				// PHPSESSID를 자동으로 넘기지 않음
ini_set("url_rewriter.tags","");							// 링크에 PHPSESSID가 따라다니는것을 무력화함 (해뜰녘님께서 알려주셨습니다.)
session_set_cookie_params(0,"/");					// 세션 쿠기가 적용되는 위치

ini_set("session.cookie_domain", ".".SERVICE_DOMAIN);		// 세션이 활성화될 도메인

//개별 페이지 실행 방지 
//if ( !defined(ADFRAME_PREFIX) ) exit();

// 스크립트 실행 시간 설정
if ( !isset($set_time_limit) ) $set_time_limit = 0;
@set_time_limit($set_time_limit);

//#########################################################
// 보안 설정
//#########################################################
include_once(ADFRAME_ROOT_PATH."/config/inc.secure.php");

@session_start();

//#########################################################
// DB 연결
//#########################################################
require_once(ADFRAME_ROOT_PATH . "/config/dsn.ini.php");

//list($aboard['db_type']) = explode('://', $dsn);
require_once("DB.php");

// DB 연결
$adb = DB::connect($dsn);

// DB 접속여부 확인
if ( DB::isError($adb) ) die($adb->getMessage()."(".__FILE__.":".__LINE__.")");
// 한글 깨짐 방지
mysql_query("set session character_set_connection=utf8;");
mysql_query("set session character_set_results=utf8;");
mysql_query("set session character_set_client=utf8;");

//TODO. pear connection은 disconnect를 명시적으로 해줘야 한다. connect 부분은 빠져야 할 거 같다.
//$adb->disconnect();

//#########################################################
// 함수 설정 파일
//#########################################################
include(ADFRAME_ROOT_PATH . "/lib/lib.function.php");

// AdFrame 필수 유틸 페이지 예정
include(ADFRAME_ROOT_PATH . "/lib/lib.required.function.php");


//TODO. adframe google analytics 설정 부분도 추가 해보자.



?>
