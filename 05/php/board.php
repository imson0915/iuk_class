<?
	include("../include/meta.php");
	include_once(ADFRAME_ROOT_PATH . "/lib/class_bbs.php");
?>
<body>
	<?
		// 컨텐츠 내용 불러오기
		$sql_content = " SELECT *
						, ( SELECT IMG_SFILE FROM ".TABLE_CMS_CONTENTS." WHERE TREE_NO = '".$PARENT."' ) AS IMG_PARENT_SFILE
						, ( SELECT CONTENTS FROM ".TABLE_TREE." WHERE TREE_NO = '".$TREE_NO."' ) AS BOARD_CODE 
						FROM ".TABLE_TREE." WHERE TREE_ID = '".$TREE_ID."' AND TREE_NO = '".$TREE_NO."' ";
		$rs_content = $adb->getRow($sql_content, DB_FETCHMODE_ASSOC);
	?>
	<!-- wrapper -->
	<div class="wrapper" style="background: url(<?=CMS_IMG_PATH.$rs_content[IMG_PARENT_SFILE]?>) no-repeat center top;">
		<!-- header -->
		<? include("../include/header.php");?>
		<!-- //header -->
		
		<!-- container -->
		<div class="container" id="container">

			<!-- lnb -->
			<? include("../include/lnb.php");?>
			<!-- //lnb -->

			<!-- contents -->
			<div class="contents">
				
				<div class="contents-title">
					<h1>
						<?=$thisPageName?>
					</h1>
					<p class="contents-navigation">
						<span class="icon-home">
							Home
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<span class="icon-word">
							<?=$thisPageParentName?>
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<strong>
							<?=$thisPageName?>
						</strong>
					</p>
				</div>
				
				<?
					create_bbs($rs_content[BOARD_CODE], '', '0', '', '', '', $row_mainContent[TEMPLATE]);
				?>

			</div>
			<!-- //contents -->
		</div>
		<!-- //container -->
		
		<script type="text/javascript">
			menuOn("<?=$thisPageParentOrder?>", "<?=$thisPageOrder?>");
		</script>

		<!-- footer -->
		<? include("../include/footer.php");?>
		<!-- //footer -->
	</div>
	<!-- //wrapper -->
</body>
</html>