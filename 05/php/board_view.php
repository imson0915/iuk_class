<? include("../include/meta.php");?>
<body>
	<!-- wrapper -->
	<div class="wrapper" style="background: url(../make_img/sub01/sub_visual01.jpg) no-repeat center top;">
		<!-- header -->
		<? include("../include/header.php");?>
		<!-- //header -->

	
		<!-- container -->
		<div class="container" id="container">

			<!-- lnb -->
			<? include("../include/lnb.php");?>
			<!-- //lnb -->

			<!-- contents -->
			<div class="contents">
				
				<div class="contents-title">
					<h1>
						공지사항
					</h1>

					<p class="contents-navigation">
						<span class="icon-home">
							Home
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<span class="icon-word">
							커뮤니티
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<strong>
							공지사항
						</strong>
					</p>
				</div>



				<div class="board-area">
					<div class="board-view">
						<div class="title-area">
							<h2>
								공지사항 타이틀 입니다.
							</h2>
						</div>
						<div class="board-view-info">
							<dl class="number">
								<dt>번호 :</dt>
								<dd>58</dd>
							</dl>
							<dl class="writeer">
								<dt>작성자 : </dt>
								<dd>홍길동</dd>
							</dl>
							<dl class="date">
								<dt>작성일 : </dt>
								<dd>2014-00-00</dd>
							</dl>
							<dl class="hit">
								<dt>조회수 : </dt>
								<dd>54</dd>
							</dl>
						</div>
						<div class="board-contents">
							


						</div>

						<ul class="preview-next-area">
							<li class="preview">
								<a href="#">
									<span class="icon-prev">이전글</span>
									이전글이 없습니다.
								</a>
							</li>
							<li class="next">
								<a href="#">
									<span class="icon-next">다음글</span>
									다음글이 없습니다.
								</a>
							</li>
						</ul>
					</div>

					<div class="btns-area">
						<div class="btns-left">
							<a href="#" class="btns-type02">
								수정
							</a>

							<a href="#" class="btns-type02">
								삭제
							</a>
						</div>

						<div class="btns-right">
							<a href="#" class="btns-type01">
								글쓰기
							</a>
							<a href="#" class="btns-type02">
								목록
							</a>
						</div>
					</div>
				</div>








			</div>
			<!-- //contents -->
		</div>
		<!-- //container -->

		<!-- footer -->
		<? include("../include/footer.php");?>
		<!-- //footer -->
	</div>
	<!-- //wrapper -->
	<script type="text/javascript">
		menuOn(5, 1);
	</script>
</body>
</html>