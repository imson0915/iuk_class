<? include("../include/meta.php");?>
<body>
	<!-- wrapper -->
	<div class="wrapper" style="background: url(../make_img/sub01/sub_visual01.jpg) no-repeat center top;">
		<!-- header -->
		<? include("../include/header.php");?>
		<!-- //header -->

	
		<!-- container -->
		<div class="container" id="container">

			<!-- lnb -->
			<? include("../include/lnb.php");?>
			<!-- //lnb -->

			<!-- contents -->
			<div class="contents">
				
				<div class="contents-title">
					<h1>
						공지사항
					</h1>

					<p class="contents-navigation">
						<span class="icon-home">
							Home
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<span class="icon-word">
							커뮤니티
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<strong>
							공지사항
						</strong>
					</p>
				</div>



				<div class="search-n-reg-area">
					<p class="total left">
						총 222건의 자료가 있습니다. (1/23페이지)
					</p>
					<div class="right">
						<div class="search-area">
							<form action="" method="">	
								<fieldset>
									<legend class="blind">
										게시판 검색
									</legend>
									<select name="">
										<option value="">
											선택
										</option>
									</select>
									<div class="search-box">
										<input type="text" id="" name="" />
										<input type="image" src="../make_img/board/btn_search.gif" alt="검색" />
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>

				<div class="board-area">
					<div class="board-list">
						<table style="width: 100%" summary="이 표는 게시판에 관한 목록정보를 제공하는 표입니다.">
							<caption>게시판 목록표</caption>
							<colgroup>
								<col style="width: 10%" />
								<col style="width: 40%" />
								<col style="width: 10%" />
								<col style="width: 15%" />
								<col style="width: 15%" />
								<col style="width: 10%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">
										번호
									</th>
									<th scope="col">
										내용
									</th>
									<th scope="col">
										파일
									</th>
									<th scope="col">
										작성자
									</th>
									<th scope="col">
										작성일
									</th>
									<th scope="col" class="none">
										조회수
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										1190
									</td>
									<td class="left">
										<a href="board_view.html">
											부수가 고려된 중문 입력 장치 및 그 입력
										</a>
									</td>
									<td>
										<img src="../make_img/board/icon_addfile.gif" alt="첨부파일" />
									</td>
									<td>
										최고관리자
									</td>
									<td>
										2013-12-12
									</td>
									<td>
										32
									</td>
								</tr>

								<tr>
									<td colspan="6">
										등록된 정보가 없습니다.
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="btns-area">
						<div class="btns-left">
							<a href="#" class="btns-type02">
								수정
							</a>

							<a href="#" class="btns-type02">
								삭제
							</a>
						</div>

						<div class="btns-right">
							<a href="#" class="btns-type01">
								글쓰기
							</a>
						</div>
					</div>
				</div>

				<p class="paging-navigation">
					<a href="#" class="btns">
						<img src="../make_img/board/btn_first.gif" alt="" />
					</a>
					<a href="#" class="btns">
						<img src="../make_img/board/btn_previous.gif" alt="" />
					</a>

					<strong>1</strong>

					<a href="#">
						2
					</a>
					<a href="#">
						3
					</a>
					<a href="#">
						4
					</a>
					<a href="#">
						5
					</a>
					<a href="#">
						6
					</a>
					<a href="#">
						7
					</a>
					<a href="#">
						8
					</a>
					<a href="#">
						9
					</a>
					<a href="#">
						10
					</a>
					<a href="#" class="btns">
						<img src="../make_img/board/btn_next.gif" alt="" />
					</a>
					<a href="#" class="btns">
						<img src="../make_img/board/btn_last.gif" alt="" />
					</a>
				</p>







			</div>
			<!-- //contents -->
		</div>
		<!-- //container -->

		<!-- footer -->
		<? include("../include/footer.php");?>
		<!-- //footer -->
	</div>
	<!-- //wrapper -->
	<script type="text/javascript">
		menuOn(5, 1);
	</script>
</body>
</html>