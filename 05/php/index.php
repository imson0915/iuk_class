	<? include("../include/meta.php");?>
	<link rel="stylesheet" href="../css/main.css">
<body>
	<!-- wrapper -->
	<?
		$bgImage = CMS_IMG_PATH.$row_mainContent[IMG_SFILE];
	?>
	<div class="wrapper" style="background: url(<?=$bgImage?>) no-repeat center top">
		<!-- header -->
		<? include("../include/header.php");?>
		<!-- //header -->
	
		<!-- container -->
		<div class="container main" id="container">
			<!-- contents -->
			<div class="main-contents">
				<ul class="main-menu01">
					<?
					$arr_mainLink = array(array("학과특성", $row_mainContent[MAIN_LINK1]),
										  array("교육과정", $row_mainContent[MAIN_LINK2]),
										  array("포토갤러리", $row_mainContent[MAIN_LINK3]),
										  array("자료실", $row_mainContent[MAIN_LINK4]),
										  array("입학안내 ", $row_mainContent[MAIN_LINK5]),
										  array("취업정보", $row_mainContent[MAIN_LINK6]),
										  array("졸업후진로", $row_mainContent[MAIN_LINK7]),
										  array("강의시간표", $row_mainContent[MAIN_LINK8]));
					foreach ( $arr_mainLink as $k => $v ) {
						$common_param = "TREE_NO=".$v[1]."&PARENT=".$find_2depth[$v[1]][PARENT]."&TREE_ID=".$TREE_ID."&ROOT_NO=".$ROOT_NO;
						switch ( $find_2depth[$v[1]][ETC1] ) {
							case 'BOARD': $href_pageUrl = "board.php?".$common_param; break;
							case 'LINK': $href_pageUrl = $find_2depth[$v[1]][LINK_URL]; break;
							default: $href_pageUrl = "contentView.php?".$common_param; break;
						}
					?>
					<li>
						<a href="<?=$href_pageUrl?>">
							<img src="../make_img/main/main_menu<?=str_pad(($k+1), 2, '0', STR_PAD_LEFT)?>.png" alt="<?=$v[0]?>" />
							<?=$v[0]?>
						</a>
					</li>
					<? } ?>
				<ul>

			</div>
			<!-- //contents -->
		</div>
		<!-- //container -->

		<!-- footer -->
				<div class="footer">
			<div class="footer-wrapper">
				<ul class="footer-menu">
					<li>
						<a href="javascript:window.open('http://www.iuk.ac.kr/popup/privacy.php', '', 'width=645px,height=700px" class="word-privacy">
							개인정보처리방침
						</a>
					</li>
					<li>
						<a href="http://information.iuk.ac.kr/sub/sub01_01.php" target="_blank" >
							정보공개
						</a>
					</li>
					<li>
						<a href="javascript:window.open('http://www.iuk.ac.kr/popup/ethics.php', '', 'width=645px, height=700px');">
							네티즌윤리강령
						</a>
					</li>
					<li>
						<a href="javascript:window.open('http://www.iuk.ac.kr/popup/email.php', '', 'width=645px, height=700px');">
							이메일집단수집거부
						</a>
					</li>
					<li>
						<a href="#">
							교직원행동강령
						</a>
					</li>
					<? if ( $row_mainContent[ETC1] != "" ) { ?>
					<li>
						<a href="<?=$row_mainContent[ETC1]?>" target="_blank">
							CYWORLD클럽
						</a>
					</li>
					<? } if ( $row_mainContent[ETC2] != "" ) { ?>
					<li>
						<a href="<?=$row_mainContent[ETC1]?>" target="_blank">
							FACEBOOK
						</a>
					</li>
					<? } if ( $row_mainContent[ETC3] != "" ) { ?>	
					<li>
						<a href="<?=$row_mainContent[ETC1]?>" target="_blank">
							네이버카페
						</a>
					</li>
					<? } ?>
				</ul>
				

				<div class="footer-area">
					<div class="footer-box">
						<h2>
							<img src="../make_img/common/footer_logo.png" alt="iuk 한국국제대학교" />
						</h2>
						<address>
							<?=$row_mainContent[FOOTER_ADDR]?>
						</address>
						<p class="footer-info">
							<span>Tel) <?=$row_mainContent[FOOTER_TEL]?><span>
							<span>Fax) <?=$row_mainContent[FOOTER_FAX]?><span>
						</p>
						<p class="copyright">
							COPYRIGHT 2016 IUK ALL RIGHTS RESERVED.
						</p>
					</div>

					<div class="footer-select-box">
						<script>
							$("document").ready(function() {
								$(".select_link").change(function() {
									var url = $(this).val();
									if ( url != "none" ) {
										window.open(url, "", "");
									}
								});	
							});
						</script>
						<select class="select_link">
							<? class_select_link("1"); ?>
						</select>
						
						<select class="select_link">
							<? class_select_link("2"); ?>
						</select>
					</div>
				</div>
				
			</div>
		</div>

		<!-- //footer -->
	</div>
	<!-- //wrapper -->
	<script type="text/javascript">
		menuOn(0, 0);
	</script>
	<?php
	// 로그인이 안되어 있을 경우에만 해당 폼을 출력
	if (!isset($_SESSION['MEMBER_UID']))  {
	?>
		<form id="mainLoginForm" name="mainLoginForm" action="/page/popup/login_proc.php" method="post">
			<input type="hidden" id="login_id" name="login_id" value=""/>
			<input type="hidden" id="login_pw" name="login_pw" value=""/>
			<input type="hidden" id="divide" name="divide" value=""/>
			<input type="hidden" name="site" value="<?=$_GET['site']?>">
			<input type="hidden" name="mainform" value="<?=$_GET['mainform']?>">
			<input type="hidden" name="Confirm" value="login">
		</form>
	<?php
	}
	?>
	
	<script>
		$(document).ready(function(){
			// 로그아웃 서브밋
			$('#btnLogout').bind('click', function() {
				location.replace('/page/popup/login_proc.php?Confirm=logout&BURI=<?=$PHP_SELF?>?<?=$_SERVER['QUERY_STRING']?>');
			})
		})
	</script>
	<?
		$adb->disconnect();
		if ( $adb2 ) $adb2->disconnect();
	?>
</body>
</html>