$(function(){
	/* 비밀글 비밀번호 입력 */
	$('.open-password-box').click(function(){
		$('#wrapper').addClass('fixed');
		$('.check-password-area').fadeIn('fast', function() {
			$('.check-password-box').show();
		});
	});

	/* 비밀글 비밀번호 입력 취소*/
	$('.check-password-box .btn-close').click(function(){
		$('#wrapper').removeClass('fixed');
		$('.check-password-area').hide();
	});
});