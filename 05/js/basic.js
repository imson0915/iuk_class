/* menu on */
function menuOn(depth1, depth2) {
	var topmenu = $("#topmenu" + depth1)
	topmenu.addClass("on");

	var topsubmenu = $("#top-submenu" + depth1 + depth2)
	topsubmenu.addClass("on");

	var lnbmenu = $("#lnbmenu" + depth2)
	lnbmenu.addClass("on");
}

/* 탑메뉴 2차 메뉴 오픈 */
$(document).ready(function(){  
	$(".depth-1").hover(function() { //마우스를 topnav에 오버시
		$(this).next(".top-submenu").slideDown('fast').show(); //subnav가 내려옴.
		$(this).parent().hover(function() {  
		}, function(){  
			$(this).parent().find(".top-submenu").slideUp('fast'); //subnav에서 마우스 벗어났을 시 원위치시킴  
		});  
	});  
});