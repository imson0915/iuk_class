<? include("../include/meta.php");?>
<body>
	<!-- wrapper -->
	<?
		// 컨텐츠 내용 불러오기
		$sql_content = " SELECT *, ( SELECT IMG_SFILE FROM ".TABLE_CMS_CONTENTS." WHERE TREE_NO = '".$PARENT."' ) AS IMG_PARENT_SFILE FROM ".TABLE_CMS_CONTENTS." WHERE TREE_ID = '".$TREE_ID."' AND TREE_NO = '".$TREE_NO."' ";
		$rs_content = $adb->getRow($sql_content, DB_FETCHMODE_ASSOC);
	?>
	<div class="wrapper" style="background: url(<?=CMS_IMG_PATH.$rs_content[IMG_PARENT_SFILE]?>) no-repeat center top;">
		<!-- header -->
		<? include("../include/header.php");?>
		<!-- //header -->
		
		<!-- container -->
		<div class="container" id="container">

			<!-- lnb -->
			<? include("../include/lnb.php");?>
			<!-- //lnb -->

			<!-- contents -->
			<div class="contents">
				
				<div class="contents-title">
					<h1>
						<?=$thisPageName?>
					</h1>

					<p class="contents-navigation">
						<span class="icon-home">
							Home
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<span class="icon-word">
							<?=$thisPageParentName?>
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<strong>
							<?=$thisPageName?>
						</strong>
					</p>
				</div>
				
				<?
					if ( $menu_2depth[$PARENT][($thisPageOrder-1)][ETC1] == "TABUPPER" ) {
						$sql_3depth = " SELECT * FROM ".TABLE_TREE." WHERE PARENT = '".$TREE_NO."' AND TREE_ID = '".$TREE_ID."' ORDER BY ORDER_NO ";
						$rs_3depth = $adb->query($sql_3depth);
						// 3차 DEPTH ( TAB MENU )
						for ( $i = 0 ; $row_3depth = $rs_3depth->fetchRow(DB_FETCHMODE_ASSOC) ; $i++ ) {
							$row_3depth[LINK_URL] = $PHP_SELF."?ROOT_NO=".$ROOT_NO."&TREE_ID=".$TREE_ID."&TREE_NO=".$TREE_NO."&PARENT=".$PARENT."&CHILD=".$row_3depth[TREE_NO];
							$menu_3depth[$row_3depth[PARENT]][] = $row_3depth;
						}
						if ( $CHILD == "" ) $CHILD = $menu_3depth[$TREE_NO][0][TREE_NO];											
				?>
				<div class="tabmenu01">
					<ul>
						<? foreach ( $menu_3depth[$TREE_NO] as $k => $v ) { if ( $v[TREE_NO] == $CHILD ) $tabmenuIndex = $v[ORDER_NO]; ?>
						<li <? if ( ($k+1) == count($menu_3depth) ) echo "class='none';" ?>>
							<a href="<?=$v[LINK_URL]?>" <?=$v[LINK_TARGET]?> id="tabmenu<?=$k+1?>"> <?=$v[NAME]?> </a>
						</li>
						<? } ?>
					</ul>
				</div>
				<?
					$sql_tabContent = " SELECT * FROM ".TABLE_CMS_CONTENTS." WHERE TREE_ID = '".$TREE_ID."' AND TREE_NO = '".$CHILD."' ";
					$row_tabContent = $adb->getRow($sql_tabContent, DB_FETCHMODE_ASSOC);
					echo htmlspecialchars_decode($row_tabContent[CONTENTS])
				?>
				<script>
					tabmenuOn(<?=$tabmenuIndex?>);

					function tabmenuOn(tabmenu1) {
						var topmenu = $("#tabmenu" + tabmenu1);
						topmenu.addClass("on");
					}
				</script>
				<?
					} else echo htmlspecialchars_decode($rs_content[CONTENTS]);
				?>
				
			</div>
			<!-- //contents -->
		</div>
		<!-- //container -->
		
		<script type="text/javascript">
			menuOn("<?=$thisPageParentOrder?>", "<?=$thisPageOrder?>");
		</script>

		<!-- footer -->
		<? include("../include/footer.php");?>
		<!-- //footer -->
	</div>
	<!-- //wrapper -->
	
</body>
</html>