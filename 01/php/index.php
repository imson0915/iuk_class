<?
	include_once("../include/meta.php");
	include_once(ADFRAME_ROOT_PATH . "/lib/class_bbs.php");
?>
<body>
	<script>
		$(function() {
			$(document).ready(function() {
				var bgImage = "<?=CMS_IMG_PATH.$row_mainContent[IMG_SFILE]?>";
				if ( "<?=$row_mainContent[IMG_SFILE]?>" != "" ) {
					$(".wrapper").css({"background": "url("+bgImage+")", "background-repeat": "no-repeat", "background-position": "center top"});
				}
			});		
		});
	</script>
	<!-- wrapper -->
	<div class="wrapper main">
		<!-- header -->
		<? include("../include/header.php");?>
		<!-- //header -->
		<!-- container -->
		<div class="container main" id="container">

			<ul class="main-menu">
				<?
				$arr_mainLink = array(array("교육목표", $row_mainContent[MAIN_LINK1]),
									  array("학사일정", $row_mainContent[MAIN_LINK2]),
									  array("강의시간표", $row_mainContent[MAIN_LINK3]),
									  array("입학안내", $row_mainContent[MAIN_LINK4]),
									  array("취업정보", $row_mainContent[MAIN_LINK5]),
									  array("커뮤니티", $row_mainContent[MAIN_LINK6]));
				foreach ( $arr_mainLink as $k => $v ) {
				?>
				<li>
					<a href="<?=$find_2depth[$v[1]][LINK_URL]?>">
						<img src="../make_img/main/main_menu<?=str_pad($k+1, 2, '0', STR_PAD_LEFT)?>.png" alt="<?=$v[0]?>" />
						<?=$v[0]?>
					</a>
				</li>
				<? } ?>
			</ul>

			<div class="main-contents">


				<div class="tabmenu-board">
					<ul class="main-tabmenu">
						<li>
							<a href="#none" class="on" id="tabmenu01">
								공지사항
							</a>
						</li>
						<li>
							<a href="#none" id="tabmenu02">
								자료실
							</a>
						</li>
					</ul>
					<div class="tabmenu-contents" id="main-contents01">
						<ul>
							<?
							$notice_list = BBS_GetList(class_databaseTableName($ROOT_NO), $find_2depth[$row_mainContent[MAIN_BOARD1]][CONTENTS], 0, 7, 300);
							for ( $i = 0 ; $i < count($notice_list); $i++ ) {
								$COMMON_PARAM = "ROOT_NO=".$ROOT_NO."&TREE_ID=".$TREE_ID."&TREE_NO=".$row_mainContent[MAIN_BOARD1]."&PARENT=".$find_2depth[$row_mainContent[MAIN_BOARD1]][PARENT];
							?>
							<li>
								<a href="board.php?<?=$COMMON_PARAM?>&bbs=see&data=<?=$notice_list[$i][linkdata]?>">
									<span class="title">
										<?=StringCut($notice_list[$i][title], 0, 20, "utf-8")?>
									</span>
									<span class="date">
										<?=substr($notice_list[$i][datetime], -5)?>
									</span>
								</a>
							</li>
							<? } ?>
						</ul>
					</div>

					<div class="tabmenu-contents" id="main-contents02">
						<ul>
							<?
							$board_list = BBS_GetList(class_databaseTableName($ROOT_NO), $find_2depth[$row_mainContent[MAIN_BOARD2]][CONTENTS], 0, 7, 300);
							for ( $i = 0 ; $i < count($board_list); $i++ ) {
								$COMMON_PARAM = "ROOT_NO=".$ROOT_NO."&TREE_ID=".$TREE_ID."&TREE_NO=".$row_mainContent[MAIN_BOARD2]."&PARENT=".$find_2depth[$row_mainContent[MAIN_BOARD2]][PARENT];
							?>
							<li>
								<a href="board.php?<?=$COMMON_PARAM?>&bbs=see&data=<?=$board_list[$i][linkdata]?>">
									<span class="title">
										<?=StringCut($board_list[$i][title], 0, 20, "utf-8")?>
									</span>

									<span class="date">
										<?=substr($board_list[$i][datetime], -5)?>
									</span>
								</a>
							</li>
							<? } ?>
						</ul>
					</div>
				</div>

				<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK7]][LINK_URL]?>" class="btns-main-contents">
					<img src="../make_img/main/img0201.gif" alt="한눈에보는 학과특성 - 꿈과 희망으로 미래를 창조하는 학과 : 한국국제대학교는 글로벌 인재양성과 함께 학생들의 꿈을 실현하고자 합니다." />
				</a>
				<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK8]][LINK_URL]?>" class="btns-main-contents">
					<img src="../make_img/main/img0202.gif" alt="학과전공 교육과정소개 - 한눈으로 보는 교육과정소개 : 현장 실무 중심의 교과편성과 맞춤식 자격증 취득, 능력 평가제도 활용" />
				</a>
				<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK9]][LINK_URL]?>" class="btns-main-contents">
					<img src="../make_img/main/img0203.gif" alt="취업정보 졸업후진로 - 나의 미래를 꿈꾼다 졸업후진로안내: 국가와 지역사회가 필요로 하는 전문적 지식 습득과 현장 중심의 실무능력 배양" />
				</a>
			</div>
		</div>
		<!-- //container -->
		<script type="text/javascript">
			$(document).ready(function() {
				$("#tabmenu01").click(function() {
					$(".main-tabmenu li a").removeClass("on");
					$(this).addClass("on");
					$("#main-contents01").show();
					$("#main-contents02").hide();
				});
	
				$("#tabmenu02").click(function() {
					$(".main-tabmenu li a").removeClass("on");
					$(this).addClass("on");
					$("#main-contents02").show();
					$("#main-contents01").hide();
				});
			});
		</script>

		<!-- footer -->
		<? include("../include/footer.php");?>
		<!-- //footer -->
	</div>
	<!-- //wrapper -->
	
</body>
</html>