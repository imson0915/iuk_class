<?
    include_once("_common.php");
    header("Content-Type: text/html; charset=UTF-8");

    // 서브도메인 분리
    $sub_domain = $_SERVER["HTTP_HOST"];
    if ( !preg_match("/iuk.ac.kr/", $sub_domain) ) {
        go_back("올바르지 않은 주소입니다.");
    } else {
        $exp_subDomain = explode(".", $_SERVER["HTTP_HOST"]);
        $subKey = str_replace("iuk_", "", strtolower($exp_subDomain[0]));
    }

    $sql = " SELECT a.TREE_NO AS TREE_NO, a.TREE_ID AS TREE_ID, a.PARENT, DEPTH, ORDER_NO, NAME, a.ETC5 AS ETC5, TEMPLATE ";
    $sql .= " FROM ".TABLE_CMS_CONTENTS." AS a LEFT OUTER JOIN ".TABLE_TREE." AS b ON a.TREE_ID = b.TREE_ID AND a.TREE_NO = b.TREE_NO ";
    $sql .= " WHERE a.ETC5 = '".$subKey."' ";
    $row = $adb->getRow($sql, DB_FETCHMODE_ASSOC);
    $Template_Num = $row[TEMPLATE];

    if ( $Template_Num == "" || count($row) == "0" ) {
        // 해당 학과에 대한 정보가 없을 경우
        go_back(iconv("utf-8", "cp949", "레이아웃이 설정되지 않았거나, 홈페이지를 변경하지 않습니다."));
        exit;
    } else {
        $url = "./".$Template_Num."/php/index.php?TREE_ID=".$row[TREE_ID]."&TREE_NO=".$row[TREE_NO]."&ROOT_NO=".$row[TREE_NO];
    }

?>
<!DOCTYPE html>
<html lang="ko" style="height: 100%; overflow: hidden;">
    <head>
        <meta charset="utf-8" />
        <title><?=$row[NAME]." - "._TAG_TITLE?></title>
    </head>
    <body style="width: 100%; height: 100%; padding: 0; margin: 0;">
        <iframe src="<?=$url?>" frameborder="0" style="width: 100%; height: 100%;"></iframe>
    </body>
</html>

