<?
	include_once("../include/meta.php");
	include_once(ADFRAME_ROOT_PATH . "/lib/class_bbs.php");
?>
<body>
	<script>
		$(function() {
			$(document).ready(function() {
				var bgImage = "<?=CMS_IMG_PATH.$row_mainContent[IMG_SFILE]?>";
				if ( "<?=$row_mainContent[IMG_SFILE]?>" != "" ) {
					$(".container").css({"background": "url("+bgImage+")", "background-repeat": "no-repeat", "background-position": "center top"});
				}
			});		
		});
	</script>
	
	<!-- wrapper -->
	<div class="wrapper">
		<!-- header -->
		<? include("../include/header.php");?>
		<!-- //header -->

		<!-- container -->
		<div class="container main" id="container">
			

			<div class="main-contents">
				
				<ul class="main-menu">
					<?
					$arr_mainLink = array(array("학사일정", $row_mainContent[MAIN_LINK1]),
										  array("학과특성", $row_mainContent[MAIN_LINK2]),
										  array("교육과정", $row_mainContent[MAIN_LINK3]),
										  array("졸업후진로", $row_mainContent[MAIN_LINK4])
										 );
					foreach ( $arr_mainLink as $k => $v ) {
					?>
					<li>	
						<a href="<?=$find_2depth[$v[1]][LINK_URL]?>">
							<img src="../make_img/main/img01<?=str_pad(($k+1), 2, '0', STR_PAD_LEFT)?>.jpg" alt="<?=$v[0]?>" />
						</a>
					</li>
					<? } ?>
					<li>	
						<img src="../make_img/main/img0105.jpg" alt="" />
					</li>
				</ul>

				<div class="main-contents-area">

					<div class="main-tabmenu-list">
						<ul>
							<li>	
								<a href="#depth-1" class="depth-1 on">
									공지사항
								</a>
								<div class="main-tebmenu-contents" style="display: block">
									<ul>
										<?
										$notice_list = BBS_GetList(class_databaseTableName($ROOT_NO), $find_2depth[$row_mainContent[MAIN_BOARD1]][CONTENTS], 0, 4, 300);
										for ( $i = 0 ; $i < count($notice_list); $i++ ) {
											$COMMON_PARAM = "ROOT_NO=".$ROOT_NO."&TREE_ID=".$TREE_ID."&TREE_NO=".$row_mainContent[MAIN_BOARD1]."&PARENT=".$find_2depth[$row_mainContent[MAIN_BOARD1]][PARENT];
										?>
										<li>
											<a href="board.php?<?=$COMMON_PARAM?>&bbs=see&data=<?=$notice_list[$i][linkdata]?>">
												<span class="title">
													<?=$notice_list[$i][title]?>
												</span>
												<span class="date">	
													<?
													$expTime = explode("-", $notice_list[$i][datetime]);
													echo $expTime[1]."/".$expTime[2];
													?>
												</span>
											</a>
										</li>
										<? } ?>
									</ul>
									<a href="board.php?<?=$COMMON_PARAM?>" class="btn-more">
										more
									</a>
								</div>
							</li>
							<li>	
								<a href="#depth-1" class="depth-1">
									자료실
								</a>
								<div class="main-tebmenu-contents">
									<ul>
										<?
										$board_list = BBS_GetList(class_databaseTableName($ROOT_NO), $find_2depth[$row_mainContent[MAIN_BOARD2]][CONTENTS], 0, 4, 300);
										for ( $i = 0 ; $i < count($board_list); $i++ ) {
											$COMMON_PARAM = "ROOT_NO=".$ROOT_NO."&TREE_ID=".$TREE_ID."&TREE_NO=".$row_mainContent[MAIN_BOARD2]."&PARENT=".$find_2depth[$row_mainContent[MAIN_BOARD2]][PARENT];
										?>
										<li>
											<a href="board.php?<?=$COMMON_PARAM?>&bbs=see&data=<?=$board_list[$i][linkdata]?>">
												<span class="title">
													<?=$board_list[$i][title]?>
												</span>
												<span class="date">	
													<?
													$expTime = explode("-", $board_list[$i][datetime]);
													echo $expTime[1]."/".$expTime[2];
													?>
												</span>
											</a>
										</li>
										<? } ?>
									</ul>
									<a href="board.php?<?=$COMMON_PARAM?>" class="btn-more">
										more
									</a>
								</div>
								
							</li>
						</ul>
					</div>

					<ul class="main-banner01">
						<li>
							<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK5]][LINK_URL]?>">
								<img src="../make_img/main/img0301.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_LINK5]][NAME]?>" />
							</a>
						</li>
						<li>
							<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK6]][LINK_URL]?>">
								<img src="../make_img/main/img0302.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_LINK6]][NAME]?>" />
							</a>
						</li>
						<li>
							<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK7]][LINK_URL]?>">
								<img src="../make_img/main/img0303.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_LINK7]][NAME]?>" />
							</a>
						</li>
					</ul>
				</div>

			</div>

		</div>
		<!-- //container -->
		<!-- //wrapper -->
		<script type="text/javascript">
			menuOn(0, 0);
	
			(function() {
				$(".depth-1").click(function() {
					$(".depth-1").removeClass('on');
					$(this).addClass('on');
					$(".main-tebmenu-contents").hide();
					$(this).next().show();
				});
			})(jQuery);
		</script>

		<!-- footer -->
		<? include("../include/footer.php");?>
		<!-- //footer -->
	</div>
	
</body>
</html>