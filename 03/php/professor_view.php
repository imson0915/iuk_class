<? include("../include/meta.php");?>
<body>
	<!-- wrapper -->
	<div class="wrapper">
		<!-- header -->
		<? include("../include/header.php");?>
		<!-- //header -->
		
		<!-- sub visual -->
		<p class="sub-visual">
			<img src="../make_img/sub01/sub_visual01.jpg" alt="" />
		</p>
		<!-- sub visual -->

		<!-- container -->
		<div class="container" id="container">

			<!-- lnb -->
			<? include("../include/lnb.php");?>
			<!-- //lnb -->

			<!-- contents -->
			<div class="contents">
				
				<div class="contents-title">
					<p class="contents-navigation">
						<span class="icon-home">
							Home
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<span class="icon-word">
							학과안내
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<strong>
							교수소개
						</strong>
					</p>
					<h1>
						교수소개
					</h1>
				</div>

				<div class="contents-area">
					<div class="professor-wrapper">
						<div class="professor-area view">
							<img src="../make_img/professor/img01.jpg" alt="" class="image-professor" />
							<div class="professor-box">
								<div class="title-area">
									<h2>
										김영환 교수(학과장)
									</h2>
								</div>

								<dl>
									<dt>
										학위
									</dt>
									<dd>
										: 공학박사
									</dd>
								</dl>
								<dl>
									<dt>
										전공/연구분야
									</dt>
									<dd>
										: 전공의학용어, 건강보험실무, 의무기록관리학, 질병분류
									</dd>
								</dl>
								<dl>
									<dt>
										담당교과목
									</dt>
									<dd>
										: 자동차공학, 유압공학, 기계공작법
									</dd>
								</dl>
								<dl>
									<dt>
										연락처
									</dt>
									<dd>
										: 055-751-8232, 010-9393-1446
									</dd>
								</dl>
								<dl>
									<dt>
										E-mail
									</dt>
									<dd>
										: 
										<a href="mailto:ilmae@naver.com">
											ilmae@naver.com
										</a>
									</dd>
								</dl>
								<dl>
									<dt>
										Homepage
									</dt>
									<dd>
										: 
										<a href="happy.co.kr" title="새창열림" target="_blank">
											happy.co.kr
										</a>
									</dd>
								</dl>
							</div>
						</div>

						<div class="professor-detail-view">
							<h3 class="title0301">
								경력사항
							</h3>
							<ul class="ul-list01 mb25">
								<li>1960.3.2~1964.2.25. 부산대학교 약학과 졸업.</li>
								<li>1970.3.2~1972.2.25. 중앙대학교 대학원 약학과 석사과정 수료.</li>
								<li>1972.3.2~1975.2.23. 중앙대학교 대학원 약학과 박사과정 수료.</li>
								<li>1977.2.23. 중앙대학교 대학원에서 약학박사 학위 받음.</li>
								<li>1977.3.1.~1982.8.31. 진주보건대학 외래교수.</li>
								<li>1980.3.1~1985.2.28. 진주간호전문대학. 시간강사, 초빙교수</li>
								<li>1980.3.4~1990.8.31. 경상대학교 의과대학 자연과학대학 이공대학. 외래교수, 시간강사.</li>
								<li>1995.8.28~2001.8.25. 순천대학교 자연과학대학 한약자원학과. 겸임교수, 시간강사.</li>
								<li>1966.1.1~2007.4.2. 진주중앙약국 개설.</li>
								<li>1995.1.1~현재. 사단법인 한국생약학회 이사 및 대의원.</li>
								<li>1986.1.25~1994.12.31 사단법인 대한약사회 이사.</li>
								<li>1977.7.1~현재. 경남생약연구소장.</li>
								<li>1986.1.25~1992.1.24. 경상남도 약사회 회장 및 총회의장.</li>
								<li>1987.7.20~1993.6.30. 보건복지부 중앙약사 심의위원회 심의위원.</li>
								<li>1985.1.18~현재. 창원지방법원 진주지원 민사?가사 조정위원회 위원.</li>
								<li>2007.3.1~2011.2.28. 한국국제대학교 약재산업학과 석좌교수.</li>
								<li>2011.3.1~현재. 한국국제대학교 제약공학과 석좌교수.</li>
							</ul>

							<h3 class="title0301">
								연구업적
							</h3>
							<ul class="ul-list02">
								<li>Caragana Chamlagu(골담초)의 약효성분에 관한 연구(약학석사 학위논문 72. 2. 25)</li>
								<li>Caragana Chamlagu LAMARCK(골담초)의 Sterol 성분에 관한 연구(약학박사 학위논문 77. 2. 23)</li>
								<li>두충 삽목에 관한 연구(Ⅰ) (한국생약학회지 76. 3월호)</li>
								<li>식품영양에 관한 연구〈고들빼기의 재배 및 일반 영양성분에 관하여〉 (진주간호전문대학 논문집 77. 12)</li>
								<li>진양호 담수어 간디스토마 유충에 관한 연구(경상대학교 논문집 78. 9)</li>
								<li>오가피나무(약용식물)의 삽목에 관한 연구(진주간호전문대학 논문집 79. 12)</li>
								<li>골담초의 삽목번식에 관한 연구(진주간호전문대학 논문집 80. 12)</li>
								<li>Caragana Chamlagu LAMARCK의 Sterol성분에 관한 연구(약학회지 제22권4호. 78. 12)</li>
								<li>들깨의 생리 생태에 관한 연구(진주교육대학 논문집 74. 5)</li>
								<li>야생고들빼기의 성분분석에 관한 연구(경상대학교 논문집 81.8)</li>
								<li>지리산 생약자원 분포조사 연구(경상대학교 논문집 85.11)</li>
								<li>(+)-α-viniferin, an anti-inflammatory counmound from Caragana Chamlagu Root(일본약학회지 90. 2)</li>
								<li>담쟁이덩굴엽의플라보놀배당체(약학회지 제39권 제3호 289~296(1995))</li>
								<li>지리바꽃 괴경의 알카로이드(약학회지 제41권 제2호 161~173(1997))</li>
								<li>지리터리풀의 플라보놀 배당체(약학회지 제43권제1호 5~10(1999))</li>
								<li>지리산의 약용식물(제8회 한ㆍ일 생약학 합동 세미나 98. 8)</li>
								<li>상황제제의 항암활성(중앙대학교 약학대학 약품식물학 교실 연구논문(2000. 4)</li>
								<li>상황의 성인병 예방 및 치료효과에 관한 연구(2002. 8)</li>
								<li>민간상황제제의생리활성(2002. 12)</li>
								<li>가시연의 성인병 예방에 관한 연구(2003. 6)</li>
								<li>지리산의 약용식물 자원(경상대학교 농업자원 이용연구소 심포지엄 발표논문(1998. 11)</li>
								<li>지리산의 약용식물 자원생약(경상대학교 생명과학연구소 심포지엄 발표논문ː2003.12)</li>
								<li>단풍마의 약효성분에 관한 연구(2002) 특허권 획득으로 제품생산관계로 미공개</li>
								<li>겨우살이의 약효성분에 관한 연구(2004) 특허권 획득으로 제품생산관계로 미공개</li>
								<li>오가피의 약효성분에 관한 연구 (2005) 특허권 획득으로 제품생산관계로 미공개</li>
							</ul>
						</div>
						
						<div class="btns-area">
							<div class="btns-right">
								<a href="#" class="btns-type02">
									목록
								</a>
							</div>
						</div>	



					</div>		

				</div>





			</div>
			<!-- //contents -->
		</div>
		<!-- //container -->

		<!-- footer -->
		<? include("../include/footer.php");?>
		<!-- //footer -->
	</div>
	<!-- //wrapper -->
	<script type="text/javascript">
		menuOn(1, 3);
	</script>
</body>
</html>