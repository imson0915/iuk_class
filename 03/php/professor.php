<? include("../include/meta.php");?>
<body>
	<!-- wrapper -->
	<div class="wrapper">
		<!-- header -->
		<? include("../include/header.php");?>
		<!-- //header -->
		
		<!-- sub visual -->
		<p class="sub-visual">
			<img src="../make_img/sub01/sub_visual01.jpg" alt="" />
		</p>
		<!-- sub visual -->

		<!-- container -->
		<div class="container" id="container">

			<!-- lnb -->
			<? include("../include/lnb.php");?>
			<!-- //lnb -->

			<!-- contents -->
			<div class="contents">
				
				<div class="contents-title">
					<p class="contents-navigation">
						<span class="icon-home">
							Home
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<span class="icon-word">
							학과안내
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<strong>
							교육목표
						</strong>
					</p>
					<h1>
						교육목표
					</h1>
				</div>


				<div class="contents-area">
					<div class="professor-wrapper">
						<div class="professor-area">
							<img src="../make_img/professor/img01.jpg" alt="" class="image-professor" />
							<div class="professor-box">
								<div class="title-area">
									<h2>
										김영환 교수(학과장)
									</h2>
									<a href="#" class="btn-detail-view">
										자세히보기									
									</a>
								</div>

								<dl>
									<dt>
										학위
									</dt>
									<dd>
										: 공학박사
									</dd>
								</dl>
								<dl>
									<dt>
										전공/연구분야
									</dt>
									<dd>
										: 전공의학용어, 건강보험실무, 의무기록관리학, 질병분류
									</dd>
								</dl>
								<dl>
									<dt>
										담당교과목
									</dt>
									<dd>
										: 자동차공학, 유압공학, 기계공작법
									</dd>
								</dl>
								<dl>
									<dt>
										연락처
									</dt>
									<dd>
										: 055-751-8232, 010-9393-1446
									</dd>
								</dl>
								<dl>
									<dt>
										E-mail
									</dt>
									<dd>
										: 
										<a href="mailto:ilmae@naver.com">
											ilmae@naver.com
										</a>
									</dd>
								</dl>
								<dl>
									<dt>
										Homepage
									</dt>
									<dd>
										: 
										<a href="happy.co.kr" title="새창열림" target="_blank">
											happy.co.kr
										</a>
									</dd>
								</dl>
							</div>
						</div>




						<div class="professor-area">
							<img src="../make_img/professor/img02.jpg" alt="" class="image-professor" />
							<div class="professor-box">
								<div class="title-area">
									<h2>
										김영환 교수(학과장)
									</h2>
									<a href="#" class="btn-detail-view">
										자세히보기									
									</a>
								</div>

								<dl>
									<dt>
										학위
									</dt>
									<dd>
										: 공학박사
									</dd>
								</dl>
								<dl>
									<dt>
										전공/연구분야
									</dt>
									<dd>
										: 전공의학용어, 건강보험실무, 의무기록관리학, 질병분류
									</dd>
								</dl>
								<dl>
									<dt>
										담당교과목
									</dt>
									<dd>
										: 자동차공학, 유압공학, 기계공작법
									</dd>
								</dl>
								<dl>
									<dt>
										연락처
									</dt>
									<dd>
										: 055-751-8232, 010-9393-1446
									</dd>
								</dl>
								<dl>
									<dt>
										E-mail
									</dt>
									<dd>
										: 
										<a href="mailto:ilmae@naver.com">
											ilmae@naver.com
										</a>
									</dd>
								</dl>
								<dl>
									<dt>
										Homepage
									</dt>
									<dd>
										: 
										<a href="happy.co.kr" title="새창열림" target="_blank">
											happy.co.kr
										</a>
									</dd>
								</dl>
							</div>
						</div>



						<div class="professor-area">
							<img src="../make_img/professor/img03.jpg" alt="" class="image-professor" />
							<div class="professor-box">
								<div class="title-area">
									<h2>
										김영환 교수(학과장)
									</h2>
									<a href="#" class="btn-detail-view">
										자세히보기									
									</a>
								</div>

								<dl>
									<dt>
										학위
									</dt>
									<dd>
										: 공학박사
									</dd>
								</dl>
								<dl>
									<dt>
										전공/연구분야
									</dt>
									<dd>
										: 전공의학용어, 건강보험실무, 의무기록관리학, 질병분류
									</dd>
								</dl>
								<dl>
									<dt>
										담당교과목
									</dt>
									<dd>
										: 자동차공학, 유압공학, 기계공작법
									</dd>
								</dl>
								<dl>
									<dt>
										연락처
									</dt>
									<dd>
										: 055-751-8232, 010-9393-1446
									</dd>
								</dl>
								<dl>
									<dt>
										E-mail
									</dt>
									<dd>
										: 
										<a href="mailto:ilmae@naver.com">
											ilmae@naver.com
										</a>
									</dd>
								</dl>
								<dl>
									<dt>
										Homepage
									</dt>
									<dd>
										: 
										<a href="happy.co.kr" title="새창열림" target="_blank">
											happy.co.kr
										</a>
									</dd>
								</dl>
							</div>
						</div>




						<div class="professor-area">
							<img src="../make_img/professor/img04.jpg" alt="" class="image-professor" />
							<div class="professor-box">
								<div class="title-area">
									<h2>
										김영환 교수(학과장)
									</h2>
									<a href="#" class="btn-detail-view">
										자세히보기									
									</a>
								</div>

								<dl>
									<dt>
										학위
									</dt>
									<dd>
										: 공학박사
									</dd>
								</dl>
								<dl>
									<dt>
										전공/연구분야
									</dt>
									<dd>
										: 전공의학용어, 건강보험실무, 의무기록관리학, 질병분류
									</dd>
								</dl>
								<dl>
									<dt>
										담당교과목
									</dt>
									<dd>
										: 자동차공학, 유압공학, 기계공작법
									</dd>
								</dl>
								<dl>
									<dt>
										연락처
									</dt>
									<dd>
										: 055-751-8232, 010-9393-1446
									</dd>
								</dl>
								<dl>
									<dt>
										E-mail
									</dt>
									<dd>
										: 
										<a href="mailto:ilmae@naver.com">
											ilmae@naver.com
										</a>
									</dd>
								</dl>
								<dl>
									<dt>
										Homepage
									</dt>
									<dd>
										: 
										<a href="happy.co.kr" title="새창열림" target="_blank">
											happy.co.kr
										</a>
									</dd>
								</dl>
							</div>
						</div>


						
						<p class="paging-navigation">
							<a href="#" class="btns">
								<img src="../make_img/board/btn_first.gif" alt="" />
							</a>
							<a href="#" class="btns">
								<img src="../make_img/board/btn_previous.gif" alt="" />
							</a>

							<strong>1</strong>

							<a href="#">
								2
							</a>
							<a href="#">
								3
							</a>
							<a href="#">
								4
							</a>
							<a href="#">
								5
							</a>
							<a href="#">
								6
							</a>
							<a href="#">
								7
							</a>
							<a href="#">
								8
							</a>
							<a href="#">
								9
							</a>
							<a href="#">
								10
							</a>
							<a href="#" class="btns">
								<img src="../make_img/board/btn_next.gif" alt="" />
							</a>
							<a href="#" class="btns">
								<img src="../make_img/board/btn_last.gif" alt="" />
							</a>
						</p>

					</div>		

				</div>





			</div>
			<!-- //contents -->
		</div>
		<!-- //container -->

		<!-- footer -->
		<? include("../include/footer.php");?>
		<!-- //footer -->
	</div>
	<!-- //wrapper -->
	<script type="text/javascript">
		menuOn(1, 3);
	</script>
</body>
</html>