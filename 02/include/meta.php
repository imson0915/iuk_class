<?
include_once("../_common.php");
// 홈페이지 유지를 위한 내용
$__GPARAM = "&ROOT_NO=".$ROOT_NO."&TREE_ID=".$TREE_ID;
// 홈페이지 기본 정보
$sql = " SELECT * FROM ".TABLE_TREE." WHERE TREE_NO = '".$ROOT_NO."' AND TREE_ID = '".$TREE_ID."' ";
$row = $adb->getRow($sql, DB_FETCHMODE_ASSOC);
// 홈페이지 공통 컨텐츠
$sql_mainContent = " SELECT * FROM ".TABLE_CMS_CONTENTS." WHERE TREE_NO = '".$ROOT_NO."' AND TREE_ID = '".$TREE_ID."' ";
$row_mainContent = $adb->getRow($sql_mainContent, DB_FETCHMODE_ASSOC);

$adb2 = DB::connect($dsn);
// 1차 DEPTH
$sql_m = " SELECT *, ( SELECT COUNT(*) FROM ".TABLE_TREE." WHERE PARENT = a.TREE_NO ) AS cnt FROM ".TABLE_TREE." AS a ";
$sql_m .= " WHERE TREE_ID = '".$TREE_ID."' AND PARENT = '".$ROOT_NO."' AND MENU_ON = 'Y' ";
$rs_m = $adb->query($sql_m);
for ( $i = 0 ; $row_m = $rs_m->fetchRow(DB_FETCHMODE_ASSOC) ; $i++ ) {
	$GET_PARAM = "?TREE_NO=".$row_m[TREE_NO].$__GPARAM;
	// 새창열림
	if ( $row_m[LINK_TARGET] == "1" ) $row_m[LINK_TARGET] = "target='_blank'";
	// 메뉴 분류에 따른 링크 설정
	if ( $row_m[ETC1] == "MENU" ) $row_m[LINK_URL] = "javascript:;";
	else if ( $row_m[ETC1] == 'LINK' ) $row_m[LINK_URL] = $row_m[CONTENTS];
	else if ( $row_m[ETC1] == 'BOARD' ) $row_m[LINK_URL] = "board.php".$GET_PARAM;
	else $row_m[LINK_URL] = "contentView.php".$GET_PARAM;
	
	$menu_1depth[] = $row_m;
	$find_1depth[$row_m[TREE_NO]][] = $row_m;
	if ( $row_m[cnt] > 0 ) {
		// 2차 DEPTH
		$sql_subM = " SELECT * FROM ".TABLE_TREE." WHERE PARENT = '".$row_m[TREE_NO]."' AND MENU_ON = 'Y' ";
		$rs_subM = $adb2->query($sql_subM);
		for ( $j = 0 ; $row_sM = $rs_subM->fetchRow(DB_FETCHMODE_ASSOC) ; $j++ ){
			$GET_PARAM = "?TREE_NO=".$row_sM[TREE_NO]."&PARENT=".$row_sM[PARENT].$__GPARAM;
			// 새창열림
			if ( $row_sM[LINK_TARGET] == "1" ) $row_sM[LINK_TARGET] = "target='_blank'";
			// 메뉴 분류에 따른 링크 설정
			if ( $row_sM[ETC1] == "MENU" ) $row_sM[LINK_URL] = "javascript:;";
			else if ( $row_sM[ETC1] == 'LINK' ) $row_sM[LINK_URL] = $row_sM[CONTENTS];
			else if ( $row_sM[ETC1] == 'BOARD' ) $row_sM[LINK_URL] = "board.php".$GET_PARAM;
			else $row_sM[LINK_URL] = "contentView.php".$GET_PARAM;
				
			$menu_2depth[$row_m[TREE_NO]][] = $row_sM;
			$find_2depth[$row_sM[TREE_NO]] = $row_sM;
		}
	}

}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />

	<link rel="stylesheet" href="../css/common.css">
	<link rel="stylesheet" href="../css/main.css">
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/basic.js"></script>
	<title>
		<?=_TAG_TITLE?>
	</title>
	<?
	$MAINSITEURL = "www1.iuk.ac.kr";
	if(empty($_SESSION["MEMBER_UID"])) {
	?>
	<script language="Javascript">
	$(window).load(function(){
		$.getJSON("http://<?=$MAINSITEURL?>/login/session_public.php?jsoncallback=?", {SessionHost:"<?=$_SERVER['HTTP_HOST']?>"}, function(data){
			if(data.session_key != "false"){
				$.getJSON("/page/popup/session_public.php", {SessionHost:"<?=$_SERVER['HTTP_HOST']?>",session_key:data.session_key}, function(sresult){
					if(sresult.enddata == "complite"){
						window.location.reload();
					}
				});
			}
		});
	});
	</script>
	<? } ?>
	<script>
		function refresh_home() {
			window.parent.location.reload();
		}
		$(document).ready(function() {
			$('#btnLogin').bind('click', function() {
				window.open('http://www1.iuk.ac.kr/login/login.php?site=<?=$_SERVER[SERVER_NAME]?>','login','width=435, height=320, left=10,top=10,scrollbars=no');
			});
		});
	</script>
</head>
