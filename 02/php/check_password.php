					<!-- 비밀글 비밀번호 입력 -->
					<div class="check-password-area">
						<div class="check-password-box">
							<form method="POST" name="pwdForm">
								<input type="hidden" name="TREE_NO" value="<?=$TREE_NO?>">
								<input type="hidden" name="DEPTH" value="<?=$DEPTH?>">
								<fieldset>
									<legend class="blind">비밀글보기 입력 폼</legend>
									<h2>
										비밀번호 입력
									</h2>
									
									<!-- 비밀번호 입력 폼 -->
									<div class="input-password">
										<p>
											<strong class="point-color01">
												비밀번호를 입력하여 주세요.
											</strong>
										</p>
										<dl>
											<dt>
												<label for="password">
													비밀번호
												</label>
											</dt>
											<dd>
												<?=$_BBS_Password?>
												<input type="submit" id="" value="확인" title="확인" />
											</dd>
										</dl>
									</div>
									<!-- 비밀번호 입력 폼 -->

									<!-- 잘못된 비밀번호 입력 시 -->
									<div class="discord-password" style="display: none">
										<p>
											잘못된 비밀번호입니다. <span class="span-br"></span>
											비밀번호를 다시 입력해주세요.
										</p>
										<a href="#ok" class="btn-ok">
											확인
										</a>
									</div>
									<!-- //잘못된 비밀번호 입력 시 -->

								</fieldset>
							</form>
							<a href="#none" class="btn-close">
								<img src="../make_img/board/btn_close@2x.gif" alt="비밀글 비밀번호 입력 창 닫기" />
							</a>
						</div>
					</div>
					<!-- //비밀글 비밀번호 입력 -->
					
				
					<script type="text/javascript" src="../js/board.js"></script>