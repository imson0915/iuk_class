<?
	include_once("../include/meta.php");
	include_once(ADFRAME_ROOT_PATH . "/lib/class_bbs.php");
?>
<body>
<!-- wrapper -->
<div class="wrapper">
	<!-- header -->
	<? include("../include/header.php");?>
	<!-- //header -->
	<!-- container -->
	<div class="container main" id="container">

		<div class="main-contents">
			<div class="main-contents-area01">
				<a href="#">
					<img src="<?=CMS_IMG_PATH.$row_mainContent[IMG_SFILE]?>" alt="" />
				</a>
			</div>
			<div class="main-contents-area02">
				<div class="main-contents-box01">
					<ul class="main-contents-info01">
						<li>
							<!-- 학과 특성 -->
							<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK1]][LINK_URL]?>">
								<img src="../make_img/main/img0201.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_LINK1]][NAME]?>" />
							</a>
						</li>

						<li class="none">
							<!-- 교육과정 -->
							<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK2]][LINK_URL]?>">
								<img src="../make_img/main/img0202.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_LINK2]][NAME]?>" />
							</a>
						</li>
					</ul>

					<div class="main-contents-info02">
						<div class="title-area">
							<h2>
								포토갤러리
									<span>
										포토갤러리 안내입니다.
									</span>
							</h2>

							<a href="<?=$find_2depth[$row_mainContent[MAIN_BOARD2]][LINK_URL]?>">
								<img src="../make_img/main/btn_more.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_BOARD2]][NAME]?> 더보기" />
							</a>
						</div>

						<ul>
							<?
							$photo_list = BBS_GetList(class_databaseTableName($ROOT_NO), $find_2depth[$row_mainContent[MAIN_BOARD2]][CONTENTS], 1, 3, 300);
							for ( $i = 0 ; $i < count($photo_list) ; $i++ ) {
								$photo_list[$i][file_src] = str_replace("imageview.php", "imageview_renew.php", $photo_list[$i][file_src]);
								$common_param = "ROOT_NO=".$ROOT_NO."&TREE_ID=".$TREE_ID."&TREE_NO=".$row_mainContent[MAIN_BOARD2]."&PARENT=".$find_2depth[$row_mainContent[MAIN_BOARD2]][PARENT];;
							?>
							<li>
								<a href="board.php?<?=$common_param?>&bbs=see&data=<?=$photo_list[$i][linkdata]?>">
									<img src="<?=$photo_list[$i][file_src]?>" alt="<?=$photo_list[$i][title]?>" />
									<?=StringCut($photo_list[$i][title], 0, 10,'utf-8')?>
								</a>
							</li>
							<? } ?>
						</ul>
					</div>
				</div>

				<ul class="main-contents-box02">
					<!-- 학사 일정 -->
					<li>
						<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK3]][LINK_URL]?>">
							<img src="../make_img/main/img0401.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_LINK3]][NAME]?>" />
						</a>
					</li>
					<li>
						<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK5]][LINK_URL]?>">
							<img src="../make_img/main/img0402.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_LINK5]][NAME]?>" />
						</a>
					</li>
					<li>
						<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK6]][LINK_URL]?>">
							<img src="../make_img/main/img0403.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_LINK6]][NAME]?>" />
						</a>
					</li>
					<li>
						<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK7]][LINK_URL]?>">
							<img src="../make_img/main/img0404.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_LINK7]][NAME]?>" />
						</a>
					</li>
				</ul>

				<div class="main-contents-box03">
					<p class="banner01">
						<a href="<?=$find_2depth[$row_mainContent[MAIN_LINK4]][LINK_URL]?>">
							<img src="../make_img/main/img0501.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_LINK4]][NAME]?>" />
						</a>
					</p>

					<div class="main-notice-area">
						<div class="title-area">
							<h2>
								공지사항
							</h2>

							<a href="<?=$find_2depth[$row_mainContent[MAIN_BOARD1]][LINK_URL]?>">
								<img src="../make_img/main/btn_more02.gif" alt="<?=$find_2depth[$row_mainContent[MAIN_BOARD1]][LINK_URL]?> 더보기" />
							</a>
						</div>

						<ul>
							<?
							$notice_list = BBS_GetList(class_databaseTableName($ROOT_NO), $find_2depth[$row_mainContent[MAIN_BOARD1]][CONTENTS], 0, 7, 300);
							for ( $i = 0 ; $i < count($notice_list); $i++ ) {
								$common_param = "ROOT_NO=".$ROOT_NO."&TREE_ID=".$TREE_ID."&TREE_NO=".$row_mainContent[MAIN_BOARD1]."&PARENT=".$find_2depth[$row_mainContent[MAIN_BOARD1]][PARENT];;
							?>
							<li>
								<a href="board.php?<?=$common_param?>&bbs=see&data=<?=$notice_list[$i][linkdata]?>">
									<?=StringCut($notice_list[$i][title], 0, 15, "utf-8")?>
								</a>
							</li>
							<? } ?>
						</ul>
					</div>

				</div>
			</div>


		</div>



	</div>
	<!-- //container -->

	<!-- footer -->
	<? include("../include/footer.php");?>
	<!-- //footer -->
</div>
<!-- //wrapper -->
</body>
</html>