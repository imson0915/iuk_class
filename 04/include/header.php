		<p class="skip-navigation">
			<a href="#container">
				본문으로 바로가기
			</a>
		</p>
		<div class="header">
			<div class="header-area">
				<div class="header-box">
					<!-- gnb -->
					<div class="gnb-area">
						<ul class="gnb">
							<li class="none">
								<a href="javascript:refresh_home();">
									홈
								</a>
							</li>
							<li>
								<? if(isset( $_SESSION['MEMBER_UID'] )) { ?>
									<a href="#" id="btnLogout">
										로그아웃
									</a>
								<? }else{ ?>
									<a href="#" id="btnLogin">
										로그인
									</a>
								<? } ?>
							</li>
							<li>
								<a href="http://www.iuk.ac.kr" target="_blank">
									대학홈
								</a>
							</li>
							<li>
								<a href="http://ipsi.iuk.ac.kr" target="_blank">
									입시홈
								</a>
							</li>
						</ul>
					</div>
					<!-- //gnb -->
					
					<div class="header-info">
						<h1 class="logo">
							<a href="javascript:refresh_home();">
								<img src="../make_img/common/logo.png" alt="IUK 한국국제대학교" />
								<span class="subject-title">
									<?=$row[NAME]?>
								</span>
							</a>
						</h1>
						
						<!-- top menu -->
						<div class="topmenu-area">
							<ul class="top-mainmenu">
								<?
									
									
									foreach ( $menu_1depth as $k => $v ) {
								?>
								<li <? if ( $k == 0 ) echo "class='first'"; ?>>
									<a href="<?=$v[LINK_URL]?>" class="depth-1" id="topmenu<?=$k+1?>" <?=$v[LINK_TARGET]?>>
										<?=$v[NAME]?>
									</a>
									<? if ( $v[cnt] > 0 ) { ?>
									<div class="top-submenu" id="top-submenu<?=$k+1?>">
										<ul>
											<? foreach ( $menu_2depth[$v[TREE_NO]] as $k1 => $v1 ) { ?>
											<li>
												<a href="<?=$v1[LINK_URL]?>" id="top-submenu<?=$k+1?><?=$k1+1?>" <?=$v1[LINK_TARGET]?>> <?=$v1[NAME]?> </a>
											</li>
											<? } ?>
										</ul>
									</div>
									<? } ?>
								</li>
								<? } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
