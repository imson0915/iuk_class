		<div class="footer">
			<div class="footer-wrapper">
				<ul class="footer-menu">
					<li>
						<a href="javascript:window.open('http://www.iuk.ac.kr/popup/privacy.php', '', 'width=645px,height=700px" class="word-privacy">
							개인정보처리방침
						</a>
					</li>
					<li>
						<a href="http://information.iuk.ac.kr/sub/sub01_01.php" target="_blank" >
							정보공개
						</a>
					</li>
					<li>
						<a href="javascript:window.open('http://www.iuk.ac.kr/popup/ethics.php', '', 'width=645px, height=700px');">
							네티즌윤리강령
						</a>
					</li>
					<li>
						<a href="javascript:window.open('http://www.iuk.ac.kr/popup/email.php', '', 'width=645px, height=700px');">
							이메일집단수집거부
						</a>
					</li>
					<li>
						<a href="#">
							교직원행동강령
						</a>
					</li>
					<? if ( $row_mainContent[ETC1] != "" ) { ?>
					<li>
						<a href="<?=$row_mainContent[ETC1]?>" target="_blank">
							CYWORLD클럽
						</a>
					</li>
					<? } if ( $row_mainContent[ETC2] != "" ) { ?>
					<li>
						<a href="<?=$row_mainContent[ETC1]?>" target="_blank">
							FACEBOOK
						</a>
					</li>
					<? } if ( $row_mainContent[ETC3] != "" ) { ?>	
					<li>
						<a href="<?=$row_mainContent[ETC1]?>" target="_blank">
							네이버카페
						</a>
					</li>
					<? } ?>
				</ul>

				<div class="footer-area">
					<div class="footer-box">
						<h2>
							<img src="../make_img/common/footer_logo.gif" alt="iuk 한국국제대학교" />
						</h2>
						<address>
							<?=$row_mainContent[FOOTER_ADDR]?>
						</address>
						<p class="footer-info">
							<span>Tel) <?=$row_mainContent[FOOTER_TEL]?><span>
							<span>Fax) <?=$row_mainContent[FOOTER_FAX]?><span>
						</p>
						<p class="copyright">
							COPYRIGHT 2016 IUK ALL RIGHTS RESERVED.
						</p>
					</div>

					<div class="footer-select-box">
						<script>
							$("document").ready(function() {
								$(".select_link").change(function() {
									var url = $(this).val();
									if ( url != "none" ) {
										window.open(url, "", "");
									}
								});	
							});
						</script>
						<select class="select_link">
							<? class_select_link("1"); ?>
						</select>
						
						<select class="select_link">
							<? class_select_link("2"); ?>
						</select>
					</div>
				</div>
				
			</div>
		</div>
		<?
			$adb->disconnect();
			if ( $adb2 ) $adb2->disconnect();
		?>