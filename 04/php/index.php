<? include("../include/meta.php");?>
<body>
	<!-- wrapper -->
	<div class="wrapper">
		<!-- header -->
		<? include("../include/header.php");?>
		<!-- //header -->

		<!-- container -->
		<div class="container main" id="container">
			<div class="main-contents">
				
				<ul class="main-menu01">
					<?
					$arr_mainLink = array(array("학과특성", $row_mainContent[MAIN_LINK1]),
										array("교육과정", $row_mainContent[MAIN_LINK2]),
										array("졸업후진로", $row_mainContent[MAIN_LINK3]),
										array("입학안내", $row_mainContent[MAIN_LINK4]),
										array("학사일정", $row_mainContent[MAIN_LINK5]));
					foreach ( $arr_mainLink as $k => $v ) {
					?>
					<li>
						<a href="<?=$find_2depth[$v[1]][LINK_URL]?>">
							<img src="../make_img/main/main_menu<?=str_pad(($k+1), 2, '0', STR_PAD_LEFT)?>.png" alt="<?=$find_2depth[$v[1]][NAME]?>" />
						</a>
					</li>
					<? } ?>
				</ul>
			</div>

			<div class="main-visual" style="height: auto;">
				<img src="<?=CMS_IMG_PATH.$row_mainContent[IMG_SFILE]?>" alt="" />
			</div>

			<div class="main-contents02">
				<div class="notice-board">
					<div class="title-area">
						<h2>
							Notice
						</h2>
						<? $COMMON_PARAM = "ROOT_NO=".$ROOT_NO."&TREE_ID=".$TREE_ID."&TREE_NO=".$row_mainContent[MAIN_BOARD1]."&PARENT=".$find_2depth[$row_mainContent[MAIN_BOARD1]][PARENT]; ?>
						<a href="board.php?<?=$COMMON_PARAM?>">
							<img src="../make_img/main/btn_more.gif" alt="" />
						</a>
					</div>

					<ul>
						<?
						$notice_list = BBS_GetList(class_databaseTableName($ROOT_NO), $find_2depth[$row_mainContent[MAIN_BOARD1]][CONTENTS], 0, 4, 300);
						for ( $i = 0 ; $i < count($notice_list); $i++ ) {
						?>
						<li>
							<a href="board.php?<?=$COMMON_PARAM?>&bbs=see&data=<?=$notice_list[$i][linkdata]?>">
								<span class="title">
									<?=$notice_list[$i][title]?>
								</span>
								<span class="date">
									<?=substr($notice_list[$i][datetime], -5)?>
								</span>
							</a>
						</li>
						<? } ?>
					</ul>
				</div>

				<div class="contact-information-area">
					<h2>
						Contact
					</h2>

					<div class="contact-information-box">
						<dl class="type01">
							<dt>
								Tel.
							</dt>
							<dd>
								<?=substr($row_mainContent[FOOTER_TEL], 0, 3).") ".substr($row_mainContent[FOOTER_TEL], 4)?>
							</dd>
						</dl>

						<dl class="type02">
							<dt>
								Fax.
							</dt>
							<dd>
								<?=substr($row_mainContent[FOOTER_FAX], 0, 3).") ".substr($row_mainContent[FOOTER_FAX], 4)?>
							</dd>
						</dl>
					</div>
				</div>
			</div>

		</div>
		<!-- //container -->

		<!-- footer -->
		<? include("../include/footer.php");?>
		<!-- //footer -->
	</div>
	<!-- //wrapper -->
	<script type="text/javascript">
		menuOn(0, 0);
	</script>
</body>
</html>