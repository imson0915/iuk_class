<? include("../include/meta.php");?>
<body>
	<!-- wrapper -->
	<div class="wrapper">
		<!-- header -->
		<? include("../include/header.php");?>
		<!-- //header -->

		<!-- sub visual -->
		<p class="sub-visual">
			<img src="../make_img/sub01/sub_visual01.jpg" alt="" />
		</p>
		<!-- sub visual -->

		<!-- container -->
		<div class="container" id="container">

			<!-- lnb -->
			<? include("../include/lnb.php");?>
			<!-- //lnb -->

			<!-- contents -->
			<div class="contents">
				
				<div class="contents-title">
					<p class="contents-navigation">
						<span class="icon-home">
							Home
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<span class="icon-word">
							학과안내
						</span>
						<span class="icon-gt">
							&gt;
						</span>
						<strong>
							커뮤니티
						</strong>
					</p>

					<h1>
						공지사항
					</h1>
					
				</div>



				<div class="board-area">
					<form action="" method="">
						<fieldset>
							<legend class="blind">게시판 글쓰기 입력 폼</legend>
							<div class="board-write">
								<dl>
									<dt><label for="textfield01">제목</label></dt>
									<dd>
										<input type="text" id="textfield01" name="" />
									</dd>
								</dl>
								<dl class="line">
									<dt>
										<label for="file01">첨부파일</label>
									</dt>
									<dd>
										<input type="file" id="file01" name="" />

										<a href="#none" class="btn-add-input-file">
											<img src="../make_img/board/btn_add.gif" alt="추가 +" />
										</a>
										<a href="#none" class="btn-minus-input-file">
											<img src="../make_img/board/btn_minus.gif" alt="빼기 -" />
										</a>
									</dd>
								</dl>
								
								<div class="edit-area">
									에디트 영역입니다.
								</div>
							</div>
							<div class="btns-area">
								<div class="btns-left">
									<a href="#" class="btns-type02">
										수정
									</a>

									<a href="#" class="btns-type02">
										삭제
									</a>
								</div>

								<div class="btns-right">
									<input type="submit" value="확인" class="btns-type01" />
									<a href="#" class="btns-type02">
										목록
									</a>
								</div>
							</div>
						</fieldset>
					</form>
					
				</div>








			</div>
			<!-- //contents -->
		</div>
		<!-- //container -->

		<!-- footer -->
		<? include("../include/footer.php");?>
		<!-- //footer -->
	</div>
	<!-- //wrapper -->
	<script type="text/javascript">
		menuOn(5, 1);
	</script>
</body>
</html>